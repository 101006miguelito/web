<?php
  include '../../sunrise.games/config/config.php';

  // Mail
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\SMTP;
  use PHPMailer\PHPMailer\Exception;

  require '../../sunrise.games/libs/PHPMailer/src/Exception.php';
  require '../../sunrise.games/libs/PHPMailer/src/PHPMailer.php';
  require '../../sunrise.games/libs/PHPMailer/src/SMTP.php';

  $playToken = $_POST['playToken'];
  $chatMessages = $_POST['chatMessages'];
  $sentSecretKey = $_POST['secretKey'];

  $validRequest = false;

  if (empty($playToken) || (empty($chatMessages) || $sentSecretKey != $secretKey)) {
    $response = "Invalid POST parameters!";
  } else {
      $validRequest = true;
  }
  $emailContents = '';
  $emailContents = file_get_contents('assets/banEmailTemplate.html');
  $partsToMod  = array("(YOURUSERNAMEHERE)", "(YOURCHATHERE)");
  $replaceWith = array($playToken, $chatMessages);

  for ($i = 0; $i < count($partsToMod); $i++) {
    $emailContents = str_replace($partsToMod[$i], $replaceWith[$i], $emailContents);
  }

  if ($validRequest) {
      // Retrieve the email tied to the account.
      $stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
      $stmt->bind_param('s', $playToken);
      $stmt->execute();

      $result = $stmt->get_result();

      if ($result->num_rows < 1) {
          // Somehow, this account does not exist!
          $response = 'The specified account does not exist!';
      } else {
          while ($arr = $result->fetch_assoc()) {
              $ID = $arr['ID'];

              if (empty($arr['Email'])) {
                  // Must be a early Sunrise Games account.
                  $response = 'No email tied to account!';
              } else {
                  // Dispatch the email.
                  $email = new PHPMailer(false);

                  $email->isSMTP();

                  $email->Host = 'smtp.mailgun.org';
                  $email->SMTPAuth = true;
                  $email->Username = 'postmaster@mg.sunrise.games';
                  $email->Password = $emailKey;
                  $email->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                  $email->Port = 587;

                  $email->setFrom('community@disneyonline.com', 'Disney Community Service');
                  $email->addAddress($arr['Email'], 'Account Holder');

                  $email->isHTML(true);

                  $email->Subject = 'Your Disney Online Account';
                  $email->Body = $emailContents;
                  $email->AltBody = '';

                  try {
                      $email->send();
                } catch (Exception $e) {
                      echo 'Caught exception: '. $e->getMessage() ."\n";
                  }

                  $response = 'Successfully dispatched email!';
              }
            }
        }
    }
  echo $response;