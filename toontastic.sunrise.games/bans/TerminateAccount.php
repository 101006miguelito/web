<?php
  include '../../sunrise.games/config/config.php';

  // Mail
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\SMTP;
  use PHPMailer\PHPMailer\Exception;

  require '../../sunrise.games/libs/PHPMailer/src/Exception.php';
  require '../../sunrise.games/libs/PHPMailer/src/PHPMailer.php';
  require '../../sunrise.games/libs/PHPMailer/src/SMTP.php';

  $username = $_POST['username'];
  $reason = $_POST['reason'];
  $sentSecretKey = $_POST['secretKey'];

  if ($sentSecretKey != $secretKey) {
      $response = 'Nice try.';
  } else {
      $validRequest = true;
  }

  $emailContents = '';
  $emailContents = file_get_contents('assets/terminatedEmailTemplate.html');
  $partsToMod  = array("(YOURUSERNAMEHERE)");
  $replaceWith = array($username);

  for ($i = 0; $i < count($partsToMod); $i++) {
    $emailContents = str_replace($partsToMod[$i], $replaceWith[$i], $emailContents);
  }

  // TODO: This should pull Terminated field from the Users table too.
  if ($validRequest) {
      // Check if the username exists.
      $takenUser  = "SELECT * FROM Users WHERE Username='$username'";
      $userNameTakenStmt = $db->query($takenUser);

      if ($userNameTakenStmt->num_rows < 1) {
          // This account does not exist!
          $response = 'This account does not exist!';
      } else {
          // Query the database.
          $sql = "SELECT * FROM Terminations WHERE Username='$username'";
          $stmt = $db->query($sql);

          if ($stmt->num_rows > 0) {
              // This account is already terminated.
              $response = 'This account is already terminated!';
          } else {
              // Terminate this account.
              $db->query("INSERT INTO Terminations (`Username`, `Reason`) VALUES('$username', '$reason')");
              $db->query("UPDATE Users SET Terminated='1' WHERE Username='".$username."' AND Terminated='0'");

              while ($arr = $userNameTakenStmt->fetch_assoc()) {
                  $ID = $arr['ID'];

                  if (empty($arr['Email'])) {
                      // Must be a early Sunrise Games account.
                      $response = 'No email tied to account!';
                  } else {
                      // Dispatch the email.
                      $email = new PHPMailer(false);

                      $email->isSMTP();

                      $email->Host = 'smtp.mailgun.org';
                      $email->SMTPAuth = true;
                      $email->Username = 'postmaster@mg.sunrise.games';
                      $email->Password = $emailKey;
                      $email->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                      $email->Port = 587;

                      $email->setFrom('community@disneyonline.com', 'Disney Community Service');
                      $email->addAddress($arr['Email'], 'Account Holder');

                      $email->isHTML(true);

                      $email->Subject = 'Your Disney Online Account';
                      $email->Body = $emailContents;
                      $email->AltBody = '';

                      try {
                          $email->send();
                        } catch (Exception $e) {
                            echo 'Caught exception: '. $e->getMessage() ."\n";
                        }
                        $response = 'Successfully terminated account and dispatched email!';
                    }
                }
            }
        }
    }
    echo $response;