<?php
include '../../sunrise.games/config/config.php';

$username = $_POST['username'];
$reason = $_POST['banReason'];
$sentSecretKey = $_POST['secretKey'];

$validRequest = false;

if ($sentSecretKey != $secretKey) {
    $response = 'Nice try.';
} else {
    $validRequest = true;
}

// TODO: This should pull Banned field from the Users table too.
if ($validRequest) {
    // Check if the username exists.
    $takenUser = "SELECT * FROM Users WHERE Username='$username'";
    $userNameTakenStmt = $db->query($takenUser);

    if ($userNameTakenStmt->num_rows < 1) {
        // This account does not exist!
        $response = 'This account does not exist!';
    } else {
        // Query the database.
        $sql = "SELECT * FROM Bans WHERE Username='$username'";
        $stmt = $db->query($sql);

        if ($stmt->num_rows > 0) {
            // This account is already banned.
            $response = 'This account is already banned!';
        } else {
            // Ban this account.
            $db->query("INSERT INTO Bans (`Username`, `Reason`) VALUES('$username', '$reason')");
            $db->query("UPDATE Users SET Banned='1' WHERE Username='" . $username . "' AND Banned='0'");
            $response = "Banned account!";
        }
    }
}

echo $response;