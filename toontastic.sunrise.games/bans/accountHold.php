<?php
  include '../../sunrise.games/config/config.php';

  $username = $_GET['username'];

  $response = 'Something is wrong.';

  if (empty($username)) {
    $response = "No username found in GET parameters!";
  } else {
    $validRequest = true;
  }

  $webContents = "";
  $webContents = file_get_contents('assets/terminatedWebTemplate.html');
  $partsToMod  = array("(YOURUSERNAMEHERE)");
  $replaceWith = array($username);

  for ($i = 0; $i < count($partsToMod); $i++) {
    $webContents = str_replace($partsToMod[$i], $replaceWith[$i], $webContents);
  }

  if ($validRequest) {
    // Query the database.
    $stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
    $stmt->bind_param('s', $username);
    $stmt->execute();

    $result = $stmt->get_result();

    while ($arr = $result->fetch_assoc()) {
        if ($arr['Terminated'] == 1) {
            $response = $webContents;
        } else {
            $response = 'Account is not terminated!';
        }
    }
    echo $response;
}