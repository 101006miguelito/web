var _____WB$wombat$assign$function_____ = function(name) {return (self._wb_wombat && self._wb_wombat.local_init && self._wb_wombat.local_init(name)) || self[name]; };
if (!self.__WB_pmw) { self.__WB_pmw = function(obj) { this.__WB_source = obj; return this; } }
{
  let window = _____WB$wombat$assign$function_____("window");
  let self = _____WB$wombat$assign$function_____("self");
  let document = _____WB$wombat$assign$function_____("document");
  let location = _____WB$wombat$assign$function_____("location");
  let top = _____WB$wombat$assign$function_____("top");
  let parent = _____WB$wombat$assign$function_____("parent");
  let frames = _____WB$wombat$assign$function_____("frames");
  let opener = _____WB$wombat$assign$function_____("opener");

/***********************
EFSP version 1.9.0.0
Created by Daniel Niland
Copyright 2006 Disney
**************************/

var fspisIE  = (navigator.appVersion.indexOf("MSIE") != -1) ? true : false;
var fspisWin = (navigator.appVersion.toLowerCase().indexOf("win") != -1) ? true : false;
var fspisOpera = (navigator.userAgent.indexOf("Opera") != -1) ? true : false;

var fspPageIsLoaded = false;

var efspParentID;
var efspParentName;
var efspParentDivID;


function efspObj(theSession)
{
	this.playQueue = new Array();
	this.waiting = true;
	this.session = theSession;

	this.ready = efspReady;
	this.broadcast = execEFSPMethod;
	this.synchIFDisplay = efspSynchIFDisplay;
	this.createSynchFrame = efspCreateSynchIFrame;

	this.flashMovieX = null;
	this.flashMovieY = null;
}

function efspSet(parentID, parentName, parentDivID)
{
	var sCR = "";
	//put in Iframes for possible use in ad tracking and hit tracking
	sCR +=	"<IFRAME id='hitIF' name='hit' scrolling='no' style='position:absolute;overflow:hidden;width:0;height:0;top:0;left:0;z-index:1002;border:0px'></IFRAME>";
	sCR +=	"<IFRAME id='logIF' name='ads' scrolling='no' style='position:absolute;overflow:hidden;width:0;height:0;top:0;left:0;z-index:1001;border:0px'></IFRAME>";
	document.write(sCR);

	efspParentID = parentID;
	efspParentName = parentName;
	efspParentDivID = parentDivID;
	fspobjs = new Array();
}

function efspCreateSynchIFrame(ifName, ifID)
{
	clearSynchedAd = this.clearSynchedAd;
	synchIF = ifname;
	synchID = ifid;
	if (frames[synchIF])
	{
		//inform the flash that the frame has been set
		this.broadcast("uiSetSynchFrame", ifname);
	}
}

function efspSynchIFDisplay(isVis, sifUrl, iWidth, iHeight)
{
	//document.getElementById("debug").innerHTML += "<br>synch if: " + isVis + ":" + sifUrl;
	var sifn = frames["synch"];
	var sifid = document.getElementById("synchIF");
	if (sifn)
	{
		if (isVis)
		{

			if(!iWidth){iWidth=0}
			if(!iHeight){iHeight=0}
			sifid.style.width = iWidth + "px";
			sifid.style.height = iHeight + "px";
			sifn.location.replace(sifUrl);
		}
		else if (clearSynchedAd)
		{
			sifid.style.width = "0px";
			sifid.style.height = "0px";
		}
	}
}
function sendLogIF(logUrl)
{
	if (frames['ads'])
	{
		frames['ads'].location.replace(logUrl);
	}
}
function sendHitIF(hitUrl)
{
	if (frames['hit'])
	{
		hitUrl = hitUrl.replace(/liiiid/, "lid");
		frames['hit'].location.replace(hitUrl);
	}
}

function execEFSPMethod(method, params)
{
	//document.getElementById("debug").innerHTML += "<br>broadcast: " + this.session + ":" + this.waiting;
	var methObj = new Object();
	methObj.session = this.session;
	methObj.method = method;
	methObj.params = params;
	if (this.waiting)//set up a simple queue
	{
		this.playQueue[this.playQueue.length] = methObj;
	}
	else
	{
		this.waiting = true;
		sendMethod(methObj);
	}
}
//called by flash client to confirm js has been received, look for next
function efspReady()
{
	if (this.playQueue.length > 0)
	{
		this.waiting = true;
		var fplay = this.playQueue[0];
		for(var i = 0; i < this.playQueue.length; i++)
		{
			this.playQueue[i] = this.playQueue[i + 1];
		}
		this.playQueue.length--;
		sendMethod(fplay);
	}
	else
	{
		this.waiting = false;
	}

}


function sendMethod(methObj)
{
	//document.getElementById("debug").innerHTML += "<br>sending: " + methObj.session + ":" + methObj.method;
	thisEfspMovie()["FSPInterface" + methObj.session](methObj.method + "~" + methObj.params);
}

function thisEfspMovie()
{
	if (navigator.appName.indexOf("Microsoft") != -1)
    {
        return window[efspParentID]
    }
    else
    {
        return document[efspParentName]
    }
}

function FSPCall(session, methString)
{

	//the entire method is sent in as a string
	if (!session)
	{
		//document.getElementById("debug").innerHTML += "<br>receiving global:" + methString;
		eval(methString);
	}
	else
	{
		//document.getElementById("debug").innerHTML += "<br>receiving instance " + fspobjs[session] + ":" + methString;
		//this is called on an fsp instance
		eval("fspobjs['" + session + "']." + methString);
	}
}

function setStatus(theSession)
{
	for (s in fspobjs)
	{
		if (s == theSession) return;
	}
	var efsp = new efspObj(theSession);
	fspobjs[theSession] = efsp;
	if (window.attachExtFunctions)
	{
		attachExtFunctions(efsp);
	}
}

function fspaddEvent(obj, evType, fn)
{
	if (obj.addEventListener)
	{
		obj.addEventListener(evType, fn, false);
		return true;
	}
	else if (obj.attachEvent)
	{
		var r = obj.attachEvent("on"+evType, fn);
		return r;
	}
	else
	{
		return false;
	}
}
fspaddEvent(window, 'load', fspPageLoaded);

function fspPageLoaded()
{
	fspPageIsLoaded = true;
	if (window.checkExtWait)
	{
		checkExtWait();
	}
}

}