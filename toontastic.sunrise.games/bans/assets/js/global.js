var _____WB$wombat$assign$function_____ = function(name) {return (self._wb_wombat && self._wb_wombat.local_init && self._wb_wombat.local_init(name)) || self[name]; };
if (!self.__WB_pmw) { self.__WB_pmw = function(obj) { this.__WB_source = obj; return this; } }
{
  let window = _____WB$wombat$assign$function_____("window");
  let self = _____WB$wombat$assign$function_____("self");
  let document = _____WB$wombat$assign$function_____("document");
  let location = _____WB$wombat$assign$function_____("location");
  let top = _____WB$wombat$assign$function_____("top");
  let parent = _____WB$wombat$assign$function_____("parent");
  let frames = _____WB$wombat$assign$function_____("frames");
  let opener = _____WB$wombat$assign$function_____("opener");


// global.js

function setcookie (name,value,expires,path,domain,secure) {
	// If we're passing an expires variable, cast it (if necessary) to a Date
	if (expires) {
		if (typeof(expires) == "number" || typeof(expires) == "string") {
			expires = new Date(expires);
		}
	}
//    alert("the cookie string is '" + name + "=" + (value) + ((expires) ? "; expires=" + expires.toUTCString() : "") + ((path) ? "; path=" + path : "; path=/") + ((domain) ? "; domain=" + domain : "; domain=.go.com") +   ((secure) ? "; secure" : "") + "'");
    document.cookie = name + "=" + (value) + ((expires) ? "; expires=" + expires.toUTCString() : "") + ((path) ? "; path=" + path : "; path=/") + ((domain) ? "; domain=" + domain : "; domain=.go.com") +   ((secure) ? "; secure" : "");
}

function getparams(wut,qp,dflt){ dflt=(dflt==null)?'':dflt; try{r=unescape(wut.match(new RegExp(qp+"=+([^&;]*)"))[1]);}catch(qp){r=dflt;} return r; }

function getCartCount() {
    count = getparams(document.cookie, "DSIshopcartcount", null);
    if (count == null || isNaN(count)) {
        return 0;
    }
    count = parseInt(count);
    return (count == NaN)  ? 0 : count;
}

function isLoggedIn() {
    var blue = getparams(document.cookie, "BLUE", null);
    if (blue == null || blue == "") {
        return false;
    } else {
        return true;
    }
}

// used to clear default text in search boxes
function setIfBlank(obj, defaulttext)
{
    if (obj.value == "") {
        obj.value = defaulttext;
    }
}

// used to populate default text in search boxes
function clearIfDefault(obj, defaulttext)
{
    if (obj.value == defaulttext) {
        obj.value = "";
    }
}



function GetFlashMovieCookie(name) {
	if (document.cookie) {
		var cookies=document.cookie.split(";");
		for (var i=0; i<cookies.length; i++) {
			var varName=(cookies[i].split("=")[0]);
			var varValue=(cookies[i].split("=")[1]);

			while (varName.charAt(0)==" ") {
				varName=varName.substr(1,varName.length);

				// the escape() function will url encode the value
				if (varName==name) {
					return escape(varValue);
				}
			}
		}
	}
	return "";
}

function GetFlashMoviePageCookies() {
	DCOMMoviePageMovieSelectionValue = "";
	DCOMMoviePageZipCodeValue = "";
	returnString = "";
	if (document.cookie) {
		var cookies=document.cookie.split(";");
		for (var i=0; i<cookies.length; i++) {
			var varName=(cookies[i].split("=")[0]);
			var varValue=(cookies[i].split("=")[1]);

			while (varName.charAt(0)==" ") {
				varName=varName.substr(1,varName.length);
			}

			// the escape() function will url encode the value
			if (varName=="DCOMMoviePageMovieSelection") {
				DCOMMoviePageMovieSelectionValue = escape(varValue);
			}
			if (varName=="DCOMMoviePageZipCode") {
				DCOMMoviePageZipCodeValue = escape(varValue);
			}
		}
	}
	return DCOMMoviePageMovieSelectionValue + "|" + DCOMMoviePageZipCodeValue;
}






function setFlashMovieCookie (name,value,expires,path,domain,secure) {

	theDate = new Date(parseInt(expires));

//    alert(name + "=" + escape (value) + ((expires) ? "; expires=" + theDate.toGMTString() : "") + ((path) ? "; path=" + path : "; path=/") + ((domain) ? "; domain=" + domain : "; domain=.go.com") +   ((secure) ? "; secure" : ""));

    document.cookie = name + "=" + escape (value) + ((expires) ? "; expires=" + theDate.toGMTString() : "") + ((path) ? "; path=" + path : "; path=/") + ((domain) ? "; domain=" + domain : "; domain=.go.com") +   ((secure) ? "; secure" : "");
}


function setFlashMoviePageCookieValues(theZipCode, theZipExpiration, theMovie, theMovieExpiration) {

	var zipExpireDate = new Date(theZipExpiration);
	var movieChoiceExpireDate = new Date(theMovieExpiration);

	setcookie('DCOMMoviePageMovieSelection', escape(theMovie), movieChoiceExpireDate,'',location.hostname,'');
	setcookie('DCOMMoviePageZipCode', escape(theZipCode), zipExpireDate,'',location.hostname,'');
	return "OK";
}

}