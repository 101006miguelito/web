<?php
include '../../sunrise.games/config/config.php';

$validRequest = false;
$username = '';
$response = '';

if (isset($_GET['username'])) {
    $username = $_GET['username'];
    $validRequest = true;
} else {
    $response = 'No username found in GET parameters!';
}

$webContents = '';
$webContents = file_get_contents('assets/banWebTemplate.html');
$partsToMod = array('(YOURUSERNAMEHERE)', '(YOURDATEHERE)', '(YOURTIMEHERE)');
$replaceWith = array($username, 'N/A', 'N/A');

for ($i = 0; $i < count($partsToMod); $i++) {
    $webContents = str_replace($partsToMod[$i], $replaceWith[$i], $webContents);
}

if ($validRequest) {
    // Query the database.
    $stmt = $db->prepare('SELECT * FROM Bans WHERE Username = ?');
    $stmt->bind_param('s', $username);
    $stmt->execute();

    $result = $stmt->get_result();

    if ($result->num_rows < 1) {
        $response = 'You are not banned!';
        header('Location: https://toontastic.sunrise.games');
    } else {
        while ($arr = $result->fetch_assoc()) {
            echo $webContents;
        }
    }
}
echo $response;