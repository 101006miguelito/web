$(document).ready(function(){
	disTT.inputStyle.init();
});

//Begin brl.js
function clearErrs(){
  $('#formMsg').hide();
  $('#emailId').hide();
  $('#acctId').hide();
  $('#screenshotId').hide();
  $('#descId').hide();
}

function formHasErrors(){
  clearErrs();

  var formErrors = true;
  var email = $("#custom_field_10").val();
  var acctID = $("#custom_field_14").val();
  var desc = $("#description").val();
  var screenshot = $("#file_2").val();
  var stringErr = '*';

  try
    {
      if (email == "" || email == undefined){
        $('#emailId').show().addClass('ttError').html(stringErr);
        $('#custom_field_10').focus();
        $('#formMsg').show().addClass('ttError').html(util.getEmailErrTxt());
        return formErrors;
      }
      else if (!util.validEmail(email)){
        $('#emailId').show().addClass('ttError').html(stringErr);
        $('#custom_field_10').focus();
        $('#formMsg').show().addClass('ttError').html(util.getEmailFormatErrTxt());
        return formErrors;
      }

      if (acctID == "" || acctID == undefined){
        $('#acctId').show().addClass('ttError').html(stringErr);
        $('#custom_field_14').focus();
        $('#formMsg').show().addClass('ttError').html(util.getAcctIdErrTxt());
        return formErrors;
      }

      if (desc == "" || desc == undefined){
        $('#descId').show().addClass('ttError').html(stringErr);
        $('#description').focus();
        $('#formMsg').show().addClass('ttError').html(util.getDescErrTxt());
        return formErrors;
      }

      if (screenshot != null && screenshot != ''){
        var checkScreenshot = util.imgType(screenshot);
        if (checkScreenshot == false){
          $('#screenshotId').show().addClass('ttError').html(stringErr);
          $('#formMsg').show().addClass('ttError').html(util.getScreenshotErrTxt());
          return formErrors;
        }
      }

      formErrors = false;
      return formErrors;
    }
    catch(err)
    {
      return formErrors;
    }

}
//End brl.js
