/*
tabrotator.js
JAVASCRIPT FUNCTIONS FOR MEMBERSHIP PAGE ROTATING TABS
*/

var isRotating = true;
var gnNumTabLayers = 5;
var cur_lyr = 1; // holds id of currently visible layer

function swapLayers(id, nNumLayers) {
 if (nNumLayers != null) gnNumTabLayers = nNumLayers;
 if (cur_lyr != id) {
 showLayer(id);
 cur_lyr = id;
 }
}

function showLayer(id) {
	setCurrentDisplay(id);
}

function initLayers(id) {
 showLayer(id);
 cur_lyr = id;
 thetimer = setTimeout("RotateLayer()",5000);
 index1 = 1;
}

function RotateLayer() {
 index1 = index1 + 1
 if (index1 == gnNumTabLayers+1) {
 index1 = 1;
 }
 swapLayers(index1 );
 thetimer = setTimeout("RotateLayer()",5000);
}

function StartRotation() {
 if (isRotating == false) {
 thetimer = setTimeout("RotateLayer()",5000);
 isRotating = true;
 }
}
