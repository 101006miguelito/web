var indexUrl = ttown.getURL();
var redirectParamName = "redirectToUrl";

function querySt(name) {
    gy = window.location.search.substring(1).split("&");
    for (i=0;i<gy.length;i++) {
        ft = gy[i].split("=");
        if (ft[0] == name) {
            return ft[1];
        }
    }
}

function redirectToUrl(URL){
	if(URL == null) {
		window.location.href=indexUrl;
	}
	else {
		var goToUrl = "";

		if(window.location.search.substring(1).length > 0)
		{
			if(URL.indexOf("?") == -1) {
				URL = URL + "?";
			}
			else {
				URL = URL + "&";
			}
		}

		goToUrl = URL + window.location.search.substring(1);
	}

	window.location.href=goToUrl;
}

function loginComplete(URL){
	if(URL != null){
		window.location.href = URL;
	}
	else if(querySt(redirectParamName) != null) {
		window.location.href = decodeURI(querySt(redirectParamName));
	}
	else{
		window.location.href=indexUrl;
	}
}

function regComplete(URL){
	if(querySt(redirectParamName) != null) {
		window.location.href = querySt(redirectParamName);
	}
	else if(URL != null) {
		window.location.href = URL;
	}
	else {
		window.location.href=indexUrl;
	}
}

function goToReg(){
	window.location.href=indexUrl;
}

function exitMod(URL){
	if(querySt(redirectParamName)!= null) {
		window.location.href=querySt(redirectParamName);
	}
	else if(URL != null){
		window.location.href = URL;
	}
	else {
		window.location.href=indexUrl;
	}
}

function purchaseComplete(){
	window.location.href=indexUrl;
}

function toCancel(URL) {
	if(URL != null) {
		window.location.href=URL;
	}
	else {
		window.location.href=cancelUrl;
	}
}

function ttUser(id){
  if (id != null && id != ''){
    var ttLoginMsg1 = 'Hi! '+id;
    var ttLoginMsg2 = '<a href="#" id="ttLogout" name="ttLogout">Log out</a>';
    $('#ttLoginMsg1').html(ttLoginMsg1);
    $('#ttLoginMsg2').html(ttLoginMsg2);
    $('#ttLogout').click( function(){
      tt.logout();
    });
  }
}

function atlasRegConfirm(){
}
function atlasPurConfirm(){
}