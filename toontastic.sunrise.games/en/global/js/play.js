(function(b) {
    var a = navigator.userAgent.toLowerCase();
    b.browser = { version: (a.match(/.+(?:rv|it|ra|ie|me|ox)[\/: ]([\d.]+)/) || [0, "0"])[1], safari: /webkit/.test(a) && !/chrome/.test(a), chrome: /chrome/.test(a), opera: /opera/.test(a), msie: /msie/.test(a) && !/opera/.test(a), mozilla: /mozilla/.test(a) && !/(compatible|webkit)/.test(a) };
    b.os = { mac: /mac/.test(a), win: /win/.test(a), ppc: /ppc/.test(a) }
})(jQuery);
var TTPLAY = function(g) {
    var r = g;
    var n = "-1";
    var p = "-1";
    var f = -1;
    var i = "-1";
    var s = "-1";
    var j = false;
    var m = false;
    var q = 418;
    var b = -1;
    var o = "ttplay";
    var h = "0";
    var l = "-1";
    var a = "pc";
    var e = "mac";

    function d() {
        if (r.pageId != "" && r.pageId != null) { l = r.pageId }
        n = navigator.userAgent.toLowerCase();
        k();
        var t = new Date();
        t.setDate(t.getDate() + 365);
        if (s == a) {
            if (m) {
                h = s;
                tt.setCookie(o, h, t);
                window.location.replace(ttown.getURL() + "download")
            } else {
                h = s + "-";
                tt.setCookie(o, h, t);
                window.location.replace(ttown.getURL() + "download")
            }
        } else {
            if (s == e) {
                if (m) {
                    h = s;
                    tt.setCookie(o, h, t);
                    window.location.replace(ttown.getURL() + "download-mac")
                } else {
                    h = s + "-";
                    tt.setCookie(o, h, t);
                    window.location.replace(ttown.getURL() + "help/technical/mac#answer1")
                }
            } else {
                tt.setCookie(o, h, t);
                window.location.replace(ttown.getURL() + "help/technical/windows-pc")
            }
        }
    }

    function c() {
        var v = "applewebkit/";
        var t = navigator.userAgent.toLowerCase();
        var x = t.indexOf(v);
        var w = (x != -1);
        if (w) {
            var u = t.substring(x + v.length, t.length);
            u = u.substring(0, u.indexOf(" "));
            return parseInt(u)
        } else { return -1 }
    }

    function k() {
        if ($.os.win) {
            s = a;
            if (n.search(/nt\s6\.1/) != -1) {
                i = "windows7";
                m = true
            } else {
                if (n.search(/nt\s6\.0/) != -1) {
                    i = "vista";
                    m = true
                } else {
                    if (n.search(/nt\s5\.1/) != -1) {
                        i = "xp";
                        m = true
                    } else {
                        if (n.search(/nt\s5\.0/) != -1) {
                            i = "2000";
                            m = true
                        }
                    }
                }
            }
        } else {
            if ($.os.mac) {
                i = e;
                s = e;
                if ($.browser.safari) { b = c(); if (b >= q) { m = true } } else { m = true }
            }
        }
    }
    d()
};