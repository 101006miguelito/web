//Begin New Chrome and Footer
var mySettings = {
  chromeStyle: 'blue',
  footerStyleSet: 'transparentLight',
  footerDisplayMode: 'legalOnly',
  footerLegalSiteMapTarget:	'_top',
  footerLegalSafetyTarget: '_top',
  footerLegalPrivacyPolicyTarget: '_top',
  footerLegalIBATarget: '_top',
  footerLegalCustom: '<br /><br /><a href="https://toontastic.sunrise.games/member-agreements">Member Agreements</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://toontastic.sunrise.games/membership/account-management/">Member/Guest Services</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://toontastic.sunrise.games/help/">Help</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://toontastic.sunrise.games/help/contact">Contact Us</a>'
};
//End New Chrome and Footer

var ttBlog = {

  logout: function(){
    $.ajax({
      type: "GET",
      url: "/api/AccountLogoutRequest",
      cache: false,
      dataType: "xml",
      success: function(xml){
        $(xml).find('AccountLogoutResponse').each(function(){
          var ttLoginMsg1='<a name="&lid=site_topnav_join_free" href="https://toontastic.sunrise.games/join">Join for Free</a>';
          var ttLoginMsg2='<a name="&lid=site_topnav_sign_in" href="https://toontastic.sunrise.games/join">Sign in</a>';
          var success = $(this).find('success').text();
          if (success=="true"){
            $('#ttLoginMsg1').html(ttLoginMsg1);
            $('#ttLoginMsg2').html(ttLoginMsg2);
            window.location.replace('https://toontastic.sunrise.games/');//redirect to Home Page
          }
        });
      },
      error: function(XMLHttpRequest, textStatus, errorThrown){
        //alert('XMLHttpRequest: '+XMLHttpRequest+"\n"+'textStatus: '+textStatus+"\n"+'errorThrown: '+errorThrown);
      }
    });
  },

  login: function(){
    $.ajax({
      type: "GET",
      url: "/api/WhoAmIRequest",
      cache: false,
      dataType: "xml",
      success: function(xml){
        $(xml).find('WhoAmIResponse').each(function(){
          var ttLoginMsg1='<a name="&lid=site_topnav_join_free" href="https://toontastic.sunrise.games/join">Join for Free</a>';
          var ttLoginMsg2='<a name="&lid=site_topnav_sign_in" href="https://toontastic.sunrise.games/join">Sign in</a>';
          var success = $(this).find('success').text();
          var status = $(this).find('status').text();
          if (success=="true" && status=="logged_in_player"){
            var username = $(this).find('username').text();
            ttLoginMsg1 = 'Hi! <a href="https://toontastic.sunrise.games/membership/account-management/">'+username+'</a>';
            ttLoginMsg2 = '<a href="#" id="ttLogout" name="ttLogout">Log out</a>';
          }
          $('#ttLoginMsg1').html(ttLoginMsg1);
          $('#ttLoginMsg2').html(ttLoginMsg2);
          $('#ttLogout').click( function(){
            ttBlog.logout();
          });

          if ($(this).find('disney_connection').length > 0){
            $('#disChrome').hide();
            $('#ttBackToDC').html('<map name="ttBackToDCImg" id="ttBackToDCImg"><area shape="rect" coords="690,0,994,54" alt="Back to Disney Connection" title="Back to Disney Connection" target="_top" href="http://dmc.go.com/acceptlink.html" /></map><img src="https://toontastic.sunrise.games/toontown/en/global/img/back-to-disney-connection.jpg" width="994" height="54" title="" alt="" usemap="#ttBackToDCImg" />');
          }else{
            $('#ttBackToDC').hide();
            $('#disChrome').removeClass('ttHidden').addClass('ttVisible').show();
          }
        });

      },
      error: function(XMLHttpRequest, textStatus, errorThrown){
        $('#ttBackToDC').hide();
        $('#disChrome').removeClass('ttHidden').addClass('ttVisible').show();
         //alert('XMLHttpRequest: '+XMLHttpRequest+"\n"+'textStatus: '+textStatus+"\n"+'errorThrown: '+errorThrown);
      }
    });
  },

  setCookie: function(id,value,expires,path,domain,secure){
    if (expires){
      if (typeof(expires) == "number" || typeof(expires) == "string"){
        expires = new Date(expires);
      }
    }
    document.cookie = id + "=" + (value) + ((expires) ? "; expires=" + expires.toUTCString() : "") + ((path) ? "; path=" + path : "; path=/") + ((domain) ? "; domain=" + domain : "; domain=.go.com") +   ((secure) ? "; secure" : "");
  },

  getCookie: function(id){
    if(document.cookie.length > 0){
      var start=document.cookie.indexOf(id + "=");
      if(start != -1){
        start=start + id.length + 1;
        var end=document.cookie.indexOf(";",start);
        if(end == -1)
            end=document.cookie.length;
        return unescape(document.cookie.substring(start, end));
      }
    }
    return null;
  },

  delCookie: function(id){
    var cookieDate=new Date ( );  // current date & time
    cookieDate.setTime ( cookieDate.getTime() - 1 );
    document.cookie=id += "=; expires=" + cookieDate.toGMTString();
  },

  parseXml: function(xml){
    if(window.ActiveXObject && window.GetObject){
      var dom = new ActiveXObject('Microsoft.XMLDOM');
      dom.loadXML(xml);
      return dom;
    }
    if(window.DOMParser)
      return new DOMParser().parseFromString(xml,'text/xml');
    throw new Error('No XML parser available');
  },

  init: function(){
    $(document).ready(function(){
      ttBlog.login();
     $('#ttLogout').click( function(){
        ttBlog.logout();
      });
    });
  }

};

ttBlog.init();
