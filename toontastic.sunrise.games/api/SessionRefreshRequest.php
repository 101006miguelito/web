<?php
session_start();

if (isset($_SESSION['success'])) {
    $success = 'true';
} else {
    $success = 'false';
}

print '<SessionRefreshResponse>';
print '<<success>true</success>';

if ($_SESSION['logged'] === true) {
    print ("<status>logged_in_player</status>");
    print ("<username>".$_SESSION['username']."</username>");
 } else{
    print("<status>not_logged_in</status>");
 }

print ("<server-time>");
print ("<day>".date("Y/m/d")."</day>");
print ("<time>".date("h:i")."</time>");
print ("<dayofweek>".date("D")."</dayofweek>");
print ("<timestamp>".date("Y-m-d h:i:s")."</timestamp>");
print ("</server-time>");

print '</SessionRefreshResponse>';

header('Content-Type: text/xml');
?>