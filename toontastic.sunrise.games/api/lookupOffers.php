<?php
echo '<response>
<success>1</success>
<results>

<offer>
<product>TTO1M</product>
<offerItemTier>1</offerItemTier>
<term>Monthly</term>
<price>$9.95</price>
<currency>USD</currency>
<description>Monthly: $9.95 monthly</description>
</offer>

<offer>
<product>TTO6M</product>
<offerItemTier>2</offerItemTier>
<term>Semi-Annual</term>
<price>$49.95</price>
<currency>USD</currency>
<description>Semi-Annual: $49.95 for 6 months</description>
</offer>

<offer>
<product>TTO1Y</product>
<offerItemTier>3</offerItemTier>
<term>Annual</term>
<price>$79.95</price>
<currency>USD</currency>
<description>Annual: $79.95 for 12 months</description>
</offer>

</results>
</response>';
header('Content-Type: text/xml');
?>