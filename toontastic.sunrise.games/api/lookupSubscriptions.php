<?php
include '../../sunrise.games/config/config.php';

session_start();

$username = $_SESSION['username'];

if (isset($_POST['swid'])) {
    $swId = $_POST['swid'];
} else {
    $swId = 0;
}

$product = $_POST['product'];

if (empty($username)) {
	print 'Invalid parameters!';
}
else {
	$validRequest = true;
}

// Query the database.
$stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
$stmt->bind_param('s', $username);
$stmt->execute();

$result = $stmt->get_result();

/*
print '<response>';
print '<success>1</success>';
print '<results>';
print '<subscription>';
print '<paymentInformation>';
print '<paymentAccount>1</paymentAccount>';
print '<nextPaymentDate>10/17/2020</nextPaymentDate>';
print '<nextPaymentAmount>50</nextPaymentAmount>';
print '<paymentAccount>';
print ('<swid>'.$swId.'</swid>');
print '</paymentInformation>';
print '</paymentAccount>';
print '<rightsToPlay>';
print '<expireDate>10/18/2020</expireDate>';
print '<startDate>10/17/2020</startDate>';
print '<address>Test</address>';
print ('<ownerSwid>'.$swId.'</ownerSwid>');
print '<orderCodeType>prepaid</orderCodeType>';
print '<orderStatus>1</orderStatus>';
print '</rightsToPlay>';
print '<renewToInformation>';
print '<payPlan>1</payPlan>';
print '</renewToInformation>';
print '</results>';
print '</subscription>';
print '</response>';
*/

/*
print '<response>';
print '<success>1</success>';
print '<results>';
print '<subscription>';
print '<paymentInformation>';
print '<paymentAccount>1</paymentAccount>';
print '<nextPaymentDate>10/17/2020</nextPaymentDate>';
print '<nextPaymentAmount>50</nextPaymentAmount>';
print '<paymentAccount>';
print ('<swid>'.$swId.'</swid>');
print '</paymentInformation>';
print '</paymentAccount>';
print '<rightsToPlay>';
print '<expireDate>10/18/2020</expireDate>';
print '<startDate>10/17/2020</startDate>';
print '<address>Test</address>';
print ('<ownerSwid>'.$swId.'</ownerSwid>');
print '<orderCodeType>prepaid</orderCodeType>';
print '<orderStatus>1</orderStatus>';
print '</rightsToPlay>';
print '<renewToInformation>';
print '<payPlan>1</payPlan>';
print '</renewToInformation>';
print '</results>';
print '</subscription>';
print '</response>';
*/

if ($validRequest) {
	if (!$result->num_rows < 1) {
		while ($arr = $result->fetch_assoc()) {
            $member = $arr['Member'];

            if ($member == 1) {
                print '<response>';
                print '<success>1</success>';
                print '</response>';
            } else {
                print '<response>';
                print '<success>1</success>';
                print '</response>';
            }
		}
	}
	header('Content-Type: text/xml');
}
?>