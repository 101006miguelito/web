<?php
include '../../sunrise.games/config/config.php';

session_start();

$validRequest = false;
$username = '';

if (isset($_SESSION['username'])) {
	$username = $_SESSION['username'];
	$validRequest = true;
} else {
	print 'Invalid parameters!';
}

// Query the database.
$stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
$stmt->bind_param('s', $username);
$stmt->execute();

$result = $stmt->get_result();

if ($validRequest) {
	if (!$result->num_rows < 1) {
		while ($arr = $result->fetch_assoc()) {
			$ID = $arr['ID'];

			$firstName = $arr['FirstName'];
			$lastName = $arr['LastName'];
			$email = $arr['Email'];
			$openChat = $arr['OpenChat'];
			$age = $arr['Age'];

			print '<response type="onRegInitComplete">';
			print '<success>1</success>';
			print '<results>';
			print '<result isMigrated="true"></result>';
			print '</results>';
			print '<touAccepted>true</touAccepted>';
			print ('<firstName>'.$firstName.'</firstName>');
			print ('<lastName>'.$lastName.'</lastName>');
			print ('<email>'.$email.'</email>');
			print ('<username>'.$username.'</username>');
			print ('<swid>'.$ID.'</swid>');
			print ('<age>'.$age.'</age>');

			if ($age >= 18) {
				print '<hoh>true</hoh>';
			}

			/*
			// Newsletter
			print '<gokartLists>';
			print '<amount>2</amount>';
			print '</gokartLists>';
			*/

			if ($openChat == 1) {
				print '<canWhitelistChat>true</canWhitelistChat>';
				print '<canWhitelistChatValidationType>0</canWhitelistChatValidationType>';
			} else {
				print '<canWhitelistChat>false</canWhitelistChat>';
			}

			print '</response>';
		}
	}
	header('Content-Type: text/xml');
}
?>