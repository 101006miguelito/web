<?php
  include '../../../sunrise.games/config/config.php';

  if (!isset($_POST['redeemCode']) || !isset($_POST['secretKey']) || !isset($_POST['avatarId']) || !isset($_POST['serverName'])) {
      // They did not provide POST parameters.
      echo json_encode(array('success' => False, 'errorCode' => -1, 'reason' => 'Missing arguments.'));
      exit();
  }

  if ($secretKey !== $_POST['secretKey'] || $_SERVER['HTTP_USER_AGENT'] !== 'SunriseGames-TTCodeRedemptionMgrAI') {
      echo json_encode(array('status' => 'error', 'code' => '1', 'message' => 'Who are you?'));
      exit(0);
  }

  $redeemCode = $_POST['redeemCode'];
  $avatarId = $_POST['avatarId'];
  $serverName = $_POST['serverName'];

  $stmt = $db->prepare('SELECT * FROM CodeRedemption WHERE Code = ? AND ServerName = ?');
  $stmt->bind_param('ss', $redeemCode, $serverName);
  $stmt->execute();

  $result = $stmt->get_result();

  if ($result->num_rows > 0) {
      while ($arr = $result->fetch_assoc()) {
          $avatarIds = json_decode($arr['UsedBy'], true);

          foreach ($avatarIds as $avId) {
            if ($avId == $avatarId) {
              echo json_encode(array('status' => 'error', 'code' => '2', 'message' => 'Avatar already redeemed this code.'));
              exit();
            }
          }

          array_push($avatarIds, $avatarId);

          $finalResult = json_encode($avatarIds);

          $updateTable = $db->prepare('UPDATE CodeRedemption SET UsedBy = ? WHERE Code = ? AND ServerName = ?');
          $updateTable->bind_param('sss', $finalResult, $redeemCode, $serverName);
          $updateTable->execute();
      }
    } else {
        $usedBy = json_encode(array($avatarId));

        $initialAdd = $db->prepare('INSERT INTO CodeRedemption (Code, UsedBy, ServerName) VALUES (?, ?, ?)');
        $initialAdd->bind_param('sss', $redeemCode, $usedBy, $serverName);
        $initialAdd->execute();

        $message = 'Logged code use for ' . $avatarId . ' successfully.';
        echo json_encode(array('status' => 'success', 'code' => '1', 'message' => $message));
    }