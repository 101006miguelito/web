<?php
  include '../../../sunrise.games/config/config.php';

  if (!isset($_POST['redeemCode']) || !isset($_POST['secretKey']) || !isset($_POST['serverName'])) {
      // They did not provide POST parameters.
      echo json_encode(array('success' => False, 'errorCode' => -1, 'reason' => 'Missing arguments.'));
      exit();
  }

  if ($secretKey !== $_POST['secretKey'] || $_SERVER['HTTP_USER_AGENT'] !== 'SunriseGames-TTCodeRedemptionMgrAI') {
	echo json_encode(array('status' => 'error', 'code' => '1', 'message' => 'Who are you?'));
	exit(0);
  }

  $redeemCode = $_POST['redeemCode'];
  $serverName = $_POST['serverName'];

  $stmt = $db->prepare('SELECT * FROM CodeRedemption WHERE Code = ? AND ServerName = ?');
  $stmt->bind_param('ss', $redeemCode, $serverName);
  $stmt->execute();

  $result = $stmt->get_result();

  if ($result->num_rows > 0) {
      while ($arr = $result->fetch_assoc()) {
          $response = array(
              'RedeemedBy' => $arr['UsedBy']
          );

          echo json_encode($response);
      }
    } else {
        $response = array(
            'RedeemedBy' => json_encode(array())
        );

        echo json_encode($response);
    }