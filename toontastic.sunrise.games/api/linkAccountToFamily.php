<?php
include '../../sunrise.games/config/config.php';

session_start();

$masterUserId = $_SESSION['userId'];
$subUserId = $_POST['subUserId'];

// Check if the linked account already exists.
$stmt = $db->prepare('SELECT * FROM ParentAccounts WHERE Parent = ?');
$stmt->bind_param('i', $masterUserId);
$stmt->execute();

$result = $stmt->get_result();

$addLink = $db->prepare('INSERT INTO ParentAccounts (Parent, Child) VALUES (?, ?)');
$addLink->bind_param('ii', $masterUserId, $subUserId);

$flagOff = 0;
$flagOn = 1;

if ($result->num_rows > 0) {
    print '<response>';
    print '<success>0</success>';
    print '</response>';
  } else if ($addLink->execute()) {
    $setLinked = $db->prepare('UPDATE Users SET LinkedToParent = ? WHERE ID = ? AND LinkedToParent = ?');
    $setLinked->bind_param('iii', $flagOn, $subUserId, $flagOff);
    $setLinked->execute();

    print '<response>';
    print '<success>1</success>';
    print '</response>';
  }

header('Content-Type: text/xml');
?>