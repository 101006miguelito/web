<?php
  include '../../../sunrise.games/config/config.php';

  $username = '';
  $siteCode = '';

  $validRequest = false;

  if (isset($_GET['username']) && isset($_GET['siteCode'])) {
      $username = $_GET['username'];
      $siteCode = $_GET['siteCode'];
      $validRequest = true;
  } else {
      $response = "Invalid parameters!";
  }

  if ($validRequest) {
    // Check if the username is already taken.
    $stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
    $stmt->bind_param('s', $username);
    $stmt->execute();

    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
      $response = '0';
    } else {
      $response = '1';
      }
    }
    echo $response;