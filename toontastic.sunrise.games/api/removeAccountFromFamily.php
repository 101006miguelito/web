<?php
include '../../sunrise.games/config/config.php';

session_start();

$masterUserId = $_SESSION['userId'];
$childId = $_POST['userId'];

$stmt = $db->prepare('SELECT * FROM ParentAccounts WHERE Parent = ?');
$stmt->bind_param('i', $masterUserId);
$stmt->execute();

$result = $stmt->get_result();

if ($result->num_rows < 1) {
    print '<response>';
    print '<success>0</success>';
    print '</response>';
} else {
    $statement = $db->prepare('DELETE FROM ParentAccounts WHERE Parent = ? and Child = ?');
    $statement->bind_param('ii', $masterUserId, $childId);

    if ($statement->execute()) {
        print '<response>';
        print '<success>1</success>';
        print '</response>';
    } else {
        print '<response>';
        print '<success>0</success>';
        print '</response>';
    }
}
?>