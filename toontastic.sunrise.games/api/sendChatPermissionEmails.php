<?php
  session_start();

  include '../../sunrise.games/config/config.php';

  // Mail
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\SMTP;
  use PHPMailer\PHPMailer\Exception;

  require '../../sunrise.games/libs/PHPMailer/src/Exception.php';
  require '../../sunrise.games/libs/PHPMailer/src/PHPMailer.php';
  require '../../sunrise.games/libs/PHPMailer/src/SMTP.php';

  $sendEmail = $_POST['sendWhitelistChatEmail'];
  $username = $_SESSION['username'];

  if (empty($sendEmail) || empty($username)) {
      $response = "Invalid POST parameters!";
  } else {
      $validRequest = true;
  }

  $chatEmailContents = "";
  $chatEmailContents = file_get_contents('assets/chatTemplate.html');
  $chatPartsToMod = array("(PUTYOURNAMEHERE)", "(YOURUSERNAMEHERE)", "(YOUREMAILHERE)", "(verifyLinkHere)");
  $chatVerifyLink = 'https://toontastic.sunrise.games/api/activateChat/?username='.$username.'';

  if ($validRequest) {
      // Retrieve the email tied to the account.
      $stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
      $stmt->bind_param('s', $username);
      $stmt->execute();

      $result = $stmt->get_result();

      if ($result->num_rows < 1) {
          // Somehow, this account does not exist!
          $response = 'The specified account does not exist!';
      } else {
          while ($arr = $result->fetch_assoc()) {
              $ID = $arr['ID'];

              if (empty($arr['Email'])) {
                  // Must be a early Sunrise Games account.
                  $response = 'No email tied to account!';
              } else {
                  // Dispatch the email.
                  $chatReplaceWith = array($arr['FirstName'], $username, $arr['Email'], $chatVerifyLink);

                  for ($i = 0; $i < count($chatPartsToMod); $i++) {
                    $chatEmailContents = str_replace($chatPartsToMod[$i], $chatReplaceWith[$i], $chatEmailContents);
                  }

                  $email = new PHPMailer(false);

                  $email->isSMTP();

                  $email->Host = 'smtp.mailgun.org';
                  $email->SMTPAuth = true;
                  $email->Username = 'postmaster@mg.sunrise.games';
                  $email->Password = $emailKey;
                  $email->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                  $email->Port = 587;

                  $email->setFrom('tmemberservices@disney.twdc.com','Disney Online Guest Services');
                  $email->addAddress($arr['Email'], 'Account Holder');

                  $email->isHTML(true);

                  $email->Subject = 'Verify your e-mail address';
                  $email->Body = $chatEmailContents;
                  $email->AltBody = '';

                  try {
                      $email->send();
                  } catch (Exception $e) {
                      echo 'Caught exception: '. $e->getMessage() ."\n";
                  }
                  $response = 'Successfully dispatched email!';
              }
            }
        }
    }
  echo $response;