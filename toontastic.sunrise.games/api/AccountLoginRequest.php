<?php
include '../../sunrise.games/config/config.php';

$username = strtolower($_POST['username']);
$password = $_POST['password'];

if (empty($username) || empty($password)) {
	print 'Why are you viewing this directly?';
}
else {
	$validRequest = true;
}

// Combine the salt and password.
$saltedPassword = $salt . $password;

// Query the database.
$stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
$stmt->bind_param('s', $username);
$stmt->execute();

$result = $stmt->get_result();

if($validRequest) {
	if (!$result->num_rows < 1) {
		while ($arr = $result->fetch_assoc()) {
			$ID = $arr['ID'];

			// Grab the database hashed password.
			$hashedPassword = $arr['Password'];

			if (password_verify($saltedPassword, $hashedPassword)) {
				if (session_status() === PHP_SESSION_NONE) {
					session_start();
				}

				$_SESSION['username'] = $username;
				$_SESSION['success'] = 1;
				$_SESSION['status'] = "logged_in_player";
				$_SESSION['logged'] = true;
				$_SESSION['userId'] = $ID;
				print '<result>';
				print '<success>1</success>';
				print '<error>NO_ERROR</error>';
				print '<input>';
				print ('<cookievalue>' . session_id() . '</cookievalue>');
				print '<logintype>hard</logintype>';
				print '</input>';
				print '<token></token>';
				print '<type>hard</type>';
				print ('<banurl>' . '</banurl>');
				print ("<legacy_account>false</legacy_account>");
				print ("<legacy_migration_prompt>false</legacy_migration_prompt>");
				print ("<account>");
				print ("<touAccepted>true</touAccepted>");
				print ("</account>");
				print '</result>';
			} else {
				print '<result>';
				print '<success>0</success>';
				print '<error>PARAM_ERROR</error>';
				print '<input>';
				print '<cookievalue></cookievalue>';
				print '<logintype>hard</logintype>';
				print '</input>';
				print '<token></token>';
				print '<type>hard</type>';
				print '<banurl></banurl>';
				print '</result>';
			}
		}
	}
}
header('Content-Type: text/xml');
?>