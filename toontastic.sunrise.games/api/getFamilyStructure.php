<?php
include '../../sunrise.games/config/config.php';

session_start();

$masterUserId = $_SESSION['userId'];

$stmt = $db->prepare('SELECT * FROM ParentAccounts WHERE Parent = ?');
$stmt->bind_param('i', $masterUserId);
$stmt->execute();

$result = $stmt->get_result();

$data = simplexml_load_file('baseResponse.xml');

if ($result->num_rows < 1) {
    print $data->asXML();
} else {
    while ($arr = $result->fetch_assoc()) {
        $childUserId = $arr['Child'];

        $data->success = 1;

        $results = $data->addChild('results');

        $familyMembers = $results->addChild('familyMembers');
        $familyMember = $familyMembers->addChild('familyMember');
        $familyMember->addChild('userId', $childUserId);

        // At this point, we will need some data from the Account object.
        $accQueryStmt = $db->prepare('SELECT * FROM Users WHERE ID = ?');
        $accQueryStmt->bind_param('i', $childUserId);
        $accQueryStmt->execute();

        $accResult = $accQueryStmt->get_result();

        while ($accArr = $accResult->fetch_assoc()) {
            $username = $accArr['Username'];
            $age = $accArr['Age'];
			$firstName = $accArr['FirstName'];
        }

        $parentAccStmt = $db->prepare('SELECT * FROM Users WHERE ID = ?');
        $parentAccStmt->bind_param('i', $masterUserId);
        $parentAccStmt->execute();

        $parentAccResult = $parentAccStmt->get_result();

        while ($parentAccArr = $parentAccResult->fetch_assoc()) {
            $parentEmail = $parentAccArr['Email'];
        }

        $familyMember->addChild('username', $username);
        $familyMember->addChild('age', $age);
        $familyMember->addChild('firstName', $firstName);
        $familyMember->addChild('parentEmail', $parentEmail);

        // We are done.
        print $data->asXML();
    }
}

header('Content-Type: text/xml');
?>