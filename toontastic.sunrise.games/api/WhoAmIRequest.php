<?php
session_start();

if (isset($_SESSION['success'])) {
   $success = "true";
} else {
   $success = "false";
}

$notification = "";
print ("<WhoAmIResponse>");
print ("<success>$success</success>");

if (isset($_SESSION['logged']) && isset($_SESSION['username'])) {
   if ($_SESSION['logged'] === true) {
      print ("<status>logged_in_player</status>");
      print ("<username>".$_SESSION['username']."</username>");
   } else {
      print("<status>not_logged_in</status>");
   }
}

print ("<notification_banner> $notification </notification_banner>");
print ("<server-time>");
print ("<day>".date("Y/m/d")."</day>");
print ("<time>".date("h:i")."</time>");
print ("<dayofweek>".date("D")."</dayofweek>");
print ("<timestamp>".date("Y-m-d h:i:s")."</timestamp>");
print ("</server-time>");
print ("</WhoAmIResponse>");

header('Content-Type: text/xml');
?>