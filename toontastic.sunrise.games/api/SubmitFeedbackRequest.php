<?php
  include '../../sunrise.games/config/config.php';

  $response = 'Are you a robot?';

  if (
    isset($_POST['site']) && isset($_POST['subject']) && isset($_POST['Member_Id'])
    && isset($_POST['Name']) && isset($_POST['comments_or_questions'])
    && isset($_POST['Category_Message']) && isset($_POST['Region_Message'])
    && isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['E-Mail'])
    && isset($_POST['Account_Id']) && isset($_POST['description'])) {
    $site = $_POST['site'];
    $subject = $_POST['subject'];
    $memberId = $_POST['Member_Id'];
    $name = $_POST['Name'];
    $commentsOrQuestions = $_POST['comments_or_questions'];
    $categoryMessage = $_POST['Category_Message'];
    $regionMessage = $_POST['Region_Message'];
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $email = $_POST['E-Mail'];
    $accountId = $_POST['Account_Id'];
    $description = $_POST['description'];
  } else {
    echo $response;
  }

  header('Location: https://toontastic.sunrise.games');