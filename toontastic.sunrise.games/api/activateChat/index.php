<?php
  include '../../../sunrise.games/config/config.php';

  $response = 'Please use the link that has been sent to your email.';

  if (isset($_GET['username'])) {
      $username = $_GET['username'];

      $flagOff = 0;
      $flagOn = 1;

      // This is where the DB is queried.
      $query = $db->prepare('SELECT * FROM Users WHERE Username = ? AND OpenChat = ?');
      $query->bind_param('si', $username, $flagOff);
      $query->execute();

      $result = $query->get_result();

      if ($result->num_rows > 0) {
          // Activate the account's Open Chat.
          $stmt = $db->prepare('UPDATE Users SET OpenChat = ? WHERE Username = ? AND OpenChat = ?');
          $stmt->bind_param('isi', $flagOn, $username, $flagOff);
          $stmt->execute();

          $response = 'Activated Open Chat!';
          header('Location: https://toontastic.sunrise.games');
      } else {
          // Nope, invalid Open Chat activation URL or the account has already been activated.
          $response = 'Account has already been activated for Open Chat!';
          header('Location: https://toontastic.sunrise.games');
      }
  }
    echo $response;