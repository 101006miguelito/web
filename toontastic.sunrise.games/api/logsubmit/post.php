<?php

$numFiles = count($_FILES['file']['tmp_name']);

for ($x = 0; $x < $numFiles; $x++) {
    $file = $_FILES['file']['name'][$x];
    $fileName = $_FILES['file']['tmp_name'][$x];

    if (move_uploaded_file($fileName, 'uploads/'. $file)) {
        header('Location: https://toontastic.sunrise.games/help/thanks');
    }
}
?>