<?php
session_start();

include '../../../sunrise.games/config/config.php';

$ownerSwid = $_POST['ownerSwid'];
$idNbr = $_POST['idNbr'];
$swid = $_POST['swid'];

$username = $_SESSION['username'];

if (empty($ownerSwid) || empty($idNbr) || empty($swid)) {
	print 'Why are you viewing this directly?';
}
else {
	$validRequest = true;
}

$stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
$stmt->bind_param('s', $username);
$stmt->execute();

$result = $stmt->get_result();

if ($result->num_rows < 1) {
    echo 'Username does not exist in database!';
} else {
    $userExists = true;
}

while ($accArr = $result->fetch_assoc()) {
    if ($accArr['Member'] == 1) {
        // Account is already a member!
        $alreadyMember = true;
        return;
    }
}

$codeStmt = $db->prepare('SELECT * FROM GameCards WHERE Code = ?');
$codeStmt->bind_param('s', $idNbr);
$codeStmt->execute();

$codeResult = $codeStmt->get_result();

$invalidCode = false;

if ($codeResult->num_rows < 1) {
    print '<response>';
    print '<success>false</success>';
    print '<errors>';
    print '<error></error>';
    print '</errors>';
    print '</response>';
    $invalidCode = true;
}

if ($validRequest && $userExists && !$invalidCode || !$alreadyMember) {
    while ($codeArr = $codeResult->fetch_assoc()) {
        $isUsed = $codeArr['Used'];

        if ($isUsed == 0) {
            print '<response>';
            print '<success>true</success>';
            print '</response>';

            $flagOff = 0;
            $flagOn = 1;

             // Make the code used.
            $usedCode = $db->prepare('UPDATE GameCards SET Used = ? WHERE Code = ? AND Used = ?');
            $usedCode->bind_param('isi', $flagOn, $idNbr, $flagOff);
            $usedCode->execute();

            // Set this account as a member.
            $setMember = $db->prepare('UPDATE Users SET Member = ? WHERE Username = ? AND Member = ?');
            $setMember->bind_param('isi', $flagOn, $username, $flagOff);
            $setMember->execute();
        } else {
            print '<response>';
            print '<success>false</success>';
            print '<errors>';
            print '<error>CARD_IS_REDEEMED</error>';
            print '</errors>';
            print '</response>';
        }
    }
}
header('Content-Type: text/xml');
?>