<?php
  include '../../../sunrise.games/config/config.php';

  function random($length)
  {
      $chars = implode(range('a','f'));
      $chars .= implode(range('0','9'));
      $shuffled = str_shuffle($chars);
      return substr($shuffled, 0, $length);
    }

  function generateCode()
  {
      return 'TT'.random(4).random(4).random(4).random(2);
    }

  $gameCode = generateCode();

  $stmt = $db->prepare('SELECT * FROM GameCards WHERE Code = ?');
  $stmt->bind_param('s', $gameCode);
  $stmt->execute();

  $result = $stmt->get_result();

  if (!$result->num_rows > 0) {
      // Insert the code into the database.
      $statement = $db->prepare('INSERT INTO GameCards (Code) VALUES (?)');
      $statement->bind_param('s', $gameCode);
      $statement->execute();

      echo $gameCode;
  }