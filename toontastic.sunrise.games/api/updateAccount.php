<?php
  session_start();

  include '../../sunrise.games/config/config.php';

  $passwordChange = false;
  $validRequest = false;
  $username = '';
  $whitelistChatChange = false;
  $firstName = '';
  $lastName = '';
  $email = '';

  if (isset($_POST['oldPassword']) && isset($_POST['password'])) {
      $oldPassword = $_POST['oldPassword'];
      $password = $_POST['password'];

      $passwordChange = true;

      // Combine the salt and password.
      $saltedPassword = $salt . $password;

      // Hash the salted password.
      $hashedPassword = password_hash($saltedPassword, PASSWORD_DEFAULT);

      $username = $_SESSION['username'];
      $validRequest = true;
  }

  if (isset($_POST['whitelistChat'])) {
      // Whitelist change.
      $whitelistChat = $_POST['whitelistChat'];
      $whitelistChatChange = true;

      $username = $_SESSION['username'];
      $validRequest = true;
  }

  if (isset($_POST['userId']) && isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['email'])) {
      $userId = $_POST['userId'];
      $firstName = $_POST['firstName'];
      $lastName = $_POST['lastName'];
      $email = $_POST['email'];

      $username = $_SESSION['username'];
      $validRequest = true;
    }

  if ($validRequest) {
      $stmt = $db->query("SELECT * FROM Users WHERE Username='".$username."'");

      if ($stmt->num_rows > 0) {
          // Change the first name.
          if ($firstName) {
              $db->query("UPDATE Users SET FirstName='".$firstName."' WHERE Username='".$username."'");
          }

          if ($lastName) {
              // Change the last name.
              $db->query("UPDATE Users SET LastName='".$lastName."' WHERE Username='".$username."'");
          }

          if ($email) {
              // Change our email address.
              $db->query("UPDATE Users SET Email='".$email."' WHERE Username='".$username."'");
          }

          if ($passwordChange) {
              // Change our password.
              $db->query("UPDATE Users SET Password='".$hashedPassword."' WHERE Username='".$username."'");
          }

          if ($whitelistChatChange) {
              // Change our whitelist chat setting.
              if ($whitelistChat == 1) {
                  $number = 1;
              } else {
                  $number = 0;
              }

              $db->query("UPDATE Users SET OpenChat='".$number."' WHERE Username='".$username."'");
          }
        }
    }