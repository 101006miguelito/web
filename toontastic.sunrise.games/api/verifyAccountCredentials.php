<?php
  include '../../sunrise.games/config/config.php';

  $username = $_POST['username'];
  $password = $_POST['password'];

  // Combine the salt and password.
  $saltedPassword = $salt . $password;

  // Query the database.
  $stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
  $stmt->bind_param('s', $username);
  $stmt->execute();

  $result = $stmt->get_result();

  if ($result->num_rows < 1) {
    echo 'A error has occured or you did not pass GET parameters.';
  } else {
    while ($arr = $result->fetch_assoc()) {
      $ID = $arr['ID'];

      // Grab the database hashed password.
      $hashedPassword = $arr['Password'];

      if (!password_verify($saltedPassword, $hashedPassword)) {
          print '<response>';
          print '<success>0</success>';
          print '</response>';
      } else {
          print '<response>';
          print '<success>1</success>';
          print ('<userId>'.$ID.'</userId>');
          print '</response>';
      }
    }
  }
  header('Content-Type: text/xml');