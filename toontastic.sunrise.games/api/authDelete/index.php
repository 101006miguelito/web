<?php
  include '../../../sunrise.games/config/config.php';

  $usr = $_GET['n'];
  $pwd = $_GET['p'];

  // Combine the salt and password.
  $spwd = $salt . $pwd;

  // Query the database.
  $stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
  $stmt->bind_param('s', $usr);
  $stmt->execute();

  $result = $stmt->get_result();

  if ($result->num_rows < 1) {
    $response = "A error has occured or you did not pass GET parameters.";
  } else {
    while ($arr = $result->fetch_assoc()) {
      $ID = $arr['ID'];

      // Grab the database hashed password.
      $hpwd = $arr['Password'];

      if (!password_verify($spwd, $hpwd)) {
        $response = "ACCOUNT SERVER RESPONSE\nerrorCode=20\nerrorMsg=bad password";
      } else {
        $response = "ACCOUNT SERVER RESPONSE";
      }
    }
  }
  echo $response;