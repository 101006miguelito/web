function clearErrs(){
  $('#contactErr').hide();
  $('#fNameErr').hide();
  $('#lNameErr').hide();
  $('#emailErr').hide();
  $('#memberNameErr').hide();
  $('#commentsErr').hide();
}

function formHasErrors(){

  clearErrs();

  var formErrors = true;
  var fName = $("input#Name").val();
  var lName = $("input#LastName").val();
  var email = $("input#E-Mail").val();
  var memberName = $("input#Member_Name").val();
  var comments = $("textarea#Comments").val();
  var errorString  = '*';

  try
  {
      if (fName == "" || fName == undefined){
        $('#fNameErr').show().addClass('ttError').html(errorString);
        $('#Name').focus();
        return formErrors;
      }

      if (lName == "" || lName == undefined){
        $('#lNameErr').show().addClass('ttError').html(errorString);
        $('#LastName').focus();
        return formErrors;
      }

      if (email == "" || email == undefined){
        $('#emailErr').show().addClass('ttError').html(errorString);
        $('#E-Mail').focus();
        $('#contactErr').show().addClass('ttError').html(util.getEmailErrTxt());
        return formErrors;
      }
      else if (!util.validEmail(email)){
        $('#emailErr').show().addClass('ttError').html(errorString);
        $('#E-Mail').focus();
        $('#contactErr').show().addClass('ttError').html(util.getEmailFormatErrTxt());
        return formErrors;
      }

      if (memberName == "" || memberName == undefined){
        $('#memberNameErr').show().addClass('ttError').html(errorString);
        $('#Member_Name').focus();
        $('#contactErr').show().addClass('ttError').html(util.getAcctIdErrTxt());
        return formErrors;
      }

      if (comments == "" || comments == undefined){
        $('#commentsErr').show().addClass('ttError').html(errorString);
        $('#comments').focus();
        $('#contactErr').show().addClass('ttError').html(util.getDescErrTxt());
        return formErrors;
      }

      formErrors = false;
      return formErrors;
    }
    catch(err)
    {
      return formErrors;
    }


}

$(document).ready(function(){
  var x=document.getElementById("contactForm");
  var postURL  ='http://apps.disneyblast.go.com/cgi-bin/mail/generic_mail.cgi';
  x.action=postURL;

	$('#contactForm').submit(function(){
		var valid = formHasErrors();
		var submitForm = false;
    if (!valid){
      $('#contactErr').show().addClass('ttMsg');
      $('#contactErr').html(util.getWaitTxt());
      $("#Submit").attr("disabled","disabled");
      submitForm = true;
    }else{
      $('#contactErr').show().addClass('ttError');
      $('#contactErr').html(util.getRequiredFieldsTxt());
      submitForm = false;
    }
    return submitForm;
	});
});
