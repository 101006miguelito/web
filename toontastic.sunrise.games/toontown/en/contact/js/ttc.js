function cookieVal(cookieName){
  thisCookie = document.cookie.split("; ")
  for (i=0; i<thisCookie.length; i++) {
    if (cookieName == thisCookie[i].split("=")[0]) {
      return thisCookie[i].split("=")[1]
    }
  }
  return "Direct" //default value
}

var blast_isp_cookie = cookieVal("DCDN").toLowerCase();


function validateEmail(email)
{
  return (email.indexOf(".") > 2) && (email.indexOf("@") > 0);
}

$(function(){

  $("#Submit").removeAttr("disabled");

  $('#Category_Message').change(function(){
    var subject = $("#Category_Message").val();
    $('#subject').val(subject);

    if (blast_isp_cookie == "comcast"){
      $("#email1").val("disney_comcastcare@cable.comcast.com");
    }else
    if (subject == 'Ask Toontown'){
      $("#email1").val("questions@toontown.com");
    }else{
      $("#email1").val("toontown@disneyonline.com");
    }
  });


  //$("#contactForm").val("http://apps.disneyblast.go.com/cgi-bin/mail/generic_mail.cgi");
  var x=document.getElementById("contactForm");
  var postURL  ='http://apps.disneyblast.go.com/cgi-bin/mail/generic_mail.cgi';
  x.action=postURL;

  if (blast_isp_cookie == "comcast"){
    $("#email1").val("disney_comcastcare@cable.comcast.com");
  }

	$('#contactForm').submit(function(){
		var valid = !formHasErrors();
    if (!valid){
      $('#contactErr').show();
      $('#contactErr').addClass('ttError');
      $('#contactErr').html('Fields with a red * are required.');
    }else{
      $('#contactErr').show();
      $('#contactErr').addClass('ttMsg');
      $('#contactErr').html('Please wait while we process your request.');
      $("#Submit").attr("disabled","disabled");
    }
		return valid;
	});
});

function clearErrs(){

  $('#contactErr').hide();
  $('#fNameErr').hide();
  $('#lNameErr').hide();
  $('#emailErr').hide();
  $('#memberNameErr').hide();
  $('#commentsErr').hide();

}

function formHasErrors(){

  clearErrs();

  var formErrors = true;
  var fName = $("input#Name").val();
  var lName = $("input#LastName").val();
  var email = $("input#E-Mail").val();
  var memberName = $("input#Member_Name").val();
  var comments = $("textarea#Comments").val();
  var errorString  = '*';

  if (fName == "" || fName == undefined){
    $('#fNameErr').show();
    $('#fNameErr').addClass('ttError');
    $('#fNameErr').html(errorString);
    return formErrors;
  }
  else if (lName == "" || lName == undefined){
    $('#lNameErr').show();
    $('#lNameErr').addClass('ttError');
    $('#lNameErr').html(errorString);
    return formErrors;
  }
  else if (email == "" || email == undefined){
    $('#emailErr').show();
    $('#emailErr').addClass('ttError');
    $('#emailErr').html(errorString);
    return formErrors;
  }
  else if (!validateEmail(email)){
    $('#emailErr').show();
    $('#emailErr').addClass('ttError');
    $('#emailErr').html(errorString);
    return formErrors;
  }
  else if (memberName == "" || memberName == undefined){
    $('#memberNameErr').show();
    $('#memberNameErr').addClass('ttError');
    $('#memberNameErr').html(errorString);
    return formErrors;
  }
  else if (comments == "" || comments == undefined){
    $('#commentsErr').show();
    $('#commentsErr').addClass('ttError');
    $('#commentsErr').html(errorString);
    return formErrors;
  }
  else {
    formErrors = false;
    return formErrors;
  }
}
