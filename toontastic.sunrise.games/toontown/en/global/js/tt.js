//Begin Chrome and Footer
var mySettings = {
    chromeStyle: 'blue',
    footerStyleSet: 'transparentLight',
    footerDisplayMode: 'legalOnly',
    footerLegalSiteMapTarget: '_top',
    footerLegalSafetyTarget: '_top',
    footerLegalPrivacyPolicyTarget: '_top',
    footerLegalIBATarget: '_top',
    footerLegalCustom: '<br /><br /><a href="' + ttown.getURL() + 'member-agreements">Member Agreements</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="' + ttown.getURL_SSL() + 'membership/account-management/">Member/Guest Services</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="' + ttown.getURL() + 'help/">Help</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="' + ttown.getURL() + 'help/contact">Contact Us</a>'
};
//End Chrome and Footer

var tt = {

    logout: function() {
        var ttLogout = false;
        $.ajax({
            type: "GET",
            url: "https://toontastic.sunrise.games/api/AccountLogoutRequest.php",
            cache: false,
            dataType: "xml",
            success: function(xml) {
                $(xml).find('AccountLogoutResponse').each(function() {
                    var ttLoginMsg1 = '<a href="' + ttown.getURL_SSL() + 'membership/subscribe">Join for Free</a>';
                    var ttLoginMsg2 = '<a href="' + ttown.getURL_SSL() + 'join">Sign in</a>';
                    var ttPlayNow = '<a href="' + ttown.getURL_SSL() + 'join">PLAY NOW!</a>';
                    var success = $(this).find('success').text();
                    if (success == "true") {
                        $('#ttLoginMsg1').html(ttLoginMsg1);
                        $('#ttLoginMsg2').html(ttLoginMsg2);
                        $('#ttPlayNow').html(ttPlayNow);
                        window.location.replace(ttown.getURL());
                    }
                });
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {}
        });
    },

    login: function() {
        $.ajax({
            type: "GET",
            url: "https://toontastic.sunrise.games/api/WhoAmIRequest.php",
            cache: false,
            dataType: "xml",
            success: function(xml) {
                $(xml).find('WhoAmIResponse').each(function() {
                    var ttLoginMsg1 = '<a name="&lid=site_topnav_join_free" href="' + ttown.getURL_SSL() + 'join">Join for Free</a>';
                    var ttLoginMsg2 = '<a name="&lid=site_topnav_sign_in" href="' + ttown.getURL_SSL() + 'join">Sign in</a>';
                    var success = $(this).find('success').text();
                    var status = $(this).find('status').text();
                    if (success == "true" && status == "logged_in_player") {
                        var username = $(this).find('username').text();
                        ttLoginMsg1 = 'Hi! <a href="' + ttown.getURL_SSL() + 'membership/account-management/">' + username + '</a>';
                        ttLoginMsg2 = '<a href="#" id="ttLogout" name="ttLogout">Log out</a>';
                    }
                    $('#ttLoginMsg1').html(ttLoginMsg1);
                    $('#ttLoginMsg2').html(ttLoginMsg2);
                    $('#ttLogout').click(function() {
                        tt.logout();
                    });
                });
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus + " - " + errorThrown);
            }
        });
    },

    setCookie: function(id, value, expires, path, domain, secure) {
        if (expires) {
            if (typeof(expires) == "number" || typeof(expires) == "string") {
                expires = new Date(expires);
            }
        }
        document.cookie = id + "=" + (value) + ((expires) ? "; expires=" + expires.toUTCString() : "") + ((path) ? "; path=" + path : "; path=/") + ((domain) ? "; domain=" + domain : "; domain=.games") + ((secure) ? "; secure" : "");
    },

    getCookie: function(id) {
        if (document.cookie.length > 0) {
            var start = document.cookie.indexOf(id + "=");
            if (start != -1) {
                start = start + id.length + 1;
                var end = document.cookie.indexOf(";", start);
                if (end == -1)
                    end = document.cookie.length;
                return unescape(document.cookie.substring(start, end));
            }
        }
        return null;
    },

    delCookie: function(id) {
        var cookieDate = new Date(); // current date & time
        cookieDate.setTime(cookieDate.getTime() - 1);
        document.cookie = id += "=; expires=" + cookieDate.toGMTString();
    },

    parseXml: function(xml) {
        if (window.ActiveXObject && window.GetObject) {
            var dom = new ActiveXObject('Microsoft.XMLDOM');
            dom.loadXML(xml);
            return dom;
        }
        if (window.DOMParser)
            return new DOMParser().parseFromString(xml, 'text/xml');
        throw new Error('No XML parser available');
    },

    log: function(data) {
        $('#ttLog').append('<div>' + data + '</div>');
    },

    init: function() {
        tt.login();
        $(function() {
            $('#ttLogout').click(function() {
                tt.logout();
            });
        });
    }

};

$(function() {
    $('#ttLogout').click(function() {
        tt.logout()
    })
});

//Begin global.min.js
var disTT = {
    carousel: {
        init: function() {
            (function(D) {
                D.fn.jCarouselLite = function(E) {
                    E = D.extend({ btnPrev: null, btnNext: null, btnGo: null, mouseWheel: false, auto: null, speed: 200, easing: null, vertical: false, circular: true, visible: 3, start: 0, scroll: 1, beforeStart: null, afterEnd: null }, E || {});
                    return this.each(function() {
                        var N = false,
                            L = E.vertical ? "top" : "left",
                            G = E.vertical ? "height" : "width";
                        var F = D(this),
                            P = D("ul", F),
                            I = D("li", P),
                            T = I.size(),
                            S = E.visible;
                        if (E.circular) {
                            P.prepend(I.slice(T - S - 1 + 1).clone()).append(I.slice(0, S).clone());
                            E.start += S;
                        }
                        var R = D("li", P),
                            O = R.size(),
                            U = E.start;
                        F.css("visibility", "visible");
                        R.css({ overflow: "hidden", "float": E.vertical ? "none" : "left" });
                        P.css({ margin: "0", padding: "0", position: "relative", "list-style-type": "none", "z-index": "1" });
                        F.css({ overflow: "hidden", position: "relative", "z-index": "2", left: "0px" });
                        var K = E.vertical ? A(R) : C(R);
                        var Q = K * O;
                        var M = K * S;
                        R.css({ width: R.width(), height: R.height() });
                        P.css(G, Q + "px").css(L, -(U * K));
                        F.css(G, M + "px");
                        if (E.btnPrev) { D(E.btnPrev).click(function() { return J(U - E.scroll); return false; }); }
                        if (E.btnNext) { D(E.btnNext).click(function() { return J(U + E.scroll); return false; }); }
                        if (E.btnGo) { D.each(E.btnGo, function(V, W) { D(W).click(function() { return J(E.circular ? E.visible + V : V); }); }); }
                        if (E.mouseWheel && F.mousewheel) { F.mousewheel(function(V, W) { return W > 0 ? J(U - E.scroll) : J(U + E.scroll); }); }
                        if (E.auto) { setInterval(function() { J(U + E.scroll); }, E.auto + E.speed); }

                        function H() { return R.slice(U).slice(0, S); }

                        function J(V) {
                            if (!N) {
                                if (E.beforeStart) { E.beforeStart.call(this, H()); }
                                if (E.circular) {
                                    if (V <= E.start - S - 1) {
                                        P.css(L, -((O - (S * 2)) * K) + "px");
                                        U = V == E.start - S - 1 ? O - (S * 2) - 1 : O - (S * 2) - E.scroll;
                                    } else {
                                        if (V >= O - S + 1) {
                                            P.css(L, -((S) * K) + "px");
                                            U = V == O - S + 1 ? S + 1 : S + E.scroll;
                                        } else { U = V; }
                                    }
                                } else { if (V < 0 || V > O - S) { return; } else { U = V; } }
                                N = true;
                                P.animate(L == "left" ? { left: -(U * K) } : { top: -(U * K) }, E.speed, E.easing, function() {
                                    if (E.afterEnd) { E.afterEnd.call(this, H()); }
                                    N = false;
                                });
                                if (!E.circular) {
                                    D(E.btnPrev + "," + E.btnNext).removeClass("disabled");
                                    D((U - E.scroll < 0 && E.btnPrev) || (U + E.scroll > O - S && E.btnNext) || []).addClass("disabled");
                                }
                            }
                            return false;
                        }
                    });
                };

                function B(E, F) { return parseInt(D.css(E[0], F)) || 0; }

                function C(E) { return E[0].offsetWidth + B(E, "marginLeft") + B(E, "marginRight"); }

                function A(E) { return E[0].offsetHeight + B(E, "marginTop") + B(E, "marginBottom"); }
            })(jQuery);
        },
        activateToonHQ_toonArt: function() { if ($("div#fanArtCarousel").length > 0) { $("div#fanArtCarousel").jCarouselLite({ btnPrev: "#fanArtMod .prevBtn", btnNext: "#fanArtMod .nextBtn", circular: true, visible: 1, speed: 700, auto: 1800 }); } },
        activateToonHQ_topToons: function() { if ($("div#topToonsCarousel").length > 0) { $("div#topToonsCarousel").jCarouselLite({ btnPrev: "#topToonsMod .prevBtn", btnNext: "#topToonsMod .nextBtn", circular: true, visible: 1, speed: 500, auto: 1800 }); } },
        activateTopToons: function(A) {
            if ($("#carousel").length > 0) {
                $("#carousel").jCarouselLite({
                    btnPrev: ".prevBtn",
                    btnNext: ".nextBtn",
                    circular: true,
                    speed: 500,
                    visible: 1,
                    btnGo: A,
                    beforeStart: function(B) { if (jQuery.browser.msie) { $("div.boxMid960 div.textDesc:visible").hide(); } else { $("div.boxMid960 div.textDesc:visible").fadeOut(); } },
                    afterEnd: function(B) {
                        var C = $("div.boxMid960 div.textDesc." + B.attr("class"));
                        if (jQuery.browser.msie) { C.show(); } else { C.fadeIn(); }
                        $(".selectorContainer span").text(C.find("h2").text());
                    }
                });
            }
        }
    },
    inputStyle: {
        init: function() {
            (function(A) {
                A.fn.filestyle = function(B) {
                    var C = { width: 250 };
                    if (B) { A.extend(C, B); }
                    return this.each(function() {
                        var E = this;
                        var F = A("<div>").css({ width: C.imagewidth + "px", height: C.imageheight + "px", background: "url(" + C.image + ") 0 0 no-repeat", "background-position": "right", display: "inline", position: "absolute", overflow: "hidden" });
                        var D = A('<input class="file">').addClass(A(E).attr("class")).css({ display: "inline", width: C.width + "px" });
                        A(E).before(D);
                        A(E).wrap(F);
                        A(E).css({ position: "relative", height: C.imageheight + "px", width: C.width + "px", display: "inline", cursor: "pointer", opacity: "0.0" });
                        if (A.browser.mozilla) { if (/Win/.test(navigator.platform)) { A(E).css("margin-left", "-142px"); } else { A(E).css("margin-left", "-168px"); } } else { A(E).css("margin-left", C.imagewidth - C.width + "px"); }
                        A(E).bind("change", function() { D.val(A(E).val()); });
                    });
                };
            })(jQuery);
            $("input[type=file]").filestyle({ image: ttown.getCDN() + "en/global/img/btns/browse-off.gif", imageheight: 34, imagewidth: 95, width: 190 });
        }
    },
    uniform: {
        init: function() {
            (function(A) {
                A.uniform = { options: { selectClass: "selector", radioClass: "radio", checkboxClass: "checker", checkedClass: "checked", focusClass: "focus" } };
                if (A.browser.msie && A.browser.version < 7) { A.selectOpacity = false; } else { A.selectOpacity = true; }
                A.fn.uniform = function(C) {
                    C = A.extend(A.uniform.options, C);

                    function B(G) {
                        var H = A("<div />"),
                            F = A("<span />");
                        H.addClass(C.selectClass);
                        F.html(G.children(":selected").text());
                        G.css("opacity", 0);
                        G.wrap(H);
                        G.before(F);
                        H = G.parent("div");
                        F = G.siblings("span");
                        G.change(function() { F.text(G.children(":selected").text()); }).focus(function() { H.addClass(C.focusClass); }).blur(function() { H.removeClass(C.focusClass); });
                    }

                    function D(G) {
                        var H = A("<div />"),
                            F = A("<span />");
                        H.addClass(C.checkboxClass);
                        A(G).wrap(H);
                        A(G).wrap(F);
                        F = G.parent();
                        H = F.parent();
                        A(G).css("opacity", 0).focus(function() { H.addClass(C.focusClass); }).blur(function() { H.removeClass(C.focusClass); }).click(function() { if (!A(G).attr("checked")) { F.removeClass(C.checkedClass); } else { F.addClass(C.checkedClass); } });
                        if (A(G).attr("checked")) { F.addClass(C.checkedClass); }
                    }

                    function E(G) {
                        var H = A("<div />"),
                            F = A("<span />");
                        H.addClass(C.radioClass);
                        A(G).wrap(H);
                        A(G).wrap(F);
                        F = G.parent();
                        H = F.parent();
                        A(G).css("opacity", 0).focus(function() { H.addClass(C.focusClass); }).blur(function() { H.removeClass(C.focusClass); }).click(function() {
                            if (!A(G).attr("checked")) { F.removeClass(C.checkedClass); } else {
                                A("." + C.radioClass + " span." + C.checkedClass).removeClass(C.checkedClass);
                                F.addClass(C.checkedClass);
                            }
                        });
                        if (A(G).attr("checked")) { F.addClass(C.checkedClass); }
                    }
                    return this.each(function() { if (A.selectOpacity) { var F = A(this); if (F.is("select")) { B(F); } else { if (F.is(":checkbox")) { D(F); } else { if (F.is(":radio")) { E(F); } } } } });
                };
            })(jQuery);
            disTT.uniform.activate();
        },
        activate: function() { $("select#topToonSelector").uniform(); }
    },
    navSelect: {
        getUrlVars: function() {
            var C = [],
                B;
            var A = window.location.href.slice(window.location.href.indexOf("?navId") + 1).split("&");
            B = A[0].split("=");
            C.push(B[0]);
            C[B[0]] = B[1];
            return C;
        }
    }
};
(function(B) {
    var A = [];
    B.preLoadImages = function() {
        var E = arguments.length;
        for (var D = E; D--;) {
            var C = document.createElement("img");
            C.src = arguments[D];
            A.push(C);
        }
    };
})(jQuery);
(function(A) {
    A.fn.lazyload = function(B) {
        var C = { threshold: 0, failurelimit: 0, event: "scroll", effect: "show", container: window };
        if (B) { A.extend(C, B); }
        var D = this;
        if ("scroll" == C.event) {
            A(C.container).bind("scroll", function(G) {
                var E = 0;
                D.each(function() { if (A.abovethetop(this, C) || A.leftofbegin(this, C)) {} else { if (!A.belowthefold(this, C) && !A.rightoffold(this, C)) { A(this).trigger("appear"); } else { if (E++ > C.failurelimit) { return false; } } } });
                var F = A.grep(D, function(H) { return !H.loaded; });
                D = A(F);
            });
        }
        return this.each(function() {
            var E = this;
            A(E).data("original", A(E).attr("src"));
            if ("scroll" != C.event || (A.abovethetop(E, C) || A.leftofbegin(E, C) || A.belowthefold(E, C) || A.rightoffold(E, C))) {
                if (C.placeholder) { A(E).attr("src", C.placeholder); } else { A(E).removeAttr("src"); }
                E.loaded = false;
            } else { E.loaded = true; }
            A(E).one("appear", function() {
                if (!this.loaded) {
                    A("<img />").bind("load", function() {
                        A(E).hide().attr("src", A(E).data("original"))[C.effect](C.effectspeed);
                        E.loaded = true;
                    }).attr("src", A(E).data("original"));
                }
            });
            if ("scroll" != C.event) { A(E).bind(C.event, function(F) { if (!E.loaded) { A(E).trigger("appear"); } }); }
        });
    };
    A.belowthefold = function(C, D) { if (D.container === undefined || D.container === window) { var B = A(window).height() + A(window).scrollTop(); } else { var B = A(D.container).offset().top + A(D.container).height(); } return B <= A(C).offset().top - D.threshold; };
    A.rightoffold = function(C, D) { if (D.container === undefined || D.container === window) { var B = A(window).width() + A(window).scrollLeft(); } else { var B = A(D.container).offset().left + A(D.container).width(); } return B <= A(C).offset().left - D.threshold; };
    A.abovethetop = function(C, D) { if (D.container === undefined || D.container === window) { var B = A(window).scrollTop(); } else { var B = A(D.container).offset().top; } return B >= A(C).offset().top + D.threshold + A(C).height(); };
    A.leftofbegin = function(C, D) { if (D.container === undefined || D.container === window) { var B = A(window).scrollLeft(); } else { var B = A(D.container).offset().left; } return B >= A(C).offset().left + D.threshold + A(C).width(); };
    A.extend(A.expr[":"], { "below-the-fold": "$.belowthefold(a, {threshold : 0, container: window})", "above-the-fold": "!$.belowthefold(a, {threshold : 0, container: window})", "right-of-fold": "$.rightoffold(a, {threshold : 0, container: window})", "left-of-fold": "!$.rightoffold(a, {threshold : 0, container: window})" });
})(jQuery);

function setCurrentDisplay(A) {
    $(".displaybDetail").css("display", "none");
    $(".displaybDetailLi").removeClass("selected");
    $("#displayDetail_" + A).show();
    $("#displayDetailLi_" + A).addClass("selected");
}
$(document).ready(function() {
    tt.init();
    $.preLoadImages(ttown.getCDN() + "/en/global/img/btn_playNowOvr.png");
});
//End global.min.js