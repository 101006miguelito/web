var mySettings = {
  chromeStyle: 'blue',
  footerStyleSet: 'transparentLight',
  footerDisplayMode: '',
  footerLegalSiteMapTarget:	'_top',
  footerLegalSafetyTarget: '_top',
  footerLegalPrivacyPolicyTarget: '_top',
  footerLegalIBATarget: '_top',
  footerLegalCustom: '<br /><br /><a href="'+ttown.getURL()+'member-agreements">Member Agreements</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="'+ttown.getURL_SSL()+'membership/account-management/">Member/Guest Services</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="'+ttown.getURL()+'help/">Help</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="'+ttown.getURL()+'help/contact">Contact Us</a>'
};

$(window).load(function (){
  if ( tt.getCookie('prospect') != '-1' ){
    var myDate = new Date();
    myDate.setDate(myDate.getDate()+365);
    tt.setCookie('prospect', '-1',myDate);
  }
});