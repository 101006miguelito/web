function getUserAgent(){
  return navigator.userAgent.toLowerCase();
}

function brwsrParams(qp,dflt)
{
  var ua=getUserAgent();
  dflt=(dflt==null)?-1:dflt;
  try{
    r=unescape(ua.match(new RegExp(qp+"( |\:|\/)+([^&;\) ]*)"))[2]);
  } catch(qp) { r=dflt; }
  return r;
}

function getMacOS(){
  var os='-1';//unknown

  ua=navigator.userAgent.toLowerCase();
  var testMacOS = ua.match(/mac os x/);
  //example:  mac os x 10.4.11

  if (testMacOS==null || testMacOS==''){
    return os;
  }else{

    var macOSVer = ua.match(/mac os x [0-9]{2}.[0-9]{0,}.[0-9]{0,}/);
    if (macOSVer != null && macOSVer != '') {
        var testStr = macOSVer[0];//convert object to string for string manipulation
        testStr = testStr.substr(9,testStr.length-9);//remove 'mac os x ' from string to get Mac OS X version
        testStr = testStr.replace(/_/,'.');//replace underscore with period
        testStr = testStr.replace(/_/,'.');//replace underscore with period
       testStr = testStr.replace(/;/,'');//replace semicolon with nada for FF

       if (testStr != null || testStr !=''){
         return testStr
       }
    }

    os='10.4.6'; //assume version is acceptable
    return os;
  }
}

function userAgentOS()
{
  var os='unknown';
  var ua=getUserAgent();

  if (ua.indexOf("win") != -1)
  {
    if (ua.search(/nt\s6\.1/) != -1){
      os="windows7";
      return os;
    } else if (ua.search(/nt\s6\.0/) != -1){
      os="vista";
      return os;
    } else if (ua.search(/nt\s5\.1/) != -1){
      os="xp";
      return os;
    } else if (ua.search(/nt\s5\.0/) != -1){
      os="2000";
      return os;
    } else if ( (ua.search(/win98/) != -1) || (ua.search(/windows\s98/)!=-1 ) ){
      os="98";
      return os;
    } else if (ua.search(/windows\sme/) != -1){
      os="me";
      return os;
    } else if (ua.search(/nt\s5\.2/) != -1){
      os="win2k3";
      return os;
    } else if ( (ua.search(/windows\s95/) != -1) || (ua.search(/win95/)!=-1 ) ){
      os="95";
      return os;
    } else if ( (ua.search(/nt\s4\.0/) != -1) || (ua.search(/nt4\.0/) ) !=-1){
      os="nt4";
      return os;
    } else {
      return os;
      }
  }
  else if ( (navigator.platform == 'MacIntel') || (navigator.platform == 'MacPPC') ){
      MacOSX = getMacOS();
      return MacOSX;
  }
  else {
    return os;
  }
}

function getMinAppleWebKit(){
  return 418;//based on OS X 10.4.6
}

function getAppleWebKit()
{
  var kitName = "applewebkit/";
  var tempStr = navigator.userAgent.toLowerCase();
  var pos = tempStr.indexOf(kitName);
  var isAppleWebkit = (pos != -1);

  if (isAppleWebkit) {
    var kitVersion = tempStr.substring(pos + kitName.length,tempStr.length);
    kitVersion = kitVersion.substring(0,kitVersion.indexOf(" "));
    return kitVersion;
  } else {
    return -1;
  }
}

function detectUA()
{
  var ua=[];
  ua['browser'] = 'unknown';
  ua['browserVer'] = -1;
  ua['version'] = -1;
  ua['appleWebKit'] = -1;
  ua['os'] = 'unknown';
  ua['strippedAppVersion'] = -2;
  ua['verStrippedApp'] = -1;
  ua['platform']=navigator.platform;

  var msie = brwsrParams("msie",0);
  var opera = brwsrParams("opera",0);
  var intelMac = brwsrParams("intelmac",0);
  var firefox = brwsrParams("firefox",0);
  var netscape = brwsrParams("netscape",0);
  var chrome = brwsrParams("chrome",0);
  var safari = brwsrParams("safari",0);
  var version = brwsrParams("version",0);

  var minAppleWebKit = getMinAppleWebKit();
  ua['appleWebKit'] = getAppleWebKit();

  if(msie){
    ua['browser'] = "IE"
    ua['strippedAppVersion'] =brwsrParams("msie");
  }
  else if(opera){
    ua['browser'] = "Opera";
    ua['strippedAppVersion'] = brwsrParams("opera");
  }
  else if(firefox){
    ua['browser'] = "Firefox";
    ua['strippedAppVersion'] = brwsrParams("firefox");
  }
  else if(netscape){
    ua['strippedAppVersion'] = brwsrParams("netscape");
    ua['browser'] = "Netscape";
  }
  else if (chrome){
    //this check must come before Safari detection since useragent has Safari as well
    //example: Chrome/2.0.172.33 Safari/530.5
    ua['browser'] = "Chrome";
    ua['strippedAppVersion'] = brwsrParams("chrome");
  }
  else if(safari){
    ua['browser'] = "Safari";
    ua['strippedAppVersion'] = brwsrParams("safari");

    //detect version of Safari
    //version appears in useragent in Safar 3 and 4 but not in Safari 1 and 2
    if (version){
      verStrippedApp = brwsrParams("version");
      if (verStrippedApp){
        ua['strippedAppVersion'] = verStrippedApp;
      }
    }else if (ua['appleWebKit'] >= minAppleWebKit){
      ua['strippedAppVersion']=2.0;
    }else {
      ua['strippedAppVersion']=1;
    }
  }

  var browserVer = parseFloat(ua['strippedAppVersion']);
  if (isNaN(browserVer)==false){
    ua['browserVer']=browserVer;
  }

  ua['os']=userAgentOS();

  return ua;
}

//Begin Cookies
function setcookie(name,value,expires,path,domain,secure){
	// If we're passing an expires variable, cast it (if necessary) to a Date
	if (expires){
		if (typeof(expires) == "number" || typeof(expires) == "string"){
			expires = new Date(expires);
		}
	}
	
	var str = '';
	
	// Check if is array then serialize
	if(value.constructor == Array) {
		for (item in value) {
			str += (str) ? '&' : '';
			str += escape(item) + '=' + escape(value[item]);
		} 
		value = str;
	}
//    alert("the cookie string is '" + name + "=" + (value) + ((expires) ? "; expires=" + expires.toUTCString() : "") + ((path) ? "; path=" + path : "; path=/") + ((domain) ? "; domain=" + domain : "; domain=.go.com") +   ((secure) ? "; secure" : "") + "'");
    document.cookie = name + "=" + (value) + ((expires) ? "; expires=" + expires.toUTCString() : "") + ((path) ? "; path=" + path : "; path=/") + ((domain) ? "; domain=" + domain : "; domain=.go.com") +   ((secure) ? "; secure" : "");
}
function getcookie(c_name){
  if(document.cookie.length > 0){
    var c_start=document.cookie.indexOf(c_name + "=");
    if(c_start != -1){
      c_start=c_start + c_name.length + 1;
      var c_end=document.cookie.indexOf(";",c_start);
      if(c_end == -1)
          c_end=document.cookie.length;
      str = unescape(document.cookie.substring(c_start, c_end));
	  // If an array, then break up into array
	  /*if(str.indexOf("&")) {
		value = new Array();
		// Construct associate array of stored values
		arr = str.split('&');
		for (i=0;i<arr.length;i++) {
			item = arr[i].split('=');
			
			value[item[0]] = item[1];
		} 
		str = value;
	  }*/
	  return str;
    }
  }
  return null;
}
function removecookie(cookieName){
  var cookieDate=new Date ( );  // current date & time
  cookieDate.setTime ( cookieDate.getTime() - 1 );
  document.cookie=cookieName += "=; expires=" + cookieDate.toGMTString();
}
//End Cookies

// Get querystring variables
function getQuerystring(name)
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

// Page redirect 
function redirectURL(url,popup) {
	var queryString = '';

	// Detect if there is a distribution partner set, add query to each redirect url for detection
	sitePartner = getQuerystring('source');
	if(sitePartner != '') {
		queryString = '?source='+sitePartner;
	//alert(queryString);
	}
  
  window.location.replace(url+queryString);
}

function ttPlay(ua){
  
  //alert('browser: '+ua['browser']+"\n"+'browserVer: '+ua['browserVer']+"\n"+'strippedAppVersion: '+ua['strippedAppVersion']+"\n"+'version: '+ua['version']+"\n"+'verStrippedApp: '+ua['verStrippedApp']+"\n"+'appleWebKit: '+ua['appleWebKit'] +"\n"+'platform: '+ua['platform']+"\n"+'os: '+ua['os'] + "\n" + 'ua: '+ navigator.userAgent.toLowerCase());

  var Launcher_URL = '/game/downloader';
  var EULA_URL = '/eula';
  var IE_Installer = '/game/ie-install';
  
  var MAC_Installer = '/game/safari-install';
  var MAC_Firefox = '/game/ff-install';

  var Unsupported_URL = '/game/browser-not-supported';
  
  var MAC_FAQ = '/help/faq/technical/mac';

  var PC_FAQ = '/help/faq/technical/windows-pc';

  if (ua['platform']=='Win32'){
	if (getcookie('eula')=='1'){ // EULA check
		if (ua['browser']=='IE' && ua['browserVer'] >=6){
		  if (ua['os']=='98' || ua['os']=='me' || ua['os']=='2000' || ua['os']=='xp' || ua['os']=='vista' || ua['os']=='windows7'){
			  redirectURL(IE_Installer);
		  }else{
			 redirectURL(Launcher_URL);
		  }
		}
		// firefox
		else{
		  redirectURL(Launcher_URL);
		}
	} else {
		redirectURL(EULA_URL);	// EULA Redirect	
	}
  } else if (ua['platform']=='MacIntel' || ua['platform']=='MacPPC'){
      //compare MacOSX version with MinMacOSX

      var minMacOSX_Met = false;
      var minSafari_Met = false;

      //minMacOSX
      var minMacOSX = '10.4.6';
      var minMac1st = 10;
      var minMac2nd = 4;
      //var minMac3rd = 6;//this check fails on Mac OS X 10.4.6 with FF 3.0.16
      var minMac3rd = 0;//work around for above
      var minMacOSX_1st_find = -1;//unknown
      var minMacOSX_2nd_find = -1;//unknown

      var MacOSX = ua['os']
      var Mac1st = 0;//unknown
      var Mac2nd = 0;//unknown
      var Mac3rd = 0;//unknown
      var MacOSX_1st_find = -1;//unknown
      var MacOSX_2nd_find = -1;//unknown

      var find = '.';//search for this string in Mac OS X number

      //MacOSX
      MacOSX_1st_find = MacOSX.indexOf(find);
      MacOSX_2nd_find = MacOSX.lastIndexOf(find);

      if (MacOSX_1st_find == 0 && MacOSX_2nd_find == -1){
        Mac1st  = MacOSX;
      } else {

        Mac1st = MacOSX.substr(0,MacOSX_1st_find);

        if (MacOSX_1st_find != 0 && (MacOSX_1st_find == MacOSX_2nd_find)){
          Mac2nd = MacOSX.substr(MacOSX_1st_find + 1,MacOSX.length - MacOSX_1st_find - 1);
        }else{
          Mac2nd = MacOSX.substr(MacOSX_1st_find + 1,MacOSX_2nd_find - MacOSX_1st_find - 1);
          Mac3rd = MacOSX.substr(MacOSX_2nd_find + 1,MacOSX.length - MacOSX_2nd_find - 1);
        }
      }

      if (Mac1st > minMac1st){
        //example: 11  > 10
        minMacOSX_Met = true;
      } else if (Mac1st == minMac1st ){
        //exampple: 10 == 10

          if ( Mac2nd > minMac2nd ){
              //10.5 > 10.4
              minMacOSX_Met = true;
          }else if (Mac2nd == minMac2nd){
            //example:  10.4 == 10.4
            if (Mac3rd >= minMac3rd){
              //example: 10.4.6 == 10.4.6
              //example: 10.4.7 > 10.4.6
              minMacOSX_Met = true;
            }
          }
      }

      if (minMacOSX_Met){
        if (getcookie('eula')=='1'){ // EULA check
			if (ua['browser']=='Safari' && ua['browserVer'] >=3){
			  redirectURL(MAC_Installer);
			}else if (ua['browser']=='Firefox' && ua['browserVer'] >=3 && ua['platform']!='MacPPC'){
			  redirectURL(MAC_Firefox);
			}else{
			  redirectURL(Unsupported_URL);
			}
		} else {
			redirectURL(EULA_URL);	// EULA Redirect	
		}
      }else{
        redirectURL(Unsupported_URL);
      }

  }else{
    redirectURL(Unsupported_URL);
  }
}

