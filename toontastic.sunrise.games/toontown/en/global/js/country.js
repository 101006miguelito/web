$(document).ready(function(){

    // Set expiration date
    var myDate = new Date();
    myDate.setDate(myDate.getDate()+365); // expire 1 year from now

  $('#usLink').click( function(){
    tt.delCookie('country');
    tt.setCookie('country','us',myDate);
    usLink();
  });

  $('#caLink').click( function(){
    tt.delCookie('country');
    tt.setCookie('country','ca',myDate);
    usLink();
  });

  $('#brLink').click( function(){
    tt.delCookie('country');
    tt.setCookie('country','br',myDate);
    brLink();
  });

  $('#ukLink').click( function(){
    tt.delCookie('country');
    tt.setCookie('country','uk',myDate);
    ukLink();
  });

  $('#jpLink').click( function(){
    tt.delCookie('country');
    tt.setCookie('country','jp',myDate);
    jpLink();
  });
});