var util = {

  emailErrTxt: 'Please enter your email address',
  emailFormatErrTxt: 'Please enter your email address in the following format xxx@xxx.xxx',
  acctIdErrTxt: 'Please enter your Account ID',
  descErrTxt: 'Please enter your description',
  screenshotErrTxt: 'Sorry, we only accept screenshots in the following formats: .JPG, .GIF, .BMP or .PNG',
  waitTxt: 'Please wait while we process your request',
  requiredFieldsTxt: 'Fields with a red * are required.',
  termsErrTxt: 'Please click on the checkbox to agree to the Terms of Use',

  getEmailErrTxt: function(){
    return this.emailErrTxt;
  },

  getEmailFormatErrTxt: function(){
    return this.emailFormatErrTxt;
  },

  getAcctIdErrTxt: function(){
    return this.acctIdErrTxt;
  },

  getDescErrTxt: function(){
    return this.descErrTxt;
  },

  getScreenshotErrTxt: function(){
    return this.screenshotErrTxt;
  },

  getWaitTxt: function(){
    return this.waitTxt;
  },

  getRequiredFieldsTxt: function(){
    return this.requiredFieldsTxt;
  },

  getTermsErrTxt: function(){
    return this.termsErrTxt;
  },

  validEmail: function(id){
    var emailReg = "^[\\w-_\.+]*[\\w-_\.]\@([\\w]+\\.)+[\\w]+[\\w]$";
    var regex = new RegExp(emailReg);
    try{
      return regex.test(id);
    }
    catch(err){
      return false;
    }
  },

  imgType: function(file){
    try{
      file=file.toLowerCase();
      var validTypes='.jpg, .gif, .bmp, .png';
      if (file != '' && file != ' '){
        var posLast=file.lastIndexOf('.');
        var fileType=file.substring(posLast);
        if (validTypes.indexOf(fileType)>-1){
          return true;
        }else{
        return false;
        }
      }else{
        return false;
      }
    }
    catch(err){
      return false;
    }
  }


};
