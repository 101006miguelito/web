// Panda Class -----------------------------------------[TG]
var PandaAPI = function(config) {
	var This = function() {

		// Private Vars ----------------------------------------[TG]
		var loader			= false;
		var pythonLoaded	= false;
		var downloading		= false;
		var updateCallback	= false;
		//------------------------------------------------------[TG]

		// Public Vars -----------------------------------------[TG]
		This._config = {};

		This.gameData = {
			GAME_SERVER				: false,
			IS_TEST_SERVER			: false,
			REFERRER_CODE			: false,
			api						: false,
			GAME_ENVIRONMENT		: false,
			IP						: false,
			DISLTOKEN				: false,
			PAID					: false,
			GAME_USERNAME			: false,
			GAME_SHOW_ADDS			: false,
			GAME_INGAME_UPGRADE		: false,
			GAME_INGAME_MOREINFO	: false,
			GAME_INGAME_NAMING		: false,
			GAME_INGAME_MANAGE_ACCT	: false
		};
		//------------------------------------------------------[TG]

		// Constructor -----------------------------------------[TG]
		function PandaAPI() {
			$.extend(This._config, config);

			return This;
		}
		//------------------------------------------------------[TG]

		// Public Functions ------------------------------------[TG]
		This.loaderReady = function() {
			return (loader) ? true : false;
		}
		This.setGameData = function(data) {
			$.extend(This.gameData, data);
		}
		This.setUpdateCallback = function(updateStateFunction) {
			updateCallback = updateStateFunction;
		}
		This.getCoreData = function(key) {
			var vaildCore		= ['status', 'pluginVersionString', 'pluginMajorVersion', 'pluginMinorVersion', 'pluginSequenceVersion', 'pluginNumericVersion', 'pluginDistributor', 'coreapiHostUrl', 'coreapiTimestamp', 'coreapiTimestampString', 'totalDownloadSize'];
			var vaildDownload	= ['downloadProgress', 'downloadPackageName', 'downloadPackageDisplayName', 'downloadComplete', 'downloadRemainingSeconds', 'downloadRemainingFormatted'];

			if (!pluginLoaded() || ( ('|' + vaildCore.join('|') + '|').indexOf('|' + key + '|') == -1 && ('|' + vaildDownload.join('|') + '|').indexOf(key) == -1))
				return false;

			if (( ('|' + vaildDownload.join('|') + '|').indexOf(key) != -1 && !downloading))
				return false;

			return loader.main[key];
		}
		This.getLog = function(name) {
			if (!pluginLoaded() || !loader.main)
				return false;

			return loader.main.read_log( name, ((arguments[1]) ? arguments[1] : 131072), ((arguments[2]) ? arguments[2] : 0), ((arguments[3]) ? arguments[3] : 0) );
		}
		This.getSystemLog = function() {
			if (!pluginLoaded() || !loader.main)
				return false;

			return loader.main.read_system_log( (arguments[0]) ? arguments[0] : 131072 );
		}
		This.getGameLog = function() {
			var game = document.getElementById(This._config['gameID']);

			if (!pluginLoaded() || !pythonLoaded || !game.main)
				return false;

			return game.main.read_game_log( ((arguments[0]) ? arguments[0] : 131072), ((arguments[1]) ? arguments[1] : 0), ((arguments[2]) ? arguments[2] : 0) );
		}
		This.onPluginLoad = function() {
			log('Panda: onPluginLoad');

			loader = document.getElementById(This._config['loaderID']);
			if (!This.isCurrentVersion()) {
				updateState('Error', 'Not Current Version');
			}
		}
		This.onUnAuth = function() {
			log('Panda: onUnAuth');

			if (!pluginLoaded())
				return false;

			loader.main.play();
		}
		This.onAuth = function() {
			log('Panda: onAuth');

			if (This.isCurrentVersion()) {
				log('Calling Play');
				loader.main.play();
			}
		}
		This.onDownloadBegin = function() {
			log('onDownloadBegin');

			if (!pluginLoaded()) {
				setTimeout(arguments.callee, 10);
				return false;
			}

			downloading = true;
			updateState('Loading');
		}
		This.onDownloadNext = function() {
			log('Plugin State: onDownloadNext');
		}
		This.onDownloadComplete = function() {
			log('Plugin State: onDownloadComplete');
			downloading = false;
			updateState('Done Loading');
		}
		This.onReady = function() {
			log('Plugin State: onReady');

			if (This.isCurrentVersion()) {
				updateState('Ready');
			}
		}
		This.onPythonLoad = function() {
			log('Plugin State: onPythonLoad');
			pythonLoaded = true;
		}
		This.onWindowOpen = function() {
			log('Plugin State: onWindowOpen');
			updateState('Window Mode');
		}
		This.onPluginFail = function() {
			log('Plugin State: onPluginFail');
			updateState('Error', 'Plugin Failed');
		}
		This.onPhythonStop = function() {
			log('Plugin State: onPhythonStop');
			updateState('Exit');
		}
		This.pluginInstalled = function() {
			navigator.plugins.refresh();
			if (navigator.plugins && navigator.plugins.length) {
				for (var i in navigator.plugins) {
					if (typeof navigator.plugins[i] == 'undefined' || typeof navigator.plugins[i].name != 'string')
						continue;

					if (navigator.plugins[i].name && navigator.plugins[i].name.toLowerCase().indexOf('panda3d') != -1) {
						return true;
					}
				}
			} else if (window.ActiveXObject) {
				try {
					if (new ActiveXObject('P3DACTIVEX.P3DActiveXCtrl.1')) {
						return true;
					}
				} catch(e) {}
			}
			return false;
		}
		This.supportedBrowser = function() {
			/*	PC:  FF 3.0+, IE 6+, Safari 4+, Chrome 3+
				MAC: Safari 4+ */
			if ($.browser.safari && parseInt($.browser.version.substr(0,1)) >= 3) {
				return true;
			} else if ($.browser.mozilla && !$.os.ppc && parseInt($.browser.version.substr(0,1)) >= 3) {
				return true;
			} else if ($.browser.msie && parseInt($.browser.version.substr(0,1)) >= 6) {
				return true;
			}
			return false;
		}
		This.isCurrentVersion = function() {
			if (This.getCoreData('pluginNumericVersion')) {
				version = [This.getCoreData('pluginMajorVersion'), This.getCoreData('pluginMinorVersion'), This.getCoreData('pluginSequenceVersion')];
			} else if (This.pluginInstalled() && $.cookie('panda_version')) {
				version = $.cookie('panda_version').split('|');
			} else {
				version = [0,0,0];
			}

			$.cookie('panda_version', '', { expires: -1 });
			$.cookie('panda_version', '', { expires: -1, path: '/' });
			$.cookie('panda_version', '', { expires: -1, path: '/game/' });
			$.cookie('panda_version', '', { expires: -1, path: '/' });
			$.cookie('panda_version', '', { expires: -1, path: '/game/' });
			$.cookie('panda_version', '', { expires: -1, path: '/' });
			$.cookie('panda_version', version.join('|'), { expires: 730, path: '/' });

			if (This._config['currentVersion'][0] > version[0]) {
				return false;
			} else if (This._config['currentVersion'][0] < version[0]) {
				return true;
			} else if (This._config['currentVersion'][1] > version[1]) {
				return false;
			} else if (This._config['currentVersion'][1] < version[1]) {
				return true;
			} else if (This._config['currentVersion'][2] > version[2]) {
				return false;
			}
			return true;
		}
		This.embed = function(path, id, width, height, params) {
			if (!This.pluginInstalled() && !arguments[5])
				return false;

			log('Comparing versions: Cookie '+$.cookie('panda_version')+' vs. Panda:'+This.isCurrentVersion());
			if ($.cookie('panda_version') && !This.isCurrentVersion())
				updateState('Error', 'Not Current Version');

			var str	= '';

			if ($.browser.msie) {
				str += '<object classid="CLSID:924B4927-D3BA-41EA-9F7E-8A89194AB3AC" codebase="http://panda-plugin.disney.go.com/plugin/win32/p3dactivex.cab#Version=' + This._config.currentVersion.join(',') + '" width="' + width + '" height="' + height + '" id="' + id + '" class="' + id + '">' + "\n";
					str += '<param name="data" value="' + path + '" />' + "\n";
					str += '<param name="type" value="application/x-panda3d" />' + "\n";
					for (name in params) {
						str += '<param name="' + name + '" value="' + params[name] + '" />' + "\n";
					}
				str += '</object>' + "\n";
			} else {
				str += '<embed src="' + path + '" id="' + id + '" class="' + id + '"' + "\n";
					str += 'type="application/x-panda3d"' + "\n";
					str += 'width="' + width + '"' + "\n";
					str += 'height="' + height + '"' + "\n";
					for (name in params) {
						str += name + '="' + params[name] + '"' + "\n";
					}
				str += '></embed>' + "\n";
			}
			$(document).ready(function() {
				$('#' + id).replaceWith(str);
			});

			log('Panda Embeded');

			return true;
		}
		This.install = function() {
			log('Installing...');
			if (!This.pluginInstalled()) {
				This.startInstall();
			} else {
				log('waiting for loader to check version');
				TPI.queueCall(This.loaderReady, This.startInstall);
			}
			updateState('Installing');
		}
		This.startInstall = function() {
			log('starting install');
			if (!This.isCurrentVersion()) {
				log('Checking if plugin Installed: ' +  This.pluginInstalled());
				log('Current Version: ' + This.isCurrentVersion());
				if ($.browser.msie) {
					log('Installing IE Version');
					// Create Hidden Object
					$('body').append('<div id="install_activex"></div>');
					This.embed('#', 'install_activex', 0, 0, {}, true);
				} else if ($.browser.mozilla) {
					log('Installing Firefox Version');
					var params = {
							"Panda3D Installer": {
							URL: 'https://panda-plugin.disney.go.com/plugin/firefox/nppanda3d.xpi',
							IconURL: 'https://toontastic.sunrise.games/toontown/favicon.ico',
							toString: function () { return this.URL; }
						}

					};
					InstallTrigger.install(params);
				} else {
					// send to download page
				}
			}
		}
		This.uninstall = function(type) {
			var uninstall = $('#panda_uninstall').get(0);

			if (uninstall && uninstall.main && confirm('Are you sure you would like to uninstall this?'))
				uninstall.main.uninstall(type);
			else
				return false;

			updateState('Uninstalled');

			return true;
		}
		This.download = function() {
			if (navigator.userAgent.toLowerCase().indexOf('mac') != -1) {
				location.href = 'http://panda-plugin.disney.go.com/plugin/osx_fat/p3d-setup.dmg';
			}
			else {
				location.href = 'http://panda-plugin.disney.go.com/plugin/win32/p3d-setup.exe';
			}
		}
		//------------------------------------------------------[TG]

		// Private Functions -----------------------------------[TG]
		function pluginLoaded() {
			return (loader.main) ? true : false;
		}
		function updateState() {
			if (updateCallback) {
				updateCallback.apply(this, arguments);
			}
		}
		function log(data) {
			$('#log').append('<div>' + data + '</div>');
		}
		//------------------------------------------------------[TG]

		// Send Back Reference ---------------------------------[TG]
		return PandaAPI();
		//------------------------------------------------------[TG]

	};
	return This();
};
//------------------------------------------------------[TG]

// Pirates Panda Class ---------------------------------[TG]
var ToontownPandaInterface = function(config) {
	var This = function() {

		// Private Vars ----------------------------------------[TG]
		var panda				= false;
		var launcher			= false;
		var game				= false;
		var state				= false;
		var ready				= false;
		var launcherEmbeded		= false;
		var progressOverlayShown= false;
		var BASEURL				= false;
		var downloadPercent		= 0;
		//------------------------------------------------------[TG]

		// Public Vars -----------------------------------------[TG]
		This._config = {};
		//------------------------------------------------------[TG]

		// Constructor -----------------------------------------[TG]
		function ToontownPandaInterface() {
			$.extend(This._config, config);

			panda = This._config.panda;
			panda.setUpdateCallback(This.updateState);

			launcher	= $('#' + This._config.launcher);
			game		= $('#' + This._config.game);

			log('Begin interface:');
			if (!panda.pluginInstalled()) {
				This.updateState('Error', 'No Plugin Found');
			} else if (!panda.supportedBrowser()) {
				This.updateState('Error', 'Not Supported');
			} else {
				This.updateState('Init');
			}

			return This;
		}
		//------------------------------------------------------[TG]

		// Public Functions ------------------------------------[TG]
		This.updateState = function(state) {
			This.state = state;

			log('State: '+state);
			switch (state) {
				case 'Init':
					log('Plugin installed on a supported browser. Done');
					if((location.href.indexOf('downloader') == -1) && panda.pluginInstalled() && DETECTONLY != true && (location.href.indexOf('uninstall') == -1)) {
						redirectURL(This._config['links']['dock']);
					}
				break;

				case 'Installing':
					This.queueCall(This.playButtonReady, pullForPlugin);
				break;

				case 'Loading':
					updateProgress();
				break;

				case 'Done Loading':

				break;

				case 'Ready':
					showGame();
				break;

				case 'Window Mode':
					game.css({
						background: '#ffffff url(' + This._config.images['close'] + ') center center no-repeat'
					});
				break;

				case 'Error':
					switch (arguments[1]) {
						case 'No Plugin Found':
							log('No Plugin Installed');
							This.queueCall(This.playButtonReady, This.redirectToInstallPage);
							if(!DETECTONLY) panda.startInstall();
						break;

						case 'Plugin Failed':
						case 'Not Current Version':
							log('failed to load plugin');
							This.queueCall(This.playButtonReady, updatePlayToInstallPage);
							This.redirectToInstallPage();
						break;

						case 'Not Supported':
							log('Browser/OS Not Supported');
							This.queueCall(This.playButtonReady, notSupported);
						break;
					}
				break;

				case 'Uninstalled':
					alert('Toontown Online has been uninstalled');
					location.href = This._config['links']['logout'];
				break;

				case 'Exit':
					log('Exiting');
					exit();
				break;

				default:
					This.state = 'Init';
				break;
			}
		}
		This.isReady = function() {
			return ready;
		}
		This.playButtonReady = function() {
			log('Play button ready');
			return true;
		}
		This.updateImages = function() {
			log('Updating...');
			if (!ready) {
				log('Not Ready');
				return;
			}

			//launcher.find('.progress-container').hide();

			This.queueCall(This.playButtonReady, updatePlayButton);

			if(location.href.indexOf('downloader') != -1) {
				if ($('#play_popup').length < 1) {

					// Update to 100%
					launcher.find('.progress').html('Finished Loading');
					launcher.find('.filler').css('width', '100%');

					if($.browser.msie) {
						launcher.find('#overlay').css('display','block');
						launcher.find('#overlay-play').css('display','block');
					} else {
						launcher.find('#overlay').fadeIn("fast", function() {
							launcher.find('#overlay-play').fadeIn("slow");
						});
					}

				}
			}

			log('Game is Ready');

			if (game.length) {
				game.find('.loading').remove();
				game.find('.panda_game').show();
			}
		}
		This.closePopup = function() {
			$('#play_popup').hide();
		}
		This.embedLauncher = function() {
			log('Embedding Launcher');
			var embeded = panda.embed(This._config['p3d'], 'panda_loader', 0, 0, {
				download			: 1,
				auto_install		: 0,
				hidden				: 1,
				alt_host			: This._config['alt_host'],
				log_history			: 3,
				onPluginLoad		: 'PND.onPluginLoad()',
				onDownloadBegin		: 'PND.onDownloadBegin()',
				onDownloadNext		: 'PND.onDownloadNext()',
				onDownloadComplete	: 'PND.onDownloadComplete()',
				onPythonLoad		: 'PND.onPythonLoad()',
				onUnAuth			: 'PND.onUnAuth()',
				onAuth				: 'PND.onAuth()',
				onPluginFail		: 'PND.onPluginFail()',
				onReady				: 'PND.onReady()'
			});
			launcherEmbeded = embeded;
		}
		This.redirectToInstallPage = function() {
			if(!DETECTONLY || ((location.href.indexOf('downloader') != -1) && panda.pluginInstalled())) return true;
			log('Redirecting to install browser');
			if (panda.supportedBrowser()) {
				if ($.browser.msie) {
					redirectURL(This._config['links']['install_ie']);
				} else if ($.browser.mozilla) {
					redirectURL(This._config['links']['install_ff']);
				} else {
					redirectURL(This._config['links']['install_other'].replace('[OS]', ((navigator.userAgent.toLowerCase().indexOf('mac') != -1) ? 'mac' : 'pc')));
				}
			} else {
				redirectURL(This._config['links']['not_supported']);
			}
		}
		This.queueCall = function(test, callee) {
			var params	= (arguments[2]) ? arguments[2] : [];
			var tries	= (arguments[2]) ? arguments[3] : 100;

			if (!this.count)
				this.count = 1;
			else
				this.count++;

			if (!test() && this.count <= tries) {
				log('test failed');
				setTimeout(function() {
					This.queueCall(test, callee, params, tries);
				}, 500);
			} else if (test()) {
				log('test passed');
				callee.apply(this, params);
			} else {
				log('queueCall: Failed After ' + tries + ' tries');
			}
		}
		This.validateBugForm = function(form) {
			var result = true;

			// Description -----------------------------------------[TG]
			if (!$(form).find('textarea[name=description]').val()) {
				$(form).find('textarea[name=description]').addClass('error');
				result = false;
			} else {
				$(form).find('textarea[name=description]').removeClass('error');
			}
			//------------------------------------------------------[TG]

			// Account Name ----------------------------------------[TG]
			if (!$(form).find('#custom_field_14').val()) {
				$(form).find('#custom_field_14').addClass('error');
				result = false;
			} else {
				$(form).find('#custom_field_14').removeClass('error');
			}
			//------------------------------------------------------[TG]

			// Email -----------------------------------------------[TG]
			if (!$(form).find('#custom_field_10').val() || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test($(form).find('#custom_field_10').val()) == false) {
				$(form).find('#custom_field_10').addClass('error');
				result = false;
			} else {
				$(form).find('#custom_field_10').removeClass('error');
			}
			//------------------------------------------------------[TG]

			// Uploads ---------------------------------------------[TG]
			var validTypes = ['jpg', 'gif', 'bmp', 'log', 'doc', 'txt'];
			for (var i = 0; i < 3; i++) {
				var val = $(form).find('#file_' + (i + 1)).val();
				if (val && $.inArray(val.substr(val.length - 3, 3).toLowerCase(), validTypes) < 0) {
					$(form).find('#file_' + (i + 1)).addClass('error');
					result = false;
				} else if (val) {
					$(form).find('#file_' + (i + 1)).removeClass('error');
				}
			}
			//------------------------------------------------------[TG]

			return result;
		}
		This.showProgressOverlay = function() {
			// Deprecated
		}
		This.closeProgressOverlay = function() {
			// Deprecated
		}
		//------------------------------------------------------[TG]

		// Private Functions -----------------------------------[TG]
		function showGame() {
			ready = true;
			This.updateImages();
		}
		function updatePlayButton() {
			if (launcher.find('#play_button_flash').length > 0) {

				// Only Do if Download just finished -------------------[TG]
				log('Download Percent: ' + downloadPercent);
				if (downloadPercent >= 90) {
					if ($('#play_popup').length < 1) {
						$('body').append('<div id="play_popup" class="play_popup"><div class="play_button"><img src="' + This._config.images['popup'] + '" usemap="#popup" border="0" /></div><div class="fade">&nbsp;</div></div><map name="popup"><area href="' + This._config['links']['game'] + '" shape="rect" coords="200,180,360,250"><area href="javascript: TPI.closePopup();" shape="rect" coords="490,55,520,85"></map>');
					}
				}
				//------------------------------------------------------[TG]
			}
		}
		function updatePlayToInstallPage() {
            log('Updating the play to install page...');
			if ($.browser.msie == true) {
				log('Found IE');
				if(DETECTONLY && !panda.pluginInstalled()) {
					redirectURL(This._config['links']['install_ie']);
				}
			}
			else if ($.browser.mozilla) {
				log('Found FF');

				if(DETECTONLY && !panda.pluginInstalled()) {
					redirectURL(This._config['links']['install_ff']);
				}
			} else {
				log('Found Other');
				if(DETECTONLY && !panda.pluginInstalled()) {
					redirectURL(This._config['links']['install_other']);
				}
			}

			log('Done setting links...');
		}
		function notSupported() {
			if(DETECTONLY && !panda.pluginInstalled()) {
				redirectURL(This._config['links']['not_supported']);
			}
		}
		function pullForPlugin() {
			if (!panda.pluginInstalled() || !panda.isCurrentVersion()) {
				if (panda.pluginInstalled()) {
					if (!launcherEmbeded) {
						This.embedLauncher();
					}
				}
				log('Still no plugin... Version: ' + panda.getCoreData('pluginMajorVersion') + '.' + panda.getCoreData('pluginMinorVersion'));
				setTimeout(arguments.callee, 1000);
			} else {
				log('Plugin Found!');
				if (location.href.indexOf('downloader') == -1) {
					log('Going to launching dock');
					redirectURL(This._config['links']['dock']);
				}

				if (!launcherEmbeded) {
					log('Launcher not embedded');
					This.embedLauncher();
				}
				else {
					log('Launcher already embedded');
				}
			}
		}
		function isLoading() {
			return (panda.getCoreData('status') == 'downloading');
		}
		function updateProgress() {

			log('Current Panda Status: '+panda.getCoreData('status'));
			if (!isLoading() || panda.getCoreData('totalDownloadSize') <= 0)
				return false;

			if (!progressOverlayShown)
				This.showProgressOverlay();

			game.find('.loading').show();
			launcher.find('.progress-container').show();

			downloadPercent	= Math.floor(panda.getCoreData('downloadProgress') * 100);
			log('Percentage Completed: '+downloadPercent+'%');
			var remaining	= panda.getCoreData('downloadRemainingFormatted');

			launcher.find('.progress').html('Now Loading '+downloadPercent + '% - ' + ((remaining) ? remaining + ' remaining' : 'calculating...'));
			launcher.find('.filler').css('width', downloadPercent + '%');

			if (downloadPercent < 100) {
				setTimeout(updateProgress, 500);
			}
		}
		function exit() {
			log('Error Code: ' + panda.gameData.pandaErrorCode);

			game.css({backgroundImage: 'none'});

			switch (panda.gameData.pandaErrorCode) {
				case 0:
					redirectURL(This._config['links']['exit_codes'][0],2);
				break;

				case 7:
					redirectURL(This._config['links']['exit_codes'][7],1);
				break;

				case 12:
					redirectURL(This._config['links']['exit_codes'][12],2);
				break;

				case 14:
					redirectURL(This._config['links']['exit_codes'][14],1);
				break;

				case 100:
					redirectURL(This._config['links']['exit_codes'][100],2);
				break;

				case 103:
					redirectURL(This._config['links']['exit_codes'][103],1);
				break;

				case 104:
					redirectURL(This._config['links']['exit_codes'][104],1);
				break;

				default:
					log('Loading form');
					bugFormReplace.bug_url = This._config['links']['bug_report']['submit_large'];
					$.post(This._config['links']['bug_report']['template_large'], bugFormReplace);
				break;

			}
		}
		function bugFormReplace(data) {
			var system_log	= panda.getSystemLog();
			var hardware	= panda.getLog('hwprofile.log');
			var game_log	= panda.getLog('toontown' + ((ENV != 'live') ? '_qa' : '') + '.log', 131072, 32768, 32768);
			var search		= ['[SUBMIT_URL]', '[PAID_STATUS]', '[IP]', '[VERSION_NUMBER]', '[BROWSER]', '[GAME_LOG]', '[SYSTEM_LOG]'];
			var replace		= [arguments.callee.bug_url, ((panda.gameData.PAID) ? 0 : 1), panda.gameData.IP, panda.gameData.ServerVersion, getBrowserName(), (game_log) ? game_log : '', hardware + ((system_log) ? system_log : '')];

			for (var i = 0; i < search.length; i++) {
				data = data.replace(search[i], replace[i]);
			}
			showBugForm(data);
		}
		function showBugForm(data) {
			launcher.find('#bug_report').html(data);
			log('Submitting data')
			launcher.find('#bug_report_form').submit();
		}
		function getBrowserName() {
			if ($.browser.msie)
				return 'Internet Explorer ' + $.browser.version;
			else if ($.browser.mozilla)
				return 'Mozilla ' + $.browser.version;
			else if ($.browser.safari)
				return 'Safari ' + $.browser.version;
			else if ($.browser.opera)
				return 'Opera ' + $.browser.version;
			else
				return 'Other ' + $.browser.version;
		}
		function getQuerystring(name) {
		  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		  var regexS = "[\\?&]"+name+"=([^&#]*)";
		  var regex = new RegExp( regexS );
		  var results = regex.exec( window.location.href );
		  if( results == null )
			return "";
		  else
			return decodeURIComponent(results[1].replace(/\+/g, " "));
		}
		function redirectURL(url,popup) {
			var queryString = '';

			// Detect if there is a distribution partner set, add query to each redirect url for detection
			sitePartner = getQuerystring('source');
			
			if(sitePartner != '') {
				queryString = '?source='+sitePartner;
			}
			
			if(queryString != '' && popup) {
				if(popup == 2) { // Open in the parent frame and redirect to home landing page
					if (window.opener && !window.opener.closed) {
						if(panda.gameData.pandaErrorCode == 12 || panda.gameData.pandaErrorCode == 0 || panda.gameData.pandaErrorCode == 100) {
							window.opener.location = '/candystand/landing';
							
							if(panda.gameData.pandaErrorCode == 100) {
								var win = window.open(url);
								if(win) {
									window.close();
								}
								else {
									alert('Please turn off pop-up blockers.');
								}
							}
							window.close();
						}
					}
					else {
						window.close();				
					}
				}
				else { // Popup in a new window
					var win = window.open(url+queryString);
					
					if(win) {
						window.close();
					}
					else {
						alert('Please turn off pop-up blockers.');
					}
				}
			}
			else {
				window.location.replace(url+queryString);
			}
		}
		function log(data) {
				$('#log').append('<div>' + data + '</div>');
		}
		//------------------------------------------------------[TG]

		// Send Back Reference ---------------------------------[TG]
		return ToontownPandaInterface();
		//------------------------------------------------------[TG]
	};
	return This();
};
//------------------------------------------------------[TG]

// Insts -----------------------------------------------[TG]
$(document).ready(function() {
	window.PND = new PandaAPI({
		loaderID		: 'panda_loader',
		gameID			: 'panda_game',
		currentVersion	: [1, 0, 1]
	});

	window.TPI = new ToontownPandaInterface({

		DETECTONLY	: DETECTONLY, // True to only DETECTONLY and forward to proper page
		panda	: PND,
		alt_host: ENV, //((ENV == 'live') ? '' : 'qa'),
		launcher: 'installation',
		game	: 'game_container',
		p3d		: ((ENV == 'live') ? 'http://download.toontown.com/english/inbrowser/toontown.p3d' : 'http://download.'+ENV+'.toontown.com/english/inbrowser/toontown.p3d'),
		images	: {
			close	: PATH['IMG'] + '/gamelaunch-preload.jpg',
			popup	: PATH['IMG'] + '/testsite/popup.play.gif'
		},
		swfs	: {
			progress_overlay: PATH['SWF'] + '/progress_overlay.swf'
		},
		links	: {
			logout			: BASEURL + '/#logout',
			game			: BASEURL + '/game/',
			dock			: BASEURL + '/game/downloader',
			install_ff		: BASEURL + '/game/ff-install',
			install_ie		: BASEURL + '/game/ie-install',
			install_other	: BASEURL + '/game/safari-install',
			not_supported	: BASEURL + '/game/browser-not-supported',
			bug_report		: {
				template_small	: BASEURL + '/game/report-bug-small',
				template_large	: BASEURL + '/game/report-bug-large',
				submit_small	: BASEURL + '/help/report-bug-launcher',
				submit_large	: BASEURL + '/help/report-bug'
			},
			exit_codes		: {
				0	: BASEURL + '/toon-hq/', // Quit
				100	: BASEURL + '/membership/', // Subscribe
				103	: BASEURL + '/membership/', // Subscribe
				104	: BASEURL + '/toon-hq/top-toons', // Top Toons
				7	: BASEURL + '/help/faq/technical/graphics',
				12	: BASEURL + '/toon-hq/',	// Quit
				14	: BASEURL + '/help/faq/technical/graphics'
			}
		}
	});
});
//------------------------------------------------------[TG]