//Begin Cookie plugin
/**
 * Cookie plugin
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
jQuery.cookie = function(x, w, v) {
    if (typeof w != "undefined") {
        v = v || {};
        if (w === null) {
            w = "";
            v.expires = -1
        }
        var u = "";
        if (v.expires && (typeof v.expires == "number" || v.expires.toUTCString)) {
            var t;
            if (typeof v.expires == "number") {
                t = new Date();
                t.setTime(t.getTime() + (v.expires * 24 * 60 * 60 * 1000))
            } else {
                t = v.expires
            }
            u = "; expires=" + t.toUTCString()
        }
        var s = v.path ? "; path=" + (v.path) : "";
        var r = v.domain ? "; domain=" + (v.domain) : "";
        var q = v.secure ? "; secure" : "";
        document.cookie = [x, "=", encodeURIComponent(w), u, s, r, q].join("")
    } else {
        var o = null;
        if (document.cookie && document.cookie != "") {
            var n = document.cookie.split(";");
            for (var p = 0; p < n.length; p++) {
                var m = jQuery.trim(n[p]);
                if (m.substring(0, x.length + 1) == (x + "=")) {
                    o = decodeURIComponent(m.substring(x.length + 1));
                    break
                }
            }
        }
        return o
    }
}
;
(function(b) {
    var a = navigator.userAgent.toLowerCase();
    b.browser = {
        version: (a.match(/.+(?:rv|it|ra|ie|me|ox)[\/: ]([\d.]+)/) || [0, "0"])[1],
        safari: /webkit/.test(a) && !/chrome/.test(a),
        chrome: /chrome/.test(a),
        opera: /opera/.test(a),
        msie: /msie/.test(a) && !/opera/.test(a),
        mozilla: /mozilla/.test(a) && !/(compatible|webkit)/.test(a)
    };
    b.os = {
        mac: /mac/.test(a),
        win: /win/.test(a),
        ppc: /ppc/.test(a)
    }
}
)(jQuery);
//End Cookie plugin

//Begin TT Code
var PandaAPI = function(a) {
    var b = function() {
        var c = false;
        var f = false;
        var e = false;
        var j = false;
        b._config = {};
        b.gameData = {
            GAME_SERVER: false,
            IS_TEST_SERVER: false,
            REFERRER_CODE: false,
            api: false,
            GAME_ENVIRONMENT: false,
            IP: false,
            DISLTOKEN: false,
            PAID: false,
            GAME_USERNAME: false,
            GAME_SHOW_ADDS: false,
            GAME_INGAME_UPGRADE: false,
            GAME_INGAME_MOREINFO: false,
            GAME_INGAME_NAMING: false,
            GAME_INGAME_MANAGE_ACCT: false
        };
        function i() {
            $.extend(b._config, a);
            return b
        }
        b.loaderReady = function() {
            return (c) ? true : false
        }
        ;
        b.setGameData = function(k) {
            $.extend(b.gameData, k)
        }
        ;
        b.setUpdateCallback = function(k) {
            j = k
        }
        ;
        b.getCoreData = function(m) {
            var k = ["status", "pluginVersionString", "pluginMajorVersion", "pluginMinorVersion", "pluginSequenceVersion", "pluginNumericVersion", "pluginDistributor", "coreapiHostUrl", "coreapiTimestamp", "coreapiTimestampString", "totalDownloadSize"];
            var l = ["downloadProgress", "downloadPackageName", "downloadPackageDisplayName", "downloadComplete", "downloadRemainingSeconds", "downloadRemainingFormatted"];
            if (!h() || (("|" + k.join("|") + "|").indexOf("|" + m + "|") == -1 && ("|" + l.join("|") + "|").indexOf(m) == -1)) {
                return false
            }
            if ((("|" + l.join("|") + "|").indexOf(m) != -1 && !e)) {
                return false
            }
            return c.main[m]
        }
        ;
        b.getLog = function(k) {
            if (!h() || !c.main) {
                return false
            }
            return c.main.read_log(k, ((arguments[1]) ? arguments[1] : 128000), ((arguments[2]) ? arguments[2] : 0), ((arguments[3]) ? arguments[3] : 0), ((arguments[4]) ? arguments[4] : 0))
        }
        ;
        b.getSystemLog = function() {
            if (!h() || !c.main) {
                return false
            }
            return c.main.read_system_log((arguments[0]) ? arguments[0] : 128000)
        }
        ;
        b.getGameLog = function() {
            var k = document.getElementById(b._config.gameID);
            if (!h() || !f || !k.main) {
                return false
            }
            return k.main.read_game_log(((arguments[0]) ? arguments[0] : 128000), ((arguments[1]) ? arguments[1] : 0), ((arguments[2]) ? arguments[2] : 0), ((arguments[3]) ? arguments[3] : 0))
        }
        ;
        b.onPluginLoad = function() {
            g("Panda: onPluginLoad");
            c = document.getElementById(b._config.loaderID);
            if (!b.isCurrentVersion()) {
                d("Error", "Not Current Version")
            }
        }
        ;
        b.onUnAuth = function() {
            g("Panda: onUnAuth");
            if (!h()) {
                return false
            }
            if (DETECTONLY) {
                c.main.play()
            }
        }
        ;
        b.onAuth = function() {
            g("Panda: onAuth");
            if (!h()) {
                return false
            }
            if (b.isCurrentVersion() && DETECTONLY) {
                g("Calling Play");
                c.main.play()
            }
        }
        ;
        b.onDownloadBegin = function() {
            g("onDownloadBegin");
            if (!h()) {
                setTimeout(arguments.callee, 10);
                return false
            }
            e = true;
            d("Loading")
        }
        ;
        b.onDownloadNext = function() {
            g("Plugin State: onDownloadNext")
        }
        ;
        b.onDownloadComplete = function() {
            g("Plugin State: onDownloadComplete");
            e = false;
            d("Done Loading")
        }
        ;
        b.onReady = function() {
            g("Plugin State: onReady");
            if (b.isCurrentVersion()) {
                d("Ready")
            }
        }
        ;
        b.onPythonLoad = function() {
            g("Plugin State: onPythonLoad");
            f = true
        }
        ;
        b.onWindowOpen = function() {
            g("Plugin State: onWindowOpen");
            d("Window Mode")
        }
        ;
        b.onPluginFail = function() {
            g("Plugin State: onPluginFail");
            d("Error", "Plugin Failed")
        }
        ;
        b.onPhythonStop = function() {
            g("Plugin State: onPhythonStop");
            d("Exit")
        }
        ;
        b.pluginInstalled = function() {
            navigator.plugins.refresh();
            if (navigator.plugins && navigator.plugins.length) {
                for (var k in navigator.plugins) {
                    if (typeof navigator.plugins[k] == "undefined" || typeof navigator.plugins[k].name != "string") {
                        continue
                    }
                    if (navigator.plugins[k].name && navigator.plugins[k].name.toLowerCase().indexOf("panda3d") != -1) {
                        return true
                    }
                }
            } else {
                if (window.ActiveXObject) {
                    try {
                        if (new ActiveXObject("P3DACTIVEX.P3DActiveXCtrl.1")) {
                            return true
                        }
                    } catch (l) {}
                }
            }
            return false
        }
        ;
        b.supportedBrowser = function() {
            if ($.browser.safari && parseInt($.browser.version.substr(0, 1)) >= 3) {
                return true
            } else {
                if ($.browser.mozilla && !$.os.ppc && parseInt($.browser.version.substr(0, 1)) >= 3) {
                    return true
                } else {
                    if ($.browser.msie && parseInt($.browser.version.substr(0, 1)) >= 6) {
                        return true
                    }
                }
            }
            return false
        }
        ;
        b.isCurrentVersion = function() {
            if (b.getCoreData("pluginNumericVersion")) {
                version = [b.getCoreData("pluginMajorVersion"), b.getCoreData("pluginMinorVersion"), b.getCoreData("pluginSequenceVersion")]
            } else {
                if (b.pluginInstalled() && $.cookie("panda_version")) {
                    version = $.cookie("panda_version").split("|")
                } else {
                    version = [0, 0, 0]
                }
            }
            $.cookie("panda_version", "", {
                expires: -1
            });
            $.cookie("panda_version", "", {
                expires: -1,
                path: "/"
            });
            $.cookie("panda_version", "", {
                expires: -1,
                path: "/game/"
            });
            $.cookie("panda_version", "", {
                expires: -1,
                path: "/"
            });
            $.cookie("panda_version", "", {
                expires: -1,
                path: "/game/"
            });
            $.cookie("panda_version", "", {
                expires: -1,
                path: "/"
            });
            $.cookie("panda_version", version.join("|"), {
                expires: 730,
                path: "/"
            });
            if (b._config.currentVersion[0] > version[0]) {
                return false
            } else {
                if (b._config.currentVersion[0] < version[0]) {
                    return true
                } else {
                    if (b._config.currentVersion[1] > version[1]) {
                        return false
                    } else {
                        if (b._config.currentVersion[1] < version[1]) {
                            return true
                        } else {
                            if (b._config.currentVersion[2] > version[2]) {
                                return false
                            }
                        }
                    }
                }
            }
            return true
        }
        ;
        b.embed = function(m, p, l, k, o) {
            if (!b.pluginInstalled() && !arguments[5]) {
                return false
            }
            g("Comparing versions: Cookie " + $.cookie("panda_version") + " vs. Panda:" + b.isCurrentVersion());
            if ($.cookie("panda_version") && !b.isCurrentVersion()) {
                d("Error", "Not Current Version")
            }
            var n = "";
            if ($.browser.msie) {
                n += '<object classid="CLSID:924B4927-D3BA-41EA-9F7E-8A89194AB3AC" codebase="/game/game/plugin/win32/p3dactivex.cab#Version=' + b._config.currentVersion.join(",") + '" width="' + l + '" height="' + k + '" id="' + p + '" class="' + p + '">\n';
                n += '<param name="data" value="' + m + '" />\n';
                n += '<param name="type" value="application/x-panda3d" />\n';
                for (name in o) {
                    n += '<param name="' + name + '" value="' + o[name] + '" />\n'
                }
                n += "</object>\n"
            } else {
                n += '<embed src="' + m + '" id="' + p + '" class="' + p + '"\n';
                n += 'type="application/x-panda3d"\n';
                n += 'width="' + l + '"\n';
                n += 'height="' + k + '"\n';
                for (name in o) {
                    n += name + '="' + o[name] + '"\n'
                }
                n += "></embed>\n"
            }
            $(document).ready(function() {
                $("#" + p).replaceWith(n)
            });
            g("Panda Embeded");
            return true
        }
        ;
        b.install = function() {
            g("Installing...");
            if (!b.pluginInstalled()) {
                b.startInstall()
            } else {
                g("waiting for loader to check version");
                TPI.queueCall(b.loaderReady, b.startInstall)
            }
            d("Installing")
        }
        ;
        b.startInstall = function() {
            g("starting install");
            if (!b.isCurrentVersion()) {
                g("Checking if plugin Installed: " + b.pluginInstalled());
                g("Current Version: " + b.isCurrentVersion());
                if ($.browser.msie) {
                    g("Installing IE Version");
                    $("body").append('<div id="install_activex"></div>');
                    b.embed("#", "install_activex", 0, 0, {}, true)
                } else {
                    if ($.browser.mozilla) {
                        g("Installing Firefox Version");
                        var k = {
                            "Panda3D Installer": {
                                URL: "/game/game/plugin/firefox/nppanda3d.xpi",
                                IconURL: "https://toontastic.sunrise.games/toontown/favicon.ico",
                                toString: function() {
                                    return this.URL
                                }
                            }
                        };
                        InstallTrigger.install(k)
                    } else {}
                }
            }
        }
        ;
        b.uninstall = function(k) {
            var l = $("#panda_uninstall").get(0);
            if (l && l.main && confirm("Are you sure you would like to uninstall this?")) {
                l.main.uninstall(k)
            } else {
                return false
            }
            d("Uninstalled");
            return true
        }
        ;
        b.download = function() {
            if (navigator.userAgent.toLowerCase().indexOf("mac") != -1) {
                location.href = "/game/game/plugin/osx_fat/p3d-setup.dmg"
            } else {
                location.href = "/game/game/plugin/win32/p3d-setup.exe"
            }
        }
        ;
        b.pluginUnload = function() {
            g("Panda: pluginUnload()");
            c = false;
            j = false;
            if (typeof (CollectGarbage) == "function") {
                g("- Collecting Garbage...");
                CollectGarbage()
            }
        }
        ;
        function h() {
            if (!c) {
                return false
            }
            if (!c.main) {
                return false
            }
            return true
        }
        function d() {
            if (j) {
                j.apply(this, arguments)
            }
        }
        function g(k) {
            $("#log").append("<div>" + k + "</div>")
        }
        return i()
    };
    return b()
};
var ToontownPandaInterface = function(a) {
    var b = function() {
        var k = false;
        var d = false;
        var z = false;
        var e = false;
        var q = false;
        var v = false;
        var r = false;
        var w = false;
        var n = 0;
        b._config = {};
        function y() {
            $.extend(b._config, a);
            k = b._config.panda;
            k.setUpdateCallback(b.updateState);
            d = $("#" + b._config.launcher);
            z = $("#" + b._config.game);
            g("Begin interface:");
            if (!k.pluginInstalled()) {
                b.updateState("Error", "No Plugin Found")
            } else {
                if (!k.supportedBrowser()) {
                    b.updateState("Error", "Not Supported")
                } else {
                    b.updateState("Init")
                }
            }
            return b
        }
        b.updateState = function(B) {
            b.state = B;
            g("State: " + B);
            switch (B) {
            case "Init":
                g("Plugin installed on a supported browser. Done");
                if (k.isCurrentVersion() && DETECTONLY != true && (location.href.indexOf("uninstall") == -1)) {
                    i(b._config.links["dock"])
                }
                break;
            case "Installing":
                if (f()) {
                    g("redirecting again");
                    b.redirectToInstallPage()
                }
                break;
            case "Loading":
                l();
                break;
            case "Done Loading":
                break;
            case "Ready":
                p();
                break;
            case "Window Mode":
                z.css({
                    background: "#ffffff url(" + b._config.images.close + ") center center no-repeat"
                });
                break;
            case "Error":
                switch (arguments[1]) {
                case "No Plugin Found":
                    g("No Plugin Installed");
                    b.queueCall(b.playButtonReady, b.redirectToInstallPage);
                    if (!DETECTONLY) {
                        k.startInstall()
                    }
                    break;
                case "Plugin Failed":
                case "Not Current Version":
                    g("failed to load plugin");
                    b.queueCall(b.playButtonReady, s);
                    b.redirectToInstallPage();
                    break;
                case "Not Supported":
                    g("Browser/OS Not Supported");
                    b.queueCall(b.playButtonReady, x);
                    break
                }
                break;
            case "Uninstalled":
                alert("Toontown Online has been uninstalled");
                location.href = b._config.links["logout"];
                break;
            case "Exit":
                g("Exiting");
                t();
                break;
            default:
                b.state = "Init";
                break
            }
        }
        ;
        b.isReady = function() {
            return q
        }
        ;
        b.playButtonReady = function() {
            g("Play button ready");
            return true
        }
        ;
        b.updateImages = function() {
            g("Updating...");
            if (!q) {
                g("Not Ready");
                return
            }
            b.queueCall(b.playButtonReady, m);
            if (location.href.indexOf("downloader") != -1) {
                if ($("#play_popup").length < 1) {
                    d.find(".progress").html("Finished Loading");
                    d.find(".filler").css("width", "100%");
                    if ($.browser.msie) {
                        d.find("#overlay").css("display", "block");
                        d.find("#overlay-play").css("display", "block")
                    } else {
                        d.find("#overlay").fadeIn("fast", function() {
                            d.find("#overlay-play").fadeIn("slow")
                        })
                    }
                }
            }
            g("Game is Ready");
            if (z.length) {
                z.find(".loading").remove();
                z.find(".panda_game").show()
            }
        }
        ;
        b.closePopup = function() {
            $("#play_popup").hide()
        }
        ;
        b.embedLauncher = function() {
            g("Embedding Launcher");
            var B = k.embed(b._config.p3d, "panda_loader", 0, 0, {
                download: 1,
                auto_install: 0,
                hidden: 1,
                alt_host: b._config.alt_host,
                log_history: 1,
                onPluginLoad: "PND.onPluginLoad()",
                onDownloadBegin: "PND.onDownloadBegin()",
                onDownloadNext: "PND.onDownloadNext()",
                onDownloadComplete: "PND.onDownloadComplete()",
                onPythonLoad: "PND.onPythonLoad()",
                onUnAuth: "PND.onUnAuth()",
                onAuth: "PND.onAuth()",
                onPluginFail: "PND.onPluginFail()",
                onReady: "PND.onReady()"
            });
            v = B
        }
        ;
        b.redirectToInstallPage = function() {
            if (!DETECTONLY) {
                return true
            }
            g("Redirecting to install browser");
            if (k.supportedBrowser()) {
                if ($.browser.msie) {
                    i(b._config.links["install_ie"])
                } else {
                    if ($.browser.mozilla) {
                        i(b._config.links["install_ff"])
                    } else {
                        i(b._config.links["install_other"].replace("[OS]", ((navigator.userAgent.toLowerCase().indexOf("mac") != -1) ? "mac" : "pc")))
                    }
                }
            } else {
                i(b._config.links["not_supported"])
            }
        }
        ;
        b.queueCall = function(E, B) {
            var D = (arguments[2]) ? arguments[2] : [];
            var C = (arguments[2]) ? arguments[3] : 100;
            if (!this.count) {
                this.count = 1
            } else {
                this.count++
            }
            if (!E() && this.count <= C) {
                g("test failed");
                setTimeout(function() {
                    b.queueCall(E, B, D, C)
                }, 500)
            } else {
                if (E()) {
                    g("test passed");
                    B.apply(this, D)
                } else {
                    g("queueCall: Failed After " + C + " tries")
                }
            }
        }
        ;
        b.validateBugForm = function(D) {
            var B = true;
            if (!$(D).find("textarea[name=description]").val()) {
                $(D).find("textarea[name=description]").addClass("error");
                B = false
            } else {
                $(D).find("textarea[name=description]").removeClass("error")
            }
            if (!$(D).find("#custom_field_14").val()) {
                $(D).find("#custom_field_14").addClass("error");
                B = false
            } else {
                $(D).find("#custom_field_14").removeClass("error")
            }
            if (!$(D).find("#custom_field_10").val() || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test($(D).find("#custom_field_10").val()) == false) {
                $(D).find("#custom_field_10").addClass("error");
                B = false
            } else {
                $(D).find("#custom_field_10").removeClass("error")
            }
            var F = ["jpg", "gif", "bmp", "log", "doc", "txt"];
            for (var C = 0; C < 3; C++) {
                var E = $(D).find("#file_" + (C + 1)).val();
                if (E && $.inArray(E.substr(E.length - 3, 3).toLowerCase(), F) < 0) {
                    $(D).find("#file_" + (C + 1)).addClass("error");
                    B = false
                } else {
                    if (E) {
                        $(D).find("#file_" + (C + 1)).removeClass("error")
                    }
                }
            }
            return B
        }
        ;
        b.showProgressOverlay = function() {}
        ;
        b.closeProgressOverlay = function() {}
        ;
        function p() {
            q = true;
            b.updateImages()
        }
        function m() {
            if (d.find("#play_button_flash").length > 0) {
                g("Download Percent: " + n);
                if (n >= 90) {
                    if ($("#play_popup").length < 1) {
                        $("body").append('<div id="play_popup" class="play_popup"><div class="play_button"><img src="' + b._config.images.popup + '" usemap="#popup" border="0" /></div><div class="fade">&nbsp;</div></div><map name="popup"><area href="' + b._config.links["game"] + '" shape="rect" coords="200,180,360,250"><area href="javascript: TPI.closePopup();" shape="rect" coords="490,55,520,85"></map>')
                    }
                }
            }
        }
        function s() {
            g("Updating the play to install page...");
            if ($.browser.msie == true) {
                g("Found IE");
                if (DETECTONLY && !k.pluginInstalled()) {
                    i(b._config.links["install_ie"])
                }
            } else {
                if ($.browser.mozilla) {
                    g("Found FF");
                    if (DETECTONLY && !k.pluginInstalled()) {
                        i(b._config.links["install_ff"])
                    }
                } else {
                    g("Found Other");
                    if (DETECTONLY && !k.pluginInstalled()) {
                        i(b._config.links["install_other"])
                    }
                }
            }
            g("Done setting links...")
        }
        function x() {
            if (DETECTONLY && !k.pluginInstalled()) {
                i(b._config.links["not_supported"])
            }
        }
        function o() {
            g("Redirect to Dock after 5!");
            i(b._config.links["dock"])
        }
        function f() {
            g("Panda: Pulling for plugin");
            if (!k.pluginInstalled() || !k.isCurrentVersion()) {
                if (k.pluginInstalled()) {
                    if (!v) {
                        b.embedLauncher()
                    }
                }
                g("Still no plugin... Version: " + k.getCoreData("pluginMajorVersion") + "." + k.getCoreData("pluginMinorVersion"));
                setTimeout(arguments.callee, 5000)
            } else {
                g("Plugin Found!");
                if (location.href.indexOf("downloader") == -1) {
                    g("Going to launching dock");
                    i(b._config.links["dock"]);
                    return true
                }
                if (!v) {
                    g("Launcher not embedded");
                    b.embedLauncher()
                } else {
                    g("Launcher already embedded")
                }
                return true
            }
        }
        function u() {
            return (k.getCoreData("status") == "downloading")
        }
        function l() {
            g("Current Panda Status: " + k.getCoreData("status"));
            if (!u() || k.getCoreData("totalDownloadSize") <= 0) {
                return false
            }
            if (!r) {
                b.showProgressOverlay()
            }
            z.find(".loading").show();
            d.find(".progress-container").show();
            n = Math.floor(k.getCoreData("downloadProgress") * 100);
            g("Percentage Completed: " + n + "%");
            var B = k.getCoreData("downloadRemainingFormatted");
            d.find(".progress").html("Now Loading " + n + "% - " + ((B) ? B + " remaining" : "calculating..."));
            d.find(".filler").css("width", n + "%");
            if (n < 100) {
                setTimeout(l, 500)
            }
        }
        function t() {
            g("Error Code: " + k.gameData.pandaErrorCode);
            z.css({
                backgroundImage: "none"
            });
            switch (k.gameData.pandaErrorCode) {
            case 0:
                i(b._config.links["exit_codes"][0], 2);
                break;
            case 4:
                i(b._config.links["exit_codes"][4], 1);
                break;
            case 7:
                i(b._config.links["exit_codes"][7], 1);
                break;
            case 12:
                i(b._config.links["exit_codes"][12], 2);
                break;
            case 14:
                i(b._config.links["exit_codes"][14], 1);
                break;
            case 100:
                i(b._config.links["exit_codes"][100], 2);
                break;
            case 103:
                i(b._config.links["exit_codes"][103], 1);
                break;
            case 104:
                i(b._config.links["exit_codes"][104], 1);
                break;
            default:
                g("Loading form");
                c.bug_url = b._config.links["bug_report"]["submit_large"];
                $.post(b._config.links["bug_report"]["template_large"], c);
                break
            }
        }
        function c(H) {
            var I = k.getSystemLog();
            var B = k.getLog("p3dplugin.log", 256000);
            var G = k.getLog("hwprofile.log", 256000);
            var F = k.getLog("toontown" + ((b._config.alt_host && (b._config.alt_host != "live")) ? "_" + b._config.alt_host : "") + ".log", 0, 0, 256000);
            var E = ["[SUBMIT_URL]", "[PAID_STATUS]", "[IP]", "[VERSION_NUMBER]", "[BROWSER]", "[GAME_LOG]", "[SYSTEM_LOG]"];
            var D = [arguments.callee.bug_url, ((k.gameData.PAID) ? 0 : 1), k.gameData.IP, k.gameData.ServerVersion, A(), (F) ? F : "", G + ((I) ? I : "") + ((B) ? B : "")];
            for (var C = 0; C < E.length; C++) {
                H = H.replace(E[C], D[C])
            }
            h(H)
        }
        function h(B) {
            d.find("#bug_report").html(B);
            g("Submitting data");
            d.find("#bug_report_form").submit()
        }
        function A() {
            if ($.browser.msie) {
                return "Internet Explorer " + $.browser.version
            } else {
                if ($.browser.mozilla) {
                    return "Mozilla " + $.browser.version
                } else {
                    if ($.browser.safari) {
                        return "Safari " + $.browser.version
                    } else {
                        if ($.browser.opera) {
                            return "Opera " + $.browser.version
                        } else {
                            return "Other " + $.browser.version
                        }
                    }
                }
            }
        }
        function j(C) {
            C = C.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var B = "[\\?&]" + C + "=([^&#]*)";
            var E = new RegExp(B);
            var D = E.exec(window.location.href);
            if (D == null) {
                return ""
            } else {
                return decodeURIComponent(D[1].replace(/\+/g, " "))
            }
        }
        function i(C, B) {
            var E = "";
            E = k.gameData.pandaErrorCode;
            g("Calling Unload & Garbage Collection...");
            k.pluginUnload();
            g("redirectUrl(" + C + ")");
            var F = "";
            sitePartner = j("source");
            if (sitePartner != "") {
                F = "?source=" + sitePartner
            }
            if (F != "" && B) {
                if (B == 2) {
                    if (window.opener && !window.opener.closed) {
                        if (E == 12 || E == 0 || E == 100) {
                            window.opener.location = "/candystand/landing";
                            if (E == 100) {
                                var D = window.open(C);
                                if (D) {
                                    window.close()
                                } else {
                                    alert("Please turn off pop-up blockers.")
                                }
                            }
                            window.close()
                        }
                    } else {
                        window.close()
                    }
                } else {
                    var D = window.open(C + F);
                    if (D) {
                        window.close()
                    } else {
                        alert("Please turn off pop-up blockers.")
                    }
                }
            } else {
                window.location.replace(C + F)
            }
        }
        function g(B) {
            $("#log").append("<div>" + B + "</div>")
        }
        return y()
    };
    return b()
};
$(document).ready(function() {
    window.PND = new PandaAPI({
        loaderID: "panda_loader",
        gameID: "panda_game",
        currentVersion: [1, 0, 1]
    });
    window.TPI = new ToontownPandaInterface({
        DETECTONLY: DETECTONLY,
        panda: PND,
        alt_host: (ENV == "live") ? "" : ENV,
        launcher: "installation",
        game: "game_container",
        p3d: ((ENV == "live") ? "http://download.toontown.com/english/inbrowser/toontown.p3d" : "http://download." + ENV + ".toontown.com/english/inbrowser/toontown.p3d"),
        images: {
            close: PATH.IMG + "/gamelaunch-preload.jpg",
            popup: PATH.IMG + "/testsite/popup.play.gif"
        },
        swfs: {
            progress_overlay: PATH.SWF + "/progress_overlay.swf"
        },
        links: {
            logout: BASEURL + "/",
            game: BASEURL + "/game/",
            dock: BASEURL + "/game/downloader",
            install_ff: BASEURL + "/game/ff-install",
            install_ie: BASEURL + "/game/ie-install",
            install_other: BASEURL + "/game/safari-install",
            not_supported: BASEURL + "/game/browser-not-supported",
            bug_report: {
                template_small: BASEURL + "/game/report-bug-small",
                template_large: BASEURL + "/game/report-bug-large",
                submit_small: BASEURL + "/help/report-bug-launcher",
                submit_large: BASEURL + "/help/report-bug"
            },
            exit_codes: {
                0: BASEURL + "/toon-hq/",
                100: BASEURL + "/membership/",
                103: BASEURL + "/membership/",
                104: BASEURL + "/toon-hq/top-toons",
                12: BASEURL + "/toon-hq/",
                4: BASEURL + "/help/technical/internet-connection#answer5",
                7: BASEURL + "/help/technical/graphics#answer2",
                14: BASEURL + "/help/technical/graphics"
            }
        }
    })
});
//End TT Code
