/*
Project:   FAST Search
Author:    Carlos A Martinez
Website:   toontown.com
Date:      April 15, 2010
Copyright: Disney 2010 (c)
*/
var TTFS = function(i) {
    var D = i;
    var c = "";
    var g = "";
    var x = "Oops &mdash; try again! Your search does not match any of our content.";
    var t = "Still searching...";
    var m = "Enter search term here...";
    var q = 'Oops &mdash; try again! Please type your search term in the field above, and then click on "Find It!"';
    var z = "Oops &mdash; try again! Your search does not match any of our content.";
    var l = "0";
    var o = "0";
    var j = 0;
    var C = "";
    var y = false;

    function a() {
        A()
    }

    function w() {
        return c
    }

    function B() {
        return g
    }

    function v() {
        return x
    }

    function f() {
        return t
    }

    function k() {
        return m
    }

    function n() {
        return q
    }

    function s() {
        return z
    }

    function e() {
        if (D.xml != null && D.xml != "") {
            c = D.xml
        }
    }

    function p() {
        if (D.page != null && D.page != "") {
            g = D.page
        }
    }

    function r() {
        $("#searchResultErr").removeClass("hrDottedGray");
        $("#searchEntriesTop,#searchEntriesBtm").hide();
        $("#searchTotalPgsTop,#searchTotalPgsBtm").hide();
        $("#paginationTop,#paginationBtm").hide();
        $("#search_0,#search_1,#search_2,#search_3,#search_4,#search_5,#search_6,#search_7,#search_8,#search_9").hide();
        $("#bear,#dog").hide();
        $("#searchErrTxt2").empty()
    }

    function b(F) {
        var H = -1;
        var E = false;
        var G = "";
        G = "&lt;";
        H = F.search(G);
        if (H >= 0) {
            E = true;
            return F
        }
        G = "&gt;";
        H = F.search(G);
        if (H >= 0) {
            E = true;
            return F
        }
        G = "&#x2f;";
        H = F.search(G);
        if (H >= 0) {
            E = true;
            return F
        }
        F = F.replace(/&quot;/g, '"');
        F = F.replace(/&#x27;/g, "'");
        return F
    }

    function d() {
        var E = true;
        var G = $("#query").val();
        try {
            if (G.length == 0) {
                if (l != "1") {
                    $("#query").val(k());
                    $("#searchErrTxt2").html(n()).show();
                    return E
                } else {
                    $("#query").val(k());
                    r();
                    $("#searchErr").show();
                    $("#searchErrTxt").html(n());
                    if ($("#searchTips").is(":visible")) {
                        $("#searchResultErr").removeClass("hrDottedGray")
                    }
                    return E
                }
            } else {
                if (G == k()) {
                    if (l != "1") {
                        $("#searchErrTxt2").html(n()).show();
                        return E
                    } else {
                        r();
                        $("#searchErr").show();
                        $("#searchErrTxt").html(n());
                        if ($("#searchTips").is(":visible")) {
                            $("#searchResultErr").removeClass("hrDottedGray")
                        }
                        return E
                    }
                } else {
                    $("#query").val(G)
                }
            }
            E = false;
            return E
        } catch (F) {
            return E
        }
    }

    function h() {
        var E = "error";
        var F = C;
        if (w().length == 0) {
            E = "blank";
            F = "no_search_entered"
        } else {
            if (o == "0") {
                E = "failed"
            } else {
                if (o > 0) {
                    E = "success"
                } else {
                    if (query == "") {
                        E = "blank"
                    }
                }
            }
        }
        cto = new CTO();
        cto.genre = "us";
        cto.account = "toontown";
        cto.category = "dgame";
        cto.site = "tnt";
        cto.siteSection = "website:search";
        cto.contentType = "search";
        cto.searchType = "toontown";
        cto.pageName = E;
        cto.numSearchResults = o;
        cto.searchRefinement = "";
        cto.internalSearchPhrase = F.replace(/ /g, "+");
        cto.property = "tnt";
        cto.track()
    }

    function u() {
        var X = ttown.getURL() + "help/search";
        var I = 0;
        var aD = "";
        var P = "";
        var ak = "0";
        var S = 0;
        var aa = "0";
        var U = 10;
        var N = 0;
        var av = "";
        var au = 0;
        var ar = 0;
        var ah = 5;
        var ai = 0;
        var M = 1;
        var ae = 1;
        var ax = "pageNumId_";
        var V = "";
        var R = 0;
        var E = 2;
        var an = 0;
        var az = "";
        var af = "title";
        var W = "";
        var ac = "#searchTitle_";
        var aC = "";
        var Q = "description";
        var ag = "";
        var Z = "#searchDesc_";
        var aF = "";
        var J = "url";
        var al = "";
        var Y = "#searchUrl_";
        var am = "";
        var ao = "thumbnail";
        var ab = "";
        var aq = "#searchThumbnail_";
        var ad = "";
        var at = "#search_";
        var aE = "";
        try {
            if (w().length == 0) {
                $("#query").val(k());
                aD = "";
                o = "0";
                if (B() == "pg" || B() == "faq") {
                    $("#searchErr").show();
                    $("#searchErrTxt").html(n());
                    $("#searchResultErr").removeClass("hrDottedGray")
                } else {
                    $("#searchTips").show();
                    if ($("#searchTips").is(":visible")) {
                        $("#searchResultErr").removeClass("hrDottedGray")
                    }
                }
                h()
            } else {
                var G = tt.parseXml(w());
                var ap = $(G);
                aD = ap.find("query").text();
                o = ap.find("totalResults").text();
                ak = ap.find("offset").text();
                aa = ap.find("hits").text();
                aD = b(aD);
                C = aD;
                $("#query").val(aD);
                if (o == "0") {
                    C = aD;
                    $("#query").val(k());
                    $("#searchErr").show();
                    $("#searchErrTxt").html(v());
                    $("#searchResultErr").removeClass("hrDottedGray");
                    h()
                } else {
                    if (o != "" && o != null) {
                        ap.find("result").each(function() {
                            if ($(af, ap).length > 0) {
                                W = ap.find(af).eq(I).text();
                                ag = ap.find(Q).eq(I).text();
                                al = ap.find(J).eq(I).text();
                                ab = ap.find(ao).eq(I).text();
                                aC = ac + I;
                                aF = Z + I;
                                am = Y + I;
                                ad = aq + I;
                                aE = at + I;
                                $(aE).show();
                                $(aC).html(W);
                                $(aF).html(ag);
                                $(am).attr("href", al);
                                if (I == 5) {
                                    $("#bear").show()
                                } else {
                                    if (I == 9) {
                                        $("#dog").show()
                                    }
                                }
                                I++
                            }
                        });
                        j = parseFloat(o);
                        N = Math.ceil(j / U);
                        S = parseFloat(ak);
                        if (S == 0 && N == 1) {
                            au = S + 1;
                            ar = o
                        } else {
                            if ((o - S) < 10) {
                                au = S + 1;
                                ar = o
                            } else {
                                au = S + 1;
                                ar = S + U
                            }
                        }
                        $("#searchEntriesTop,#searchEntriesBtm").html(au + "&nbsp;-&nbsp;" + ar + " of ");
                        $("#searchTotalPgsTop,#searchTotalPgsBtm").html(o);
                        if (N > 1) {
                            $("#paginationTop,#paginationBtm").show()
                        }
                        M = Math.ceil(S / U) + ae;
                        if ((N > 1) && (M > 1)) {
                            var K = (M - 2) * U;
                            var T = "";
                            T = X + "?query=" + encodeURIComponent(aD) + "&offset=" + K;
                            var aB = '<a href="' + T + '">Prev</a>';
                            $("#searchPrevTop,#searchPrevBtm").addClass("ttVisible").html(aB)
                        } else {
                            $("#searchPrevTop,#searchPrevBtm").removeClass(".arrowBack").addClass("arrowBack2")
                        }
                        if (N > ah) {
                            R = N - M;
                            E = (N - ah) + 1;
                            var F = 1;
                            var O = 4;
                            var aj = 1 - (N - O);
                            if (R == E) {
                                an = F
                            }
                            if (R < E) {
                                var L = 2;
                                an = (N - M) + aj + (L * (E - R));
                                if (an > (N - ah)) {
                                    an = N - ah
                                }
                            }
                        }
                        var ay = 1;
                        var aw = ay + an;
                        ai = ah + an;
                        for (aw; aw <= ai; aw++) {
                            V = ax + aw;
                            if (aw <= N) {
                                var K = (aw - 1) * U;
                                var T = "";
                                T = X + "?query=" + encodeURIComponent(aD) + "&offset=" + K;
                                if (aw >= 1 && aw < 10) {
                                    az = "&nbsp;"
                                } else {
                                    az = ""
                                }
                                if (aw == M) {
                                    if (M == 1 && y) {
                                        $("#paginationLnksTop,#paginationLnksBtm").append("&nbsp;")
                                    }
                                    $("#paginationLnksTop,#paginationLnksBtm").append('<span class="selected"><a>' + az + aw + az + "</a></span>")
                                } else {
                                    if ((aw > M) && ((aw == ah) || (aw == N))) {
                                        $("#paginationLnksTop,#paginationLnksBtm").append('<span><a href="' + T + '" id="' + V + '">' + az + aw + az + "</a></span>")
                                    } else {
                                        $("#paginationLnksTop,#paginationLnksBtm").append('<span><a href="' + T + '" id="' + V + '">' + az + aw + az + "</a></span>")
                                    }
                                }
                            }
                        }
                        if (N > M) {
                            var K = (M) * U;
                            var T = "";
                            T = X + "?query=" + encodeURIComponent(aD) + "&offset=" + K;
                            var H = '<a href="' + T + '">Next</a>';
                            $("#searchNextTop,#searchNextBtm").addClass("ttVisible").html(H)
                        } else {
                            $("#searchNextTop,#searchNextBtm").removeClass(".arrowNext").addClass("arrowNext2")
                        }
                        h()
                    } else {}
                }
            }
        } catch (aA) {}
    }

    function A() {
        if ($.browser.msie && parseInt($.browser.version.substr(0, 1)) <= 6) {
            y = true
        }
        if (D.pageResults == "1") {
            l = D.pageResults;
            e();
            p();
            u()
        } else {
            $("#query").val(k())
        }
        $("#query").keydown(function() {
            if ($("#query").val() == k()) {
                $("#query").val("")
            }
        });
        $("#query").mouseup(function() {
            $("#query").val("")
        });
        $("#searchForm").submit(function() {
            var F = d();
            var E = false;
            if (!F) {
                $("#ttSearchBtn").attr("disabled", "disabled");
                E = true
            } else {
                E = false
            }
            return E
        })
    }
    a()
};