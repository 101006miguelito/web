var ttSearch = {
  xml: "",
  page: "",
  queryNotFound: 'Oops &mdash; try again! Your search does not match any of our content.',
  waitTxt: 'Still searching...',
  inputTxt: 'Enter search term here...',
  errTxt: 'Oops &mdash; try again! Please type your search term in the field above, and then click on "Find It!"',
  badWordTxt: 'Oops &mdash; try again! Your search does not match any of our content.',

  getXml: function(){
    return this.xml;
  },

  getPage: function(){
    return this.page;
  },

  getQueryNotFound: function(){
    return this.queryNotFound;
  },

  getWaitTxt: function(){
    return this.waitTxt;
  },

  getInputTxt: function(){
    return this.inputTxt;
  },

  getErrTxt: function(){
    return this.errTxt;
  },

  getBadWordTxt: function(){
    return this.badWordTxt;
  },

  setXml: function(str){
    if (str != null && str != ''){
      this.xml = str;
    }
  },

  setPage: function(str){
    if (str != null && str != ''){
      this.page = str;
    }
  },

  clearUI: function(){
    $('#searchResultErr').removeClass('hrDottedGray');
    $('#searchEntriesTop,#searchEntriesBtm').hide();
    $('#searchTotalPgsTop,#searchTotalPgsBtm').hide();
    $('#paginationTop,#paginationBtm').hide();
    $('#search_0,#search_1,#search_2,#search_3,#search_4,#search_5,#search_6,#search_7,#search_8,#search_9').hide();
    $('#bear,#dog').hide();
  },

  formErrs: function(){
    var formErrors = true;
    var query = $('#query').val();
    query = $.trim(query);

    try{
      if (query.length == 0){
        $('#query').val(ttSearch.getInputTxt());
        ttSearch.clearUI();
        $('#searchErr').show();
        $('#searchErrTxt').html(ttSearch.getErrTxt());
          if( $('#searchTips').is(':visible') ) {
            $('#searchResultErr').removeClass('hrDottedGray');
          }
        return formErrors;
      }else if (query == ttSearch.getInputTxt()){
          ttSearch.clearUI();
          $('#searchErr').show();
          $('#searchErrTxt').html(ttSearch.getErrTxt());
          if( $('#searchTips').is(':visible') ) {
            $('#searchResultErr').removeClass('hrDottedGray');
          }
        return formErrors;
      }else{
        $("#query").val(query);
      }
      formErrors = false;
      return formErrors;
    }catch(err){
        return formErrors;
      }
  },

  find: function(){

    var searchUrl = 'http://'+location.host+'/help/search';
    var counter = 0;
    var query = '';
    var ttsearch = '';

    var totalResults = '0';
    var totalResultsInt = 0;

    var offset = '0';
    var offsetInt = 0;

    var hits = '0';
    var hitsInt = 10;//10 results per page

    var totalPages = 0;
    var searchEntriesTop = '';
    var pageEntries1 = 0;
    var pageEntries2 = 0;

    var maxPaginationSet = 5;
    var maxPaginationSetTmp = 0;
    var pageNum = 1;
    var pageNumOffset = 1;
    var pageNumId = 'pageNumId_';
    var pageNumIdTmp = '';
    var paginationOffset = 0;
    var pageShift = 2;//2 values to the left and right of pageNum, example; 2 3 [4] 5 6
    var pageShiftDiff = 0;


    var title='title';
    var titleVal='';
    var titleId='#searchTitle_';
    var titleIdTmp='';

    var description='description';
    var descriptionVal='';
    var descriptionId='#searchDesc_';
    var descriptionIdTmp='';

    var url='url';
    var urlVal='';
    var urlId='#searchUrl_';
    var urlIdTmp='';

    var thumbnail='thumbnail';
    var thumbnailVal='';
    var thumbnailId='#searchThumbnail_';
    var thumbnailIdTmp='';

    var searchId = '#search_';
    var searchIdTmp = '';

    try{
      if (ttSearch.getXml().length == 0){
        query = '';
        totalResults = '-1';

        if ( ttSearch.getPage() == 'pg' || ttSearch.getPage() == 'faq'){
          //guest clicked on "Find It!" from Player's Guide or FAQs left search field blank from
          $('#searchErr').show();
          $('#searchErrTxt').html(ttSearch.getErrTxt());
          $('#searchResultErr').removeClass('hrDottedGray');
        }else{
          $('#searchTips').show();
          if( $('#searchTips').is(':visible') ) {
            $('#searchResultErr').removeClass('hrDottedGray');
          }
        }

      }else{
        var dom = tt.parseXml(ttSearch.getXml());
        var $dom = $(dom);

        query = $dom.find('query').text();
        totalResults = $dom.find('totalResults').text()
        offset = $dom.find('offset').text();
        hits = $dom.find('hits').text();

        var query2 = query;
        var quotesHTML = '"';
        query2 = query2.replace(/&quot;/g, quotesHTML);
        $('#query').val(query2);

        if (totalResults == '0'){
          //search not found
          $('#query').val(ttSearch.getInputTxt());
          $('#searchErr').show();
          $('#searchErrTxt').html(ttSearch.getQueryNotFound());
          $('#searchResultErr').removeClass('hrDottedGray');

        }else if (totalResults != '' && totalResults != null){
        //search found

          $dom.find('result').each(function(){
            if($(title,$dom).length > 0){
                titleVal = $dom.find(title).eq(counter).text();
                descriptionVal = $dom.find(description).eq(counter).text();
                urlVal = $dom.find(url).eq(counter).text();
                thumbnailVal = $dom.find(thumbnail).eq(counter).text();

                titleIdTmp = titleId+counter;
                descriptionIdTmp = descriptionId+counter;
                urlIdTmp = urlId+counter;
                thumbnailIdTmp = thumbnailId+counter;
                searchIdTmp = searchId+counter;

                $(searchIdTmp).show();
                $(titleIdTmp).html(titleVal);
                $(descriptionIdTmp).html(descriptionVal);
                $(urlIdTmp).attr('href',urlVal);
                //$(thumbnailIdTmp).html(thumbnailVal);

                if (counter ==  5){
                  $('#bear').show();
                }else if (counter == 9){
                  $('#dog').show();
                }

                counter++;
            }
          });

          //Begin Pagination
          totalResultsInt = parseFloat(totalResults);
          totalPages = Math.ceil(totalResultsInt/hitsInt);
          offsetInt = parseFloat(offset);

          if (offsetInt == 0 && totalPages == 1){
            //first page and only page
            pageEntries1 = offsetInt+1;
            pageEntries2 = totalResults;
          }else if ( (totalResults - offsetInt) < 10 ){
            //last page
            pageEntries1 = offsetInt+1;
            pageEntries2 = totalResults;
          }else{
            //all other pages
            pageEntries1 = offsetInt+1;
            pageEntries2 = offsetInt+hitsInt;
          }
          $('#searchEntriesTop,#searchEntriesBtm').html(pageEntries1+'&nbsp;-&nbsp;'+pageEntries2+' of ');
          $('#searchTotalPgsTop,#searchTotalPgsBtm').html(totalResults);

          if (totalPages > 1){
            $('#paginationTop,#paginationBtm').show();
          }

          pageNum =  Math.ceil(offsetInt/hitsInt)+pageNumOffset;

          if ( (totalPages > 1) && (pageNum > 1) ){
            var offsetVal = (pageNum-2)*hitsInt;
            var searchUrlTmp = '';
            searchUrlTmp = searchUrl +'?query='+query+'&offset='+offsetVal;
            var prevLink = '<a href="'+searchUrlTmp+'">Prev</a>';
            $('#searchPrevTop,#searchPrevBtm').addClass('ttVisible').html(prevLink);
          }else{
            $('#searchPrevTop,#searchPrevBtm').removeClass('.arrowBack').addClass('arrowBack2');//remove background img
          }


          if (totalPages > maxPaginationSet){
            paginationOffset = totalPages - pageNum;
            pageShift = (totalPages - maxPaginationSet)+1;

            var pageShiftDiffConstant = 1;
            var firstShift = 4;
            var y = 1 - (totalPages - firstShift);

            if (paginationOffset == pageShift){
              pageShiftDiff = pageShiftDiffConstant;
            }
            if (paginationOffset < pageShift){
              var shiftConstant = 2;
              pageShiftDiff = (totalPages - pageNum) + y +(shiftConstant*(pageShift - paginationOffset));
              if (pageShiftDiff > (totalPages - maxPaginationSet)){
                pageShiftDiff = totalPages - maxPaginationSet;
              }
            }
          }

          var i = 1;
          var j = i + pageShiftDiff;
          maxPaginationSetTmp = maxPaginationSet + pageShiftDiff;

          for (j; j <= maxPaginationSetTmp; j++){

            pageNumIdTmp = pageNumId+j;
            if ( j <= totalPages){

              var offsetVal = (j-1)*hitsInt;
              var searchUrlTmp = '';
              searchUrlTmp = searchUrl +'?query='+query+'&offset='+offsetVal;

              if (j == pageNum){
                $('#searchPaginationTop,#searchPaginationBtm').append('<li class="selected"><a>'+j+'</a></li>');
              }else if ( (j > pageNum) && ( (j == maxPaginationSet) || (j == totalPages)  ) ){
                $('#searchPaginationTop,#searchPaginationBtm').append('<li><a class="last2" href="'+searchUrlTmp+'" id="'+pageNumIdTmp+'">'+j+'</a></li>');
              }else{
                $('#searchPaginationTop,#searchPaginationBtm').append('<li><a href="'+searchUrlTmp+'" id="'+pageNumIdTmp+'">'+j+'</a></li>');
              }

            }

          }

          if ( totalPages > pageNum ){
            var offsetVal = (pageNum)*hitsInt;
            var searchUrlTmp = '';
            searchUrlTmp = searchUrl +'?query='+query+'&offset='+offsetVal;
            var nextLink = '<a href="'+searchUrlTmp+'">Next</a>';
            $('#searchNextTop,#searchNextBtm').addClass('ttVisible').html(nextLink);
          }else{
            $('#searchNextTop,#searchNextBtm').removeClass('.arrowNext').addClass('arrowNext2');//remove background img
          }
          //End Pagination

        }else{
        //error TBD
        }
      }
    }catch(err){
      //alert('ttSearch.find() - try catch err: '+err);
    }
  },

  init: function(){
    $('#query').val(ttSearch.getInputTxt());
    ttSearch.find();

    $('#query').keydown(function(){
      if ($('#query').val() == ttSearch.getInputTxt()){
        $('#query').val('');
      }
    });

    $('#query').mouseup(function(){
        $('#query').val('');
    });

    $('#searchForm').submit(function(){
      var valid = ttSearch.formErrs();
      var submitForm = false;
      if (!valid){
        $('#ttSearchBtn').attr('disabled','disabled');
        submitForm = true;
      }else{
        submitForm = false;
      }
      return submitForm;
    });
  }
};
