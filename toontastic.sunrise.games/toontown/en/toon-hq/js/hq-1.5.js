// Category Display names

var categoryName = new Object();
	categoryName["buildingDefeated"]="Buildings Recovered ";
	categoryName["minigame"]="Trolley Games Played";
	categoryName["questComplete"]="Tasks Completed";
	categoryName["fishedFish"]="Fish Caught";
	categoryName["jellybeansearned"]="Jellybeans Earned";
	categoryName["catalog-purchase"]="Cattlelog Items Purchased";
	categoryName["gavegifts"]="Gifts Given";
	categoryName["golf_ace"]="Holes in One";
	categoryName["golf_underPar"]="Courses Under Par";
	categoryName["bosswon_s"]="Sellbot VP Victories";
	categoryName["bosswon_m"]="Cashbot CFO Victories";
	categoryName["bosswon_l"]="Lawbot CJ Victories";
	categoryName["bosswon_c"]="Bossbot CEO Victories";
	categoryName["kartingwon"]="Most Races Won";
	categoryName["gamesplayed"]="Trolley Games Played";
	categoryName["fishcaught"]="Fish Caught";
	categoryName["cogsdefeated"]="Cogs Defeated";
	categoryName["fishbingowin"]="Fish Bingo";

// Category Descriptions
var categoryDescription = new Object();
	categoryDescription["buildingDefeated"]="These are the Top five Toons who recovered the most Toon buildings from the Cogs yesterday. Thanks for taking the streets back!";
	categoryDescription["minigame"]="These are the Top five Toons who played the most Trolley Games yesterday. These Toons really know how to get their game on!";
	categoryDescription["questComplete"]="These are the Top five Toons who completed the most ToonTasks yesterday. Way to do your Toon duty!";
	categoryDescription["fishedFish"]="These are the Top five Toons who caught the most fish yesterday. They must be swimming in jellybeans!";
	categoryDescription["jellybeansearned"]="These are the Top five Toons who earned the most jellybeans yesterday. We bet these Toons like to fish!";
	categoryDescription["catalog-purchase"]="These are the Top five Toons who purchased the most Cattlelog items yesterday. Clarabelle says thanks for the business!";
	categoryDescription["gavegifts"]="These are the Top five Toons who gave out the most gifts yesterday. Your Toon generosity is truly appreciated!";
	categoryDescription["golf_ace"]="These are the Top five Toons who made the most minigolf holes in one yesterday. You're all Toontastic shots!";
	categoryDescription["golf_underPar"]="These are the Top five Toons who completed the most minigolf courses under par yesterday. Remember - no mulligans allowed!";
	categoryDescription["bosswon_s"]="These are the Top five Toons who earned the most Sellbot V.P. victories yesterday. You sure showed that Cog who's boss!";
	categoryDescription["bosswon_m"]="These are the Top five Toons who earned the most Cashbot C.F.O. victories yesterday. It's like dropping a safe on a Cog!";
	categoryDescription["bosswon_l"]="These are the Top five Toons who earned the most Lawbot C.J. victories yesterday. Looks like his verdict came in!";
	categoryDescription["bosswon_c"]="These are the Top five Toons who earned the most Bossbot C.E.O. victories yesterday. You sure downsized that Cog!";
	categoryDescription["kartingwon"]="These are the Top five Toons who won the most Goofy Speedway races yesterday. These Toons are fast enough!";
	categoryDescription["gamesplayed"]="These are the Top five Toons who played the most Trolley Games yesterday. These Toons really know how to get their game on!";
	categoryDescription["fishcaught"]="These are the Top five Toons who caught the most fish yesterday. They must be swimming in jellybeans!";
	categoryDescription["cogsdefeated"]="These are the Top five Toons who defeated the most Cogs yesterday. They really got those gears spinning!";
	categoryDescription["fishbingowin"]="These are the Top five Toons who earned the most jellybeans during Fish Bingo yesterday. Congratulations!";

var toonArtThumbs = [
  ttown.getCDN()+"en/fan-art/img/137x100/amanda.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/princess-ladybug.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/furball-saltyfoot.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/princess-ladybug.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/taffy-wonder-pop.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/thora.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/obsidian.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/prof-rufflenoodle.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/jet.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/indigo-heart-kitten.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/roxy.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/memory.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/cutey.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/little-aqua-cat.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/cuddles-petal-face.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/crazy-candy-megadoodle.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/geronimo.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/lady-violet-sparkleburger.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/little-rainbow.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/vancu.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/allie.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/bloop.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/bunny-hip-hop.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/cassandra.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/happy.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/king-reggie.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/king-reggie-thundertoon.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/miss-bonnie.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/miss-ginger-twinklestink.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/nacey.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/nightt.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/pickles.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/princess-fancy.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/princess-yippie.jpg",
  ttown.getCDN()+"en/fan-art/img/137x100/ruby.jpg",
	ttown.getCDN()+"en/fan-art/img/137x100/silly-cukoo-dandybee.jpg",
	ttown.getCDN()+"en/fan-art/img/137x100/silly-flip-bizzenbubble.jpg",
	ttown.getCDN()+"en/fan-art/img/137x100/stirs.jpg",
	ttown.getCDN()+"en/fan-art/img/137x100/violet.jpg"
];

$(function() {
	$.preLoadImages(ttown.getCDN()+"en/toon-hq/img/img_toonHqTrophy.png");/*TT change*/

	var currentTime = (new Date).getTime();
	var serverTime = new Date(toonTownTime);

	setInterval(function(){

		var startTime = (new Date).getTime();
		var d = new Date((Date.parse(serverTime) + Math.abs(startTime - currentTime)));

		var hours = d.getHours();
		if (hours<0) {
			hours = 24-Math.abs(hours);
		}
		var mins = d.getMinutes();
		if (mins < 10){
			mins = "0" + mins;
		}

		document.getElementById('toonTime').innerHTML = ( ((hours>12)?(hours-12):hours) + ":" + mins );

		}, 1000 );

	/* TT Change
	$('#flashObj').flash(
		{
			src: '../swf/ttPoll.swf',
			width: 130,
			height: 180,
			wmode: 'transparent',
			flashvars:
			{
				configfile: '../swf/data/config.xml'
			}
		},
		{ version: 10 }
	); End TT Change */


	$.ajax({
	url: "/content/en/toptoons/today/TopToons.xml",
  	type: "GET",
  	dataType: "xml",
	success: function(xml) {

		var topToons = new Array();

		$(xml).find('toptoons').children().each(function(index){
			//get the things we are going to need
			var currCatID = $(this).get(0).nodeName;
			var currCatName = categoryName[currCatID];
			var topUser = $(this).find('user').eq(0);
			var topUserName = topUser.attr('name');

			/*Begin TT change*/
			if (currCatID!=undefined && currCatName!=undefined && topUserName!=undefined){
			  topToons[index] = '<li class="'+currCatID+'"><div class="catName"><h4>'+currCatName+'</h4></div><div class="playerName"><p>'+topUserName+'</p></div><img src="/content/en/toptoons/today/'+topUser.find('imgSmall').attr('src') +'" alt="'+topUserName+'" width="33" height="33"/></li>';
			}
			/*End TT change*/

		});

		$('div#topToonsCarousel ul').append(topToons.join(""));
		/* TT owned code- initializing Art carousel */
		var artcarousel= $('div#fanArtCarousel ul');
		for(var artpage in toonArtThumbs){
			artcarousel.append('<li><img src="'+toonArtThumbs[artpage]+'"/></li>');
		}
		//now that we are all done with the ajax loading, activate the carousel
		disTT.carousel.init();
		disTT.carousel.activateToonHQ_toonArt(); // activate top art
    	disTT.carousel.activateToonHQ_topToons(); // activate top toons

		//jquery png fix for IE6
		$('#topToonsMod img').pngFix();

	 },

	  error: function(xhr, status, errorThrown) {

		 tt.log(errorThrown+', '+status+', '+xhr.statusText);

	  }


	});


});

//jQuery png fix for the inline user images that are loaded via AJAX

/*
	jQuery Version:				jQuery 1.3.2
	Plugin Name:				pngFix V 1.0
	Plugin by: 					Ara Abcarians: http://ara-abcarians.com
	License:					pngFix is licensed under a Creative Commons Attribution 3.0 Unported License
								Read more about this license at --> http://creativecommons.org/licenses/by/3.0/
*/
(function($) {
    $.fn.pngFix = function(options) {

		return this.each(function() {
			var obj = $(this);
			// detect IE6
			if(jQuery.browser.msie && parseInt(jQuery.browser.version) == 6){
				obj.each(function(){

					// set variables
					var obj = $(this);
					var bg = obj.css("backgroundImage");

					// check if its an image
					if(obj.attr("src")){
						var clear=ttown.getCDN()+"en/global/img/blank.gif"; //path to clear.gif /*TT change*/
						obj.css({
							height: obj.height(),
							width: obj.width(),
							filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+obj.attr("src")+"', sizingMethod='scale')"
						});
						obj.attr({ src: clear });
					} else {
						// but if its a bg image through css then do this
						var img = bg.substring(bg.indexOf('"') + 1, bg.lastIndexOf('"'));
						obj.css({
							filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + img + "', sizingMethod='scale')",
							backgroundImage: "none"
						});
					}
				});

			} // END if

		}); // END: return this

		// returns the jQuery object to allow for chainability.
        return this;
    };
})(jQuery);

/*Begin TT code*/
function ttBlog(xml){

  var counter=-1;
  var tester=0;
  var blgId='';

  var title='title';
  var titleVal='';
  var titleId='#blgTitle_';

  var link='link';
  var linkVal='';
  var linkId='#blgLink_';

  var updated='updated';
  var updatedVal='';
  var updatedId='#blgDate_';

  var summary='summary';
  var summaryVal='';
  var summaryId='#blgSummary_';

  var commentTotal='commentTotal';
  var commentTotalVal='';
  var commentTotalId='#blgComments_';
  var updatedMonth = new Array("January","February","March","April","May","June","July","August","September","October","November","December");

  $(xml).find('entry').each(function(){

     counter++;
     if($(title,xml).length > 0){

       titleVal = $(this).find(title).text();
       linkVal = $(this).find(link).attr("href");
       updatedVal = $(this).find(updated).text();
       summaryVal = $(this).find(summary).text();
       commentTotalVal = $(this).find(commentTotal).text();

       blgId=titleId+counter;
       $(blgId).html(titleVal);

       blgId=linkId+counter;
       $(blgId).attr('href',linkVal);

       if (counter==0){
         //only for the first entry
         blgId=summaryId+counter;
         $(blgId).html(summaryVal);
       }

       updatedVal=updatedMonth[updatedVal.substr(5,2)-1]+' '+updatedVal.substr(8,2)+", "+updatedVal.substr(0,4); // place in Month DD, YYYY format
       blgId=updatedId+counter;
       $(blgId).html(updatedVal);

       blgId=commentTotalId+counter;
       commentTotalVal+=' Comments';
       $(blgId).html(commentTotalVal);

     }
    });
  $('#ttBlogEntries').show();

}

function initToonHQ(URL){

  if (URL != null && URL !='' ){
    $.ajax({
      'type' : 'GET',
      'url' : URL,
      'cache': false,
      'dataType' : 'xml',
       dataType: ($.browser.msie) ? 'text' : 'xml',
      'success' : function(data){
        var xml;

        if (typeof data == 'string') {
          xml = new ActiveXObject("Microsoft.XMLDOM");
          xml.async = false;
          xml.loadXML(data);
        } else {
          xml = data;
        }

       ttBlog(xml);
      },
      'error'	: function(request, text, error){        
        $('#ttBlogErrImg').html('<img src="'+ttown.getCDN()+'en/toon-hq/img/blog-down.jpg" width="334"  height="248" alt="blog down" />');
        $('#ttBlogErrTxt').addClass('ttError').html("We're experiencing technical difficulties. Please try again later.");
        $('#ttBlogErr').show();      
      }
    } );

  }else{
   //TBD
  }

}
/*End TT code*/
