var indexUrl = ttown.getURL();
var redirectParamName = "redirectToUrl";

function querySt(name) {
    gy = window.location.search.substring(1).split("&");
    for (i=0;i<gy.length;i++) {
        ft = gy[i].split("=");
        if (ft[0] == name) {
            return ft[1];
        }
    }
}

function redirectToUrl(URL){
	if(URL == null) {
		window.location.href=indexUrl;
	}
	else {
		var goToUrl = "";

		if(window.location.search.substring(1).length > 0)
		{
			if(URL.indexOf("?") == -1) {
				URL = URL + "?";
			}
			else {
				URL = URL + "&";
			}
		}

		goToUrl = URL + window.location.search.substring(1);
	}

	window.location.href=goToUrl;
}

function loginComplete(URL){
	if(URL != null){
		window.location.href = URL;
	}
	else if(querySt(redirectParamName) != null) {
		window.location.href = decodeURI(querySt(redirectParamName));
	}
	else{
		window.location.href=indexUrl;
	}
}

function regComplete(URL){
	if(querySt(redirectParamName) != null) {
		window.location.href = querySt(redirectParamName);
	}
	else if(URL != null) {
		window.location.href = URL;
	}
	else {
		window.location.href=indexUrl;
	}
}

function goToReg(){
	window.location.href=indexUrl;
}

function exitMod(URL){
	if(querySt(redirectParamName)!= null) {
		window.location.href=querySt(redirectParamName);
	}
	else if(URL != null){
		window.location.href = URL;
	}
	else {
		window.location.href=indexUrl;
	}
}

function purchaseComplete(){
	window.location.href=indexUrl;
}

function toCancel(URL) {
	if(URL != null) {
		window.location.href=URL;
	}
	else {
		window.location.href=cancelUrl;
	}
}

function ttUser(id){
  if (id != null && id != ''){
    var ttLoginMsg1 = 'Hi! '+id;
    var ttLoginMsg2 = '<a href="#" id="ttLogout" name="ttLogout">Log out</a>';
    $('#ttLoginMsg1').html(ttLoginMsg1);
    $('#ttLoginMsg2').html(ttLoginMsg2);
    $('#ttLogout').click( function(){
      tt.logout();
    });
  }
}

function atlasRegConfirm(){
  $('#atlasRegConfirm').html('<img height="1" width="1" src="https://switch.atdmt.com/action/tgmttn_welcometrial_3" />');
}
function atlasPurConfirm(){
  $('#atlasPurConfirm').html('<img height="1" width="1" src="https://switch.atdmt.com/action/tgmttn_thanksforpurchasing_7" />');
  $('#atlasPurConfirm2').html('<img src="https://ad.yieldmanager.com/pixel?id=755165&t=2" width="1" height="1" />');
  $('#atlasPurConfirm2').append('<IMG SRC="https://ad.doubleclick.net/activity;src=1397212;type=toont797;cat=23134043;ord=[Random Number]?" WIDTH=1 HEIGHT=1 BORDER=0><div id=\'bfe_tracker\' style=\'position: absolute; left: 0px; top: 0px; visibility: hidden;\'><img src="https://secure.blindferret.com/openx/external/TrackingPixel.php?redir=1&id=1&campaigngroupid=2065" /><img src="https://secure.blindferret.com/openx/external/TrackingPixel.php?redir=1&id=5&campaigngroupid=1973" /><img src="https://secure.blindferret.com/openx/external/TrackingPixel.php?redir=1&id=2" /><img src=\'https://secure.blindferret.com/openx/www/delivery/ti.php?trackerid=127&cb=%%RANDOM_NUMBER%%\' width=\'0\' height=\'0\' alt=\'\' /><img src=\'https://secure.blindferret.com/openx/www/delivery/ti.php?trackerid=128&cb=%%RANDOM_NUMBER%%\' width=\'0\' height=\'0\' alt=\'\' /></div>');
}