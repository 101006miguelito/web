var mySettings = {
  chromeStyle: 'blue',
  footerStyleSet: 'transparentLight',
  footerDisplayMode: '',
  footerLegalSiteMapTarget:	'_top',
  footerLegalSafetyTarget: '_top',
  footerLegalPrivacyPolicyTarget: '_top',
  footerLegalIBATarget: '_top',
  footerLegalCustom: '<br /><br /><a href="'+ttown.getURL()+'member-agreements">Member Agreements</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="'+ttown.getURL_SSL()+'membership/account-management/">Member/Guest Services</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="'+ttown.getURL()+'help/">Help</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="'+ttown.getURL()+'help/contact">Contact Us</a>'
};

$(window).load(function (){
  if ( tt.getCookie('prospect') != '-1' ){
    $('#atlasHomeProspect').html('<img height="1" width="1" src="http://switch.atdmt.com/action/tgmttn_homeprospect_1" />');
    $('#atlasHomeProspect2').html('<img src="http://ad.yieldmanager.com/pixel?id=755164&t=2" width="1" height="1" />');
    cto.pageName='toontown_prospect';
    cto.track();
    var myDate = new Date();
    myDate.setDate(myDate.getDate()+365);
    tt.setCookie('prospect', '-1',myDate);
  }
  if ($.browser.msie && parseInt($.browser.version.substr(0,1)) <= 6){
  }else{
    $.getScript('http://static.decipherinc.com/apps/intercept/js/qst/qst10006a:pop1');
  }
});