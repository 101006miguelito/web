var CurrentDate = new Date();

var CurrentDay = CurrentDate.getDate();
var CurrentMonth = CurrentDate.getMonth() + 1;

if (CurrentMonth == 12 || CurrentMonth == 9 && CurrentDay >= 23 || CurrentMonth == 10) {
  if (CurrentMonth == 12) {
    var HomepageSwf = "en/home/swf/winter-holiday.swf";
  }
  if (CurrentMonth > 8 && CurrentMonth < 11) {
    var HomepageSwf = "en/home/swf/halloween.swf";
  }
} else {
  var HomepageSwf = "en/global/swf/TTHomepage.swf";
}

var mySettings = {
  chromeStyle: 'blue',
  footerStyleSet: 'transparentLight',
  footerDisplayMode: '',
  footerLegalSiteMapTarget:	'_top',
  footerLegalSafetyTarget: '_top',
  footerLegalPrivacyPolicyTarget: '_top',
  footerLegalIBATarget: '_top',
  footerLegalCustom: '<br /><br /><a href="'+ttown.getURL()+'member-agreements">Member Agreements</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="'+ttown.getURL_SSL()+'membership/account-management/">Member/Guest Services</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="'+ttown.getURL()+'help/">Help</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="'+ttown.getURL()+'help/contact">Contact Us</a>'
};

function loadNavBar(){
	$('.bottomInnerContainerMid dl dt a').css({background: 'url('+ttown.getCDN()+'en/global/img/img_homeSubNavSprite.jpg) no-repeat scroll'});
	$('.bottomInnerContainerMid dl').show();
};

$('.bottomInnerContainerMid dl').hide();
var flashvars = {
  contentSwfUrl: ttown.getCDN()+'en/global/swf/TTHomepage.swf',
  aboutLink: ttown.getURL()+'about/',
  toontownLink: ttown.getURL()+'toon-hq/',
  playNowLink: ttown.getURL()+'play'
};
var params = {
  wmode: 'transparent',
  allowscriptaccess: 'always'
};
var attributes = {
  wmode: 'opaque',
  menu: 'false'
};

if (CurrentMonth == 12 || CurrentMonth == 9 && CurrentDay >= 23 || CurrentMonth == 10) {
  if (CurrentMonth == 12) {
    swfobject.embedSWF(ttown.getCDN()+"en/home/swf/winter-holiday-loader.swf","homepageFlash","995","590","10",ttown.getCDN()+"en/global/swf/expressInstall.swf",flashvars,params,attributes);
  }
  if (CurrentMonth > 8 && CurrentMonth < 11) {
    swfobject.embedSWF(ttown.getCDN()+"en/home/swf/halloween-loader.swf","homepageFlash","995","590","10",ttown.getCDN()+"en/global/swf/expressInstall.swf",flashvars,params,attributes);
  }
} else {
  swfobject.embedSWF(ttown.getCDN()+"en/global/swf/TTHomepageLoader.swf","homepageFlash","995","590","10",ttown.getCDN()+"en/global/swf/expressInstall.swf",flashvars,params,attributes);
}


$(window).load(function (){
  if ( tt.getCookie('prospect') != '-1' ){
    var myDate = new Date();
    myDate.setDate(myDate.getDate()+365);
    tt.setCookie('prospect', '-1',myDate);
  }
});
