var mySettings = {
  chromeStyle: 'blue',
  footerStyleSet: 'transparentLight',
  footerDisplayMode: '',
  footerLegalSiteMapTarget:	'_top',
  footerLegalSafetyTarget: '_top',
  footerLegalPrivacyPolicyTarget: '_top',
  footerLegalIBATarget: '_top',
  footerLegalCustom: '<br /><br /><a href="'+ttown.getURL()+'member-agreements">Member Agreements</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="'+ttown.getURL_SSL()+'membership/account-management/">Member/Guest Services</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="'+ttown.getURL()+'help/">Help</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="'+ttown.getURL()+'help/contact">Contact Us</a>'
};

function loadNavBar(){
	$('.bottomInnerContainerMid dl dt a').css({background: 'url('+ttown.getCDN()+'en/global/img/img_homeSubNavSprite.jpg) no-repeat scroll'});
	$('.bottomInnerContainerMid dl').show();
};


$('.bottomInnerContainerMid dl').hide();
var flashvars = {
  contentSwfUrl: ttown.getCDN()+'en/home/swf/winter-holiday-2010.swf',
  aboutLink: ttown.getURL()+'about/',
  toontownLink: ttown.getURL()+'toon-hq/',
  playNowLink: ttown.getURL()+'play'
};
var params = {
  wmode: 'transparent',
  allowscriptaccess: 'always'
};
var attributes = {
  wmode: 'opaque',
  menu: 'false'
};

swfobject.embedSWF(ttown.getCDN()+"en/home/swf/winter-holiday-2010-loader.swf", "homepageFlash", "995", "590", "10",ttown.getCDN()+"en/global/swf/expressInstall.swf", flashvars, params, attributes);


$(window).load(function (){
  if ( tt.getCookie('prospect') != '-1' ){
    $('#atlasHomeProspect').html('<img height="1" width="1" src="http://switch.atdmt.com/action/tgmttn_homeprospect_1" />');
    $('#atlasHomeProspect2').html('<img src="http://ad.yieldmanager.com/pixel?id=755164&t=2" width="1" height="1" />');
    cto.pageName='toontown_prospect';
    cto.track();
    var myDate = new Date();
    myDate.setDate(myDate.getDate()+365);
    tt.setCookie('prospect', '-1',myDate);
  }
});