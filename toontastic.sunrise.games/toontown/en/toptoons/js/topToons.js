// Category Display names

var categoryName = new Object();
	categoryName["buildingDefeated"]="Buildings Recovered ";
	categoryName["minigame"]="Trolley Games Played";
	categoryName["questComplete"]="Tasks Completed";
	categoryName["fishedFish"]="Fish Caught";
	categoryName["jellybeansearned"]="Jellybeans Earned";
	categoryName["catalog-purchase"]="Cattlelog Items Purchased";
	categoryName["gavegifts"]="Gifts Given";
	categoryName["golf_ace"]="Holes in One";
	categoryName["golf_underPar"]="Courses Under Par";
	categoryName["bosswon_s"]="Sellbot V.P. Victories";     /* TT change */
	categoryName["bosswon_m"]="Cashbot C.F.O. Victories";   /* TT change */
	categoryName["bosswon_l"]="Lawbot C.J. Victories";      /* TT change */
	categoryName["bosswon_c"]="Bossbot C.E.O. Victories";   /* TT change */
	categoryName["kartingwon"]="Most Races Won";
	categoryName["gamesplayed"]="Trolley Games Played";
	categoryName["fishcaught"]="Fish Caught";
	categoryName["cogsdefeated"]="Cogs Defeated";
	categoryName["fishbingowin"]="Fish Bingo";

// Category Descriptions
var categoryDescription = new Object();
	categoryDescription["buildingDefeated"]="These are the Top Toons who recovered the most Toon buildings from the Cogs yesterday. Thanks for taking the streets back!";
	categoryDescription["minigame"]="These are the Top Toons who played the most Trolley Games yesterday. These Toons really know how to get their game on!";
	categoryDescription["questComplete"]="These are the Top Toons who completed the most ToonTasks yesterday. Way to do your Toon duty!";
	categoryDescription["fishedFish"]="These are the Top Toons who caught the most fish yesterday. They must be swimming in jellybeans!";
	categoryDescription["jellybeansearned"]="These are the Top Toons who earned the most jellybeans yesterday. We bet these Toons like to fish!";
	categoryDescription["catalog-purchase"]='These are the Top Toons who purchased the most Cattlelog items yesterday. Clarabelle says, "Thanks for the business!"';
	categoryDescription["gavegifts"]="These are the Top Toons who gave out the most gifts yesterday. Your Toon generosity is truly appreciated!";
	categoryDescription["golf_ace"]="These are the Top Toons who made the most minigolf holes in one yesterday. You're all Toontastic shots!";
	categoryDescription["golf_underPar"]="These are the Top Toons who completed the most minigolf courses under par yesterday. Remember - no mulligans allowed!";
	categoryDescription["bosswon_s"]="These are the Top Toons who earned the most Sellbot V.P. victories yesterday. You sure showed that Cog who's boss!";
	categoryDescription["bosswon_m"]="These are the Top Toons who earned the most Cashbot C.F.O. victories yesterday. It's like dropping a safe on a Cog!";
	categoryDescription["bosswon_l"]="These are the Top Toons who earned the most Lawbot C.J. victories yesterday. Looks like his verdict came in!";
	categoryDescription["bosswon_c"]="These are the Top Toons who earned the most Bossbot C.E.O. victories yesterday. You sure downsized that Cog!";
	categoryDescription["kartingwon"]="These are the Top Toons who won the most Goofy Speedway races yesterday. These Toons are fast enough!";
	categoryDescription["gamesplayed"]="These are the Top Toons who played the most Trolley Games yesterday. These Toons really know how to get their game on!";
	categoryDescription["fishcaught"]="These are the Top Toons who caught the most fish yesterday. They must be swimming in jellybeans!";
	categoryDescription["cogsdefeated"]="These are the Top Toons who defeated the most Cogs yesterday. They really got those gears spinning!";
	categoryDescription["fishbingowin"]="These are the Top Toons who earned the most jellybeans during Fish Bingo yesterday. Congratulations!";

/* TTP-377 - order determines dropdown menu order. */
var order= ["cogsdefeated", "buildingDefeated", "catalog-purchase", "gavegifts", "questComplete", "minigame", "gamesplayed", "kartingwon", "fishedFish", "fishcaught", "jellybeansearned", "golf_ace", "golf_underPar", "bosswon_s", "bosswon_m", "bosswon_l", "bosswon_c", "fishbingowin"];

$(function() {

	disTT.uniform.init();
	$('#print').click(function() {
		window.print();
		return false;
	});
	//set a base category to display..
	//var baseCategory = "BuildingsRecovered";
	//var baseCategory = "jellybeansearned";/*TT change*/
  var baseCategory = "cogsdefeated";/*TTP-377 change*/
	var baseCatDescriptor = categoryDescription[baseCategory];
	var baseCatTitle = categoryName[baseCategory];

	//...and set it as default description and title
	$('.textDesc').append('<h2>'+baseCatTitle+'</h2><p>'+baseCatDescriptor+'</p>');

		$.ajax({
			//url: "../xml/topToons.xml",
			url: "/content/en/toptoons/today/TopToons.xml",/*TT change*/
		  	type: "POST",
		  	dataType: "xml",
			success: function(xml) {
			var dropDownInfo = new Array();
			var descInfo = new Array();
			var carouselInfo = new Array();
			var triggerInfo = new Array();

      var currCatID;    /* TTP-377 */
      var currCatDesc;  /* TTP-377 */
      var currCatName;  /* TTP-377 */

      /* TTP-377 - Loop stores nodes in desired order for dropdown based on array order.
         orderIndex is total number of dropdown menu items.
      */
      for ( var orderIndex = 0; orderIndex <= 17; orderIndex++ ){

          $(xml).find('toptoons').children(order[orderIndex]).each(function(index){  /* TTP-377 */

            //get the things we are going to need
            currCatID = $(this).get(0).nodeName;            /* TTP-377 */
            currCatDesc = categoryDescription[currCatID];   /* TTP-377 */
            currCatName = categoryName[currCatID];          /* TTP-377 */

            var ttUndefined = false;//TT change for undefined

            if ( currCatID == undefined || currCatDesc == undefined || currCatName  == undefined){
              //TT change for undefined
              ttUndefined = true;
            }

            if ( !ttUndefined ){
              //TT change for undefined
              dropDownInfo[orderIndex] = '<option class="'+currCatID+'">'+currCatName+'</option>';   /* TTP-377 */
            }


            //populate the descriptions
            if ( !ttUndefined ){
              //TT change for undefined
              descInfo[orderIndex] = '<div class="textDesc '+currCatID+'" style="display:none"><h2>'+currCatName+'</h2><p>'+currCatDesc+'</p></div>';  /* TTP-377 */
            }

            var userInfo = new Array();

            if ( !ttUndefined ){
              //TT change for undefined
              carouselInfo[orderIndex] = '<li class="'+currCatID+'">';   /* TTP-377 */


              $(this).find('user').each(function(place){
                var name = $(this).attr('name');
                var imgSrc = $(this).find('imgSmall').attr('src');

                //points placeholder
                var points = $(this).attr('points');
                points = addCommas(points);
                //carouselInfo[index] += '<table border="0" cellspacing="5" cellpadding="5" class="place'+place+'"><tr><th>'+name+'</th></tr><tr><td>'+points+'</td></tr></table><img class="place'+place+'" src="../img/topToons/'+imgSrc+'" width="100" height="100" alt="'+name+'"/>';
                carouselInfo[orderIndex] += '<table border="0" cellspacing="5" cellpadding="5" class="place'+place+'"><tr><th>'+name+'</th></tr><tr><td>'+points+'</td></tr></table><img class="place'+place+'" src="/content/en/toptoons/today/'+imgSrc+'" width="50" height="50" alt="'+name+'"/>';/*TT change*/

              });

              carouselInfo[orderIndex] += '</li>';   /* TTP-377 */

              triggerInfo[orderIndex] = '<li class="'+currCatID+'"></li>';   /* TTP-377 */

            }//TT change for undefined


          });

        };  /* TTP-377 */


				//populate the carousel
				$('div#carousel ul').append(carouselInfo.join(""));

				//populate the dropdown
				$('#topToonSelector').append(dropDownInfo.join(""));

				//populate the descriptions
				$('.boxMid960').prepend(descInfo.join(""));

				//make hidden triggers that we can bind the carousel to later
				$('ul#blindTriggers').append(triggerInfo.join(""));

				//function to move the carousel and description based on the dropdown
				//make a variable to store what the old selection was
				var prevSelection = "";

				//note: mouseup is unreliable in IE.  use click
				$('div.selectorContainer select').click(function() {

					//what is the current selection, using class instead of val, for consistency's sake, across the DOM
					var currentSelection = $('select#topToonSelector option:selected').attr('class');

					if (currentSelection != ''){

						//make sure that we don't do an action if the same selection is already up.  this section's counterpart is in the carousel callback in global.js
						if (prevSelection != currentSelection){
							//out with the old text description, in with the new
							$('div.boxMid960 div.textDesc.'+currentSelection).show().siblings('div.textDesc').hide();
							//here is where the blind triggers come in.  this triggers the carousel to move.
							$('ul#blindTriggers li.'+currentSelection).trigger('click');
						}

						//set the current selection to the previous, so we can test against it later
						prevSelection = currentSelection;
					}

				});

				//make an array of the blind triggers for the carousel
				var carouselTriggers = $.makeArray($('ul#blindTriggers li'));

				//now that we are all done with the ajax loading, activate the carousel
				disTT.carousel.init();
				//and this make the carousel for this page, with the triggers for each slide, which are hidden
				disTT.carousel.activateTopToons(carouselTriggers);

				//lazy load the images
				$('#carousel ul img').lazyload({
					container: $("#carousel ul"),
					//placeholder : "../img/blank.gif",
					placeholder : "https://toontastic.sunrise.games/toontown/en/global/img/blank.gif",/*TT change*/
					effect : "fadeIn"
				});

				//jquery png fix for IE6
				$('#carousel img').pngFix();

			 },

		  error: function(xhr, status, errorThrown) {
			//just in case
			 //alert(errorThrown+'\n'+status+'\n'+xhr.statusText);/*TT change*/

		  }


		});


});

//jQuery png fix for the inline user images that are loaded via AJAX

/*
	jQuery Version:				jQuery 1.3.2
	Plugin Name:				pngFix V 1.0
	Plugin by: 					Ara Abcarians: http://ara-abcarians.com
	License:					pngFix is licensed under a Creative Commons Attribution 3.0 Unported License
								Read more about this license at --> http://creativecommons.org/licenses/by/3.0/
*/
(function($) {
    $.fn.pngFix = function(options) {

		return this.each(function() {
			var obj = $(this);
			// detect IE6
			if(jQuery.browser.msie && parseInt(jQuery.browser.version) == 6){
				obj.each(function(){

					// set variables
					var obj = $(this);
					var bg = obj.css("backgroundImage");

					// check if its an image
					if(obj.attr("src")){
						//var clear="../img/blank.gif"; //path to clear.gif
						var clear="https://toontastic.sunrise.gamestown/en/global/img/blank.gif"; //path to clear.gif /*TT change*/
						obj.css({
							height: obj.height(),
							width: obj.width(),
							filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='"+obj.attr("src")+"', sizingMethod='crop')"
						});
						obj.attr({ src: clear });
					} else {
						// but if its a bg image through css then do this
						var img = bg.substring(bg.indexOf('"') + 1, bg.lastIndexOf('"'));
						obj.css({
							filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + img + "', sizingMethod='scale')",
							backgroundImage: "none"
						});
					}
				});

			} // END if

		}); // END: return this

		// returns the jQuery object to allow for chainability.
        return this;
    };
})(jQuery);

/*
 * Lazy Load - jQuery plugin for lazy loading images
 *
 * Copyright (c) 2007-2009 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   http://www.appelsiini.net/projects/lazyload
 *
 * Version:  1.4.0
 *
 */
(function($) {

    $.fn.lazyload = function(options) {
        var settings = {
            threshold    : 0,
            failurelimit : 0,
            event        : "scroll",
            effect       : "show",
            container    : window
        };

        if(options) {
            $.extend(settings, options);
        }

        /* Fire one scroll event per scroll. Not one scroll event per image. */
        var elements = this;
        if ("scroll" == settings.event) {
            $(settings.container).bind("scroll", function(event) {

                var counter = 0;
                elements.each(function() {
                    if ($.abovethetop(this, settings) ||
                        $.leftofbegin(this, settings)) {
                            /* Nothing. */
                    } else if (!$.belowthefold(this, settings) &&
                        !$.rightoffold(this, settings)) {
                            $(this).trigger("appear");
                    } else {
                        if (counter++ > settings.failurelimit) {
                            return false;
                        }
                    }
                });
                /* Remove image from array so it is not looped next time. */
                var temp = $.grep(elements, function(element) {
                    return !element.loaded;
                });
                elements = $(temp);
            });
        }

        return this.each(function() {
            var self = this;
            $(self).data("original", $(self).attr("src"));

            if ("scroll" != settings.event || (
                         $.abovethetop(self, settings) ||
                         $.leftofbegin(self, settings) ||
                         $.belowthefold(self, settings) ||
                         $.rightoffold(self, settings) )) {
                if (settings.placeholder) {
                    $(self).attr("src", settings.placeholder);
                } else {
                    $(self).removeAttr("src");
                }
                self.loaded = false;
            } else {
                self.loaded = true;
            }

            /* When appear is triggered load original image. */
            $(self).one("appear", function() {
                if (!this.loaded) {
                    $("<img />")
                        .bind("load", function() {
                            $(self)
                                .hide()
                                .attr("src", $(self).data("original"))
                                [settings.effect](settings.effectspeed);
                            self.loaded = true;
                        })
                        .attr("src", $(self).data("original"));
                };
            });

            /* When wanted event is triggered load original image */
            /* by triggering appear.                              */
            if ("scroll" != settings.event) {
                $(self).bind(settings.event, function(event) {
                    if (!self.loaded) {
                        $(self).trigger("appear");
                    }
                });
            }
        });

    };

    /* Convenience methods in jQuery namespace.           */
    /* Use as  $.belowthefold(element, {threshold : 100, container : window}) */

    $.belowthefold = function(element, settings) {
        if (settings.container === undefined || settings.container === window) {
            var fold = $(window).height() + $(window).scrollTop();
        } else {
            var fold = $(settings.container).offset().top + $(settings.container).height();
        }
        return fold <= $(element).offset().top - settings.threshold;
    };

    $.rightoffold = function(element, settings) {
        if (settings.container === undefined || settings.container === window) {
            var fold = $(window).width() + $(window).scrollLeft();
        } else {
            var fold = $(settings.container).offset().left + $(settings.container).width();
        }
        return fold <= $(element).offset().left - settings.threshold;
    };

    $.abovethetop = function(element, settings) {
        if (settings.container === undefined || settings.container === window) {
            var fold = $(window).scrollTop();
        } else {
            var fold = $(settings.container).offset().top;
        }
        return fold >= $(element).offset().top + settings.threshold  + $(element).height();
    };

    $.leftofbegin = function(element, settings) {
        if (settings.container === undefined || settings.container === window) {
            var fold = $(window).scrollLeft();
        } else {
            var fold = $(settings.container).offset().left;
        }
        return fold >= $(element).offset().left + settings.threshold + $(element).width();
    };
    /* Custom selectors for your convenience.   */
    /* Use as $("img:below-the-fold").something() */

    $.extend($.expr[':'], {
        "below-the-fold" : "$.belowthefold(a, {threshold : 0, container: window})",
        "above-the-fold" : "!$.belowthefold(a, {threshold : 0, container: window})",
        "right-of-fold"  : "$.rightoffold(a, {threshold : 0, container: window})",
        "left-of-fold"   : "!$.rightoffold(a, {threshold : 0, container: window})"
    });

})(jQuery);

//format the points all nice
function addCommas(nStr)
{
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

