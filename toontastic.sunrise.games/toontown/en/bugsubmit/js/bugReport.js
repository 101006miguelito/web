//Init Pages
var reportBug=false;
var reportBugNoPlugin=false;
var graphicsDrivers=false;
var missedRequirements=false;
var reportBugPCLauncher=false;
var reportBugLauncher=false;//Mac

var installer = null;
var installerError = '';
var installerEmbedded = 0;
var pageFunction = 'UNSET';
var deployment = '';
//var begin = new Date();

var SPECS_PLATFORM_MAC=false;
var noScreenshot=false;


function checkAttachFileLog(xAttach)
{
	xAttach=xAttach.toLowerCase();
	var varAcceptTypes='.log, .txt';//do not allow .doc formats
	var varReturn=true;
	if (xAttach != '' && xAttach != ' '){
		var varTypePos=xAttach.lastIndexOf('.');
		var varType=xAttach.substring(varTypePos);
		if (varAcceptTypes.indexOf(varType)>-1){
      return true;
		}else{
      return false;
		}
	}else{
    return false;
  }
}


function attachFileLogErr(id,err)
{
  if (err=='2'){
    if (SPECS_PLATFORM_MAC){
      html='To submit a problem report to Disney&#39;s Toontown Online, you must include your .LOG file, located under <user>/Library/Logs';
    }else{
      html='To submit a problem report to Disney&#39;s Toontown Online, you must include your .LOG file, located under &#92;Program Files&#92;Disney&#92;Disney Online&#92;ToontownOnline';
    }
  }else{
    html='Sorry, we only accept log file in the following formats: .LOG or .TXT';
  }
  document.getElementById(id).innerHTML=html;
}


function attachFilePhaseLogErr(id,err)
{
  if (err=='2'){
    html='To submit a problem report to Disney&#39;s Toontown Online, you must include your phase1.log file, located under <user>/Library/Logs';
  }else{
    html='Sorry, we only accept log file in the following formats: .LOG or .TXT';
  }
  document.getElementById(id).innerHTML=html;
}


function checkAttachFileScreenshot(xAttach)
{
	xAttach=xAttach.toLowerCase();
	var varAcceptTypes='.jpg, .gif, .bmp, .png';
	var varReturn=true;
	if (xAttach != '' && xAttach != ' '){
		var varTypePos=xAttach.lastIndexOf('.');
		var varType=xAttach.substring(varTypePos);
		if (varAcceptTypes.indexOf(varType)>-1){
      return true;
		}else{
		return false;
		}
	}else{
    return false;
  }
}


function attachFileScrnshotErr(id)
{
  html='Sorry, we only accept screenshot in the following formats: .JPG, .GIF, .BMP or .PNG';
  document.getElementById(id).innerHTML=html;
}


function descriptionErr(id)
{
  html='Please enter a description';
  document.getElementById(id).innerHTML=html;
}


function deleteErrs()
{

  document.getElementById('descriptionErr').innerHTML='';

	document.getElementById('acctNameErr').innerHTML='';

  if (SPECS_PLATFORM_MAC && reportBugNoPlugin){
    document.getElementById('inputPhaseLogErr').innerHTML='';
  }

  if (reportBugNoPlugin){
    document.getElementById('inputLogErr').innerHTML='';
  }

  document.getElementById('inputScrnShotErr').innerHTML='';
	document.getElementById('emailErr').innerHTML='';

  if (reportBugNoPlugin){
    document.getElementById('acctNameErr').innerHTML='';
  }
}


function updateBrowseField()
{
		if (ua['platform']=='Win32' && ua['browser']=='IE' && ua['browserVer'] >=6 && (ua['os']=='98' || ua['os']=='me' || ua['os']=='2000' || ua['os']=='xp')){
			document.getElementById('dfile_1').value = santizeBrowseField(document.getElementById('file_1').value);
		} else {

			document.getElementById('dfile_1').value = santizeBrowseField(document.getElementById('file_1').value);
			document.getElementById('dfile_2').value = santizeBrowseField(document.getElementById('file_2').value);
			document.getElementById('dfile_3').value = santizeBrowseField(document.getElementById('file_3').value);
		}
}

function santizeBrowseField(text)
{
  var pos1 = -1;
  var pos2 = -1;

  if(text != ''){
    //Remove fake path

    pos1 = text.indexOf('fake_path\\');
    pos2 = text.indexOf('fakepath\\');

    //alert('text: ' + text +"\n"+ 'pos1: '+pos1 +"\n"+ 'pos2: '+pos2);
    if (pos1 > -1){
      return text.substring(pos1+10); // Grab the filename only
    }else
    if (pos2 > -1){
      return text.substring(pos2+9); // Grab the filename only
    }else{
      return text;
    }
  }else{
    return text;
  }
}

function checkTextArea(id)
{
  var description = document.getElementById(id).value;
  if (description == '' || description==null){
    html='Please enter a description';
    document.getElementById('descriptionErr').innerHTML=html;
    return false;
  }else{
    return true;
  }
}


function emailErr(id,err)
{
  if (err=='2'){
    html='Please enter an e-mail address in the following format xxx@xxx.xxx';
  }else{
    html='Please enter an e-mail address';//null email
  }
  document.getElementById(id).innerHTML=html;
}


function acctNameErr(id)
{
  html='Please enter an account name';
  document.getElementById(id).innerHTML=html;
}


function echeck(str)
{
  var at='@'
  var dot='.'
  var lat=str.indexOf(at)
  var lstr=str.length
  var ldot=str.indexOf(dot)
  if (str == 'xxx@xxx.xxx') {
     emailErr('emailErr','2');
    return false;
  }

  if (str.indexOf(at)==-1){
     emailErr('emailErr','2');
    return false;
  }

  if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr){
     emailErr('emailErr','2');
    return false;
  }

  if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr){
     emailErr('emailErr','2');
    return false;
  }

  if (str.indexOf(at,(lat+1))!=-1){
    emailErr('emailErr','2');
    return false;
  }

  if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot){
    emailErr('emailErr','2');
    return false;
  }

  if (str.indexOf(dot,(lat+2))==-1){
    emailErr('emailErr','2');
    return false;
  }

  if (str.indexOf(' ')!=-1){
    emailErr('emailErr','2');
    return false;
  }

  return true;
}


function sendReportBug()
{
  //bugReportBlobs.inc.php

  deleteErrs();

  var formError=false;
  var submitForm=false;

  var checkDescription=checkTextArea('description_area');
  if (!checkDescription){
    formError=true;
  }

  if (SPECS_PLATFORM_MAC){
    var file_0=document.forms['report_form'].elements['file_0'].value;
    if (file_0=='' || file_0==null){
      attachFilePhaseLogErr('inputPhaseLogErr', '2');
      formError=true;
    }else{
      var checkFileTypePhaseLog=checkAttachFileLog(file_0);
      if (checkFileTypePhaseLog){
      }else{
        attachFilePhaseLogErr('inputPhaseLogErr');
        formError=true;
      }
    }
  }

  var emailID=document.forms['report_form'].elements['custom_field_10'];
  if ((emailID.value==null)||(emailID.value=='')){
    emailErr('emailErr');
    formError=true;
  }else if (echeck(emailID.value)==false){
    formError=true;
  }

	var checkAcctName=document.forms['report_form'].elements['custom_field_14'];
  if (checkAcctName.value=='' || checkAcctName.value==null){
    acctNameErr('acctNameErr');
    formError=true;
  }

  if (noScreenshot){
  }else{
    var file_1=document.forms['report_form'].elements['file_1'].value;
    if (file_1=='' || file_1==null){
     //do not validate since the there's no file attach and this file is not required
    }else{
      var checkFileTypeScrnshot=checkAttachFileScreenshot(file_1);
      if (checkFileTypeScrnshot){
      }else{
        attachFileScrnshotErr('inputScrnShotErr');
        formError=true;
      }
    }
  }

  if (!formError && !submitForm){
    //submit form when there are no form errors and only submit the form once
    submitForm=true;
    document.getElementById('submitBtn').disabled=true;
    changeBugReportBtn();
    document.forms['report_form'].submit();
  }

}


function sendReportBugNoPlugin()
{
  //reportBugNoPlugin.php

  deleteErrs();
  var formError=false;
  var submitForm=false;

  var checkDescription=checkTextArea('description_area');
  if (!checkDescription){
    formError=true;
  }

  if (SPECS_PLATFORM_MAC){
    var file_0=document.forms['report_form'].elements['file_0'].value;
    if (file_0=='' || file_0==null){
      attachFilePhaseLogErr('inputPhaseLogErr', '2');
      formError=true;
    }else{
      var checkFileTypePhaseLog=checkAttachFileLog(file_0);
      if (checkFileTypePhaseLog){
      }else{
        attachFilePhaseLogErr('inputPhaseLogErr');
        formError=true;
      }
    }
  }

  var file_1=document.forms['report_form'].elements['file_1'].value;
  if (file_1=='' || file_1==null){
    attachFileLogErr('inputLogErr', '2');
    formError=true;
  }else{
    var checkFileTypeLog=checkAttachFileLog(file_1);
    if (checkFileTypeLog){
    }else{
      attachFileLogErr('inputLogErr');
      formError=true;
    }
  }

  var file_2=document.forms['report_form'].elements['file_2'].value;
  if (file_2=='' || file_2==null){
   //do not validate since the there's no file attach and this file is not required
  }else{
    var checkFileTypeScrnshot=checkAttachFileScreenshot(file_2);
    if (checkFileTypeScrnshot){
    }else{
      attachFileScrnshotErr('inputScrnShotErr');
      formError=true;
    }
  }

  var emailID=document.forms['report_form'].elements['custom_field_10'];
  if ((emailID.value==null)||(emailID.value=='')){
    emailErr('emailErr');
    formError=true;
  }else if (echeck(emailID.value)==false){
    formError=true;
  }

  var checkAcctName=document.forms['report_form'].elements['custom_field_14'];
  if (checkAcctName.value=='' || checkAcctName.value==null){
    acctNameErr('acctNameErr');
    formError=true;
  }

  if (!formError && !submitForm){
    //submit form when there are no form errors and only submit the form once
    submitForm=true;
    document.getElementById('submitBtn').disabled=true;
    changeBugReportBtn();
    document.forms['report_form'].submit();
  }

}


function sendReportBugPCLauncher()
{
  //PC Launcher: reportBugPCLauncher.php

  deleteErrs();
  var formError=false;
  var submitForm=false;

  var checkDescription=checkTextArea('description');
  if (!checkDescription){
    formError=true;
  }

  if (SPECS_PLATFORM_MAC){
    var file_0=document.forms['report_form'].elements['file_0'].value;
    if (file_0=='' || file_0==null){
      attachFilePhaseLogErr('inputPhaseLogErr', '2');
      formError=true;
    }else{
      var checkFileTypePhaseLog=checkAttachFileLog(file_0);
      if (checkFileTypePhaseLog){
      }else{
        attachFilePhaseLogErr('inputPhaseLogErr');
        formError=true;
      }
    }
  }

  var emailID=document.forms['report_form'].elements['custom_field_10'];
  if ((emailID.value==null)||(emailID.value=='')){
    emailErr('emailErr');
    formError=true;
  }else if (echeck(emailID.value)==false){
    formError=true;
  }

  if (noScreenshot){
  }else{
    var file_1=document.forms['report_form'].elements['file_1'].value;
    if (file_1=='' || file_1==null){
     //do not validate since the there's no file attach and this file is not required
    }else{
      var checkFileTypeScrnshot=checkAttachFileScreenshot(file_1);
      if (checkFileTypeScrnshot){
      }else{
        attachFileScrnshotErr('inputScrnShotErr');
        formError=true;
      }
    }
  }

  if (!formError && !submitForm){
    //submit form when there are no form errors and only submit the form once
    submitForm=true;
    document.getElementById('submitBtn').disabled=true;
    changeBugReportBtn();
    document.forms['report_form'].submit();
  }
}


function sendReportBugLauncher()
{
  //MAC Launcher: reportBugLauncher.php

  deleteErrs();
  var formError=false;
  var submitForm=false;

  var checkDescription=checkTextArea('description');
  if (!checkDescription){
    formError=true;
  }

  var emailID=document.forms['report_form'].elements['userEmail'];
  if ((emailID.value==null)||(emailID.value=='')){
    formError=true;
    emailErr('emailErr');
  }else if (echeck(emailID.value)==false){
    formError=true;
  }

  if (noScreenshot){
  }else{
    var file_1=document.forms['report_form'].elements['screenShot'].value;
    if (file_1=='' || file_1==null){
     //do not validate since the there's no file attach and this file is not required
    }else{
      var checkFileTypeScrnshot=checkAttachFileScreenshot(file_1);
      if (checkFileTypeScrnshot){
      }else{
        attachFileScrnshotErr('inputScrnShotErr');
        formError=true;
      }
    }
  }

  if (!formError && !submitForm){
    //submit form when there are no form errors and only submit the form once
    submitForm=true;
    document.getElementById('submitBtn').disabled=true;
    changeBugReportBtn();
    document.forms['report_form'].submit();
  }
}


function getBugReportDeployment(confTTServerID,postDeployment)
{
  var deployment='UKNOWN';

  if (confTTServerID=='UKPLAY'){
    deployment='UK';
  }else{
    if ( SPECS_PLATFORM_MAC || postDeployment=='US_MAC'){
      deployment='US_MAC';
    }else{
      deployment='US';
    }
  }

  return deployment;

}


function changeBugReportBtn()
{
  var btn=document.report_form.submitBtn;
  btn.value='Please wait';
}

function getUserErrorString()
{
  url = location.search;

  //alert('location search params '' + url + ''');
  userErrorStringText = getURLParam(url, 'userErrorString');

  //logDebug('getUserErrorString', 'UserErrorString from URL '' + userErrorStringText + ''');

  //alert(userErrorStringText);
  if (userErrorStringText.length) {
    return userErrorStringText;
  }
  else
    return installer.getValue('UserErrorString');
}

function getToontownNotes() {
		//alert('Begin getToontownNotes');
    if(initActiveX()) {
        switch(deployment)
        {
        case 'JP': case 'BR': case 'FR':
            installer.InitForStatus();  // Vista-compatible API hint
            break;
        }
        return getUserErrorString() + '\nLog:\n' + installer.getValue('ToontownNotes') + '\n[end of logs]';
    }
    else
        return '';
}

function getURLParam(url, name)
{
  if(url) {
    index = url.indexOf(name);
    if (index != -1) {
      countbegin = (url.indexOf('=', index) + 1);
      countend = url.indexOf('&', index);
      if (countend == -1) {
        countend = url.length;
      }
      val = url.substring(countbegin, countend);
      return unescape(val);
    }
  }
  return '';
}

function initActiveX() {
  //alert('initActiveX: Begin');

  //
  // Short-cut if the activeX is already up
  //
  if (installer) {
    //alert('initActiveX: ActiveX already initted - but called initActiveX again.');
    return 1;
  }

  //
  // Otherwise go for it
  //
  result = 0;

  if (installerCtrl) {
    //alert('initActiveX: installerCtrl has value *' + installerCtrl + '*');
  }

  try {
    result = installerCtrl.Init();
  } catch (e) {
		//alert('initActiveX: Got e of *' + e.message + '*');
  }

  //alert('initActiveX :Init result is *' + result + '*');
  if (result != 0) {
    installer = installerCtrl;
  }
  return result;
}