//for the auto-scroll below
var pos = 0;

$(function() {

	$('.swappableItems:first').fadeIn();
	
	//give each item in the dock a hover state to show the tooltip
	$('ul.dock li span').each(function() {
	
		$(this).hover(function() {
		
			$(this).addClass('hover');
			$(this).parent().find('a.toolTip').show();
			
		}, function() {
			$(this).removeClass('hover')
			$(this).parent().find('> .toolTip').hide();
		});
		
		$(this).click(function() {
			$('.clicked').removeClass('clicked');
			$(this).addClass('clicked');
			var id = $(this).parent().attr('id');
			//strip off the Docks part of the ID
			id = id.substr(0, id.length-4);

			$('.swappableItems:visable').hide();
			$('.swappableItems').filter("#"+id).show();
			
			pos = $('ul.dock li').index($(this).parent());
			//pos
		});
	});
	
});

setInterval(function(){
		
		var dock = $('ul.dock li');
		//make sure someone is not trying to do something
		if (!dock.children().hasClass('.hover')) {
			var nextLi = dock.eq(pos);
			
			$('.clicked').removeClass('clicked');
			nextLi.find('span.rotate').addClass('clicked');
			
			var id = nextLi.attr('id');
			//strip off the Docks part of the ID
			id = id.substr(0, id.length-4);
			
			$('.swappableItems:visable').hide();
			$('.swappableItems').filter("#"+id).show();
			
			if (pos >= (dock.length-1))
				pos = 0;
			else 
				pos++;
		}
	}, 10000	 );

