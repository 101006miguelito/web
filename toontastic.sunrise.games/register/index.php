<?php
  include '../../sunrise.games/config/config.php';
  include '../../sunrise.games/libs/Mail.php';

  $doLogin = $_POST['doLogin'];
  $username = strtolower($_POST['username']);
  $email = $_POST['email'];
  $firstName = $_POST['firstName'];
  $lastName = $_POST['lastName'];
  $password = $_POST['password'];
  $bdayYear = $_POST['bdayYear'];

  $currentYear = date('Y');
  $age = $currentYear - $bdayYear;

  if (empty($doLogin)) {
      $response = 'Why are you viewing this directly?';
      echo $response;
      return;
  }

  $saltedPassword = $salt . $password;
  $hashedPassword = password_hash($saltedPassword, PASSWORD_DEFAULT);
  $emailHash = bin2hex(random_bytes(16));

  // Check if the username is already taken.
  $userNameTakenQuery = $db->prepare('SELECT * FROM Users WHERE Username = ?');
  $userNameTakenQuery->bind_param('s', $username);
  $userNameTakenQuery->execute();

  $userNameTakenResult = $userNameTakenQuery->get_result();

  $statement = $db->prepare('INSERT INTO Users (Email, Username, Password, EmailHash, FirstName, LastName, Age) VALUES (?, ?, ?, ?, ?, ?, ?)');
  $statement->bind_param('ssssssi', $email, $username, $hashedPassword, $emailHash, $firstName, $lastName, $age);

  $emailContents = '';
  $emailContents = file_get_contents('assets/emailTemplate.html');
  $partsToMod = array('(PUTYOURNAMEHERE)', '(YOURUSERNAMEHERE)', '(YOUREMAILHERE)', '(verifyLinkHere)');
  $verifyLink = 'https://sunrise.games/verification/?emailAddress='.$email.'&emailHash='.$emailHash.'';
  $replaceWith = array($firstName, $username, $email, $verifyLink);

  for ($i = 0; $i < count($partsToMod); $i++) {
    $emailContents = str_replace($partsToMod[$i], $replaceWith[$i], $emailContents);
  }

  if ($userNameTakenResult->num_rows > 0) {
    $response = 'This username is already taken!';
  } else if ($statement->execute()) {
    if ($isDevelopment) {
      // In development, we don't want emails to be sent for verification.
      $response = 'Successfully registered account!';
    } else {
      // Dispatch the email.
      if (sendMail($email, $emailContents, 'toontown@disneyonline.com', "Disney's Toontown Online", 'Account Holder', "Welcome to Disney's Toontown Online!", $emailKey)) {
        $response = 'Successfully registered account and dispatched email!';
      } else {
        $response = 'Something has gone wrong.';
      }
  }
}
  echo $response;