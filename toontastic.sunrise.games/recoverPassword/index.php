<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
    <link href="/images/global/disney/global.css" rel="stylesheet" type="text/css" media=" screen">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Recover your password</title>
</head>
<body>

<!-- START MAINCONTAINER -->
<div id="maincontainer">

    <!-- START CENTERED PANE -->
    <div id="centeredpane">

    <!-- START HEADER -->
    <div id="header">
    </div>
    <!-- END HEADER -->

        <!-- START CONTENT BELOW HEADER -->
        <div id="maincontent">
        <h1> Forgot Your Password?</h1>
                <div id ="instructions">
                    <p><strong>If you have forgotten your password, please enter the required information below to receive instructions on how to reset it.</strong></p>
                </div>
                <p>

                </p>
        <div class="gutter padtop">
        <form method="POST" action="recoverPassword.php">
        <INPUT type="hidden" name="SUBMIT" value="1">

            <INPUT type="hidden" name="appRedirect" value="https://toontastic.sunrise.games">

        <table cellpadding="4">
        <tr>
            <td class="formlabel">Member Name:</td>
            <td><INPUT class="plain narrow" type=text name="username" value="" size=30 maxLength=64> </td>
        </tr>
        <tr>
            <td class="formlabel"></td>
            <td><INPUT class="plain" name="SUBMIT" type="SUBMIT" value="Send"> </td>
        </tr>
        </table>
    </FORM>
        </div>
        </div>
    <!-- END CONTENT BELOW HEADER -->
        <div id="footer">

<a href="https://register.go.com/go/goFAQ">Help</a>
&nbsp;|&nbsp;<a href="https://disney.go.com/corporate/privacy/pp_wdig.html" >Privacy Policy/Your California Privacy Rights</a>
&nbsp;|&nbsp;<a href="https://corporate.disney.go.com/corporate/pp_wdig.html#ThirdParties">Interest-Based Ads</a>
&nbsp;|&nbsp;<a href="https://disney.go.com/corporate/legal/terms.html">Terms of Use</a><br/>
Copyright &copy; 2013 Walt Disney Internet Group. All rights reserved.
        </div>

    </div>
    <!-- END CENTERED PANE -->
</div>
<!-- END MAINCONTAINER -->
</body>