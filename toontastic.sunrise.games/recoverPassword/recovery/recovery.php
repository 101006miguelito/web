<?php
  include '../../../sunrise.games/config/config.php';

  $response = '';
  $validRequest = false;

  if (isset($_POST['SUBMIT']) && isset($_GET['username'])) {
      $validRequest = true;
  }

  $username = $_GET['username'];
  $recoverHash = $_GET['recoverHash'];

  $curDate = date('Y-m-d H:i:s');

  // Query the database.
  $stmt = $db->prepare('SELECT * FROM PasswordReset WHERE Hash = ? AND Username = ?');
  $stmt->bind_param('ss', $recoverHash, $username);
  $stmt->execute();

  $result = $stmt->get_result();

  if ($result->num_rows < 1) {
      // Invalid link.
      header('Location: https://toontastic.sunrise.games');
  } else {
      while ($arr = $result->fetch_assoc()) {
          $expDate = $arr['Expire'];

          if ($expDate >= $curDate) {
              $notExpired = true;
          } else {
              // Delete the expired recovery hash and redirect the user.
              $deleteHash = $db->prepare('DELETE FROM PasswordReset WHERE Hash = ? AND Username = ?');
              $deleteHash->bind_param('ss', $recoverHash, $username);
              $deleteHash->execute();

              header('Location: https://toontastic.sunrise.games');
          }
        }
    }

  if ($validRequest && $notExpired) {
      $nonHashedPass = $_POST['password'];

      // Combine the salt and password.
      $saltedPassword = $salt . $nonHashedPass;

      // Hash the salted password.
      $hashedPassword = password_hash($saltedPassword, PASSWORD_DEFAULT);

      $userNameQuery = $db->prepare('SELECT * FROM Users WHERE Username = ?');
      $userNameQuery->bind_param('s', $username);
      $userNameQuery->execute();

      $userNameResult = $userNameQuery->get_result();

      if ($userNameResult->num_rows > 0) {
          // Reset the password.
          $updatePass = $db->prepare('UPDATE Users SET Password = ? WHERE Username = ?');
          $updatePass->bind_param('ss', $hashedPassword, $username);
          $updatePass->execute();

          $deleteHash = $db->prepare('DELETE FROM PasswordReset WHERE Hash = ? AND Username = ?');
          $deleteHash->bind_param('ss', $recoverHash, $username);
          $deleteHash->execute();

          $response = 'Password has been reset!';

          header('Location: https://toontastic.sunrise.games');
      }
    }
    echo $response;