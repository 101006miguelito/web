<?php
  include '../../sunrise.games/config/config.php';

  // Mail
  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\SMTP;
  use PHPMailer\PHPMailer\Exception;

  require '../../sunrise.games/libs/PHPMailer/src/Exception.php';
  require '../../sunrise.games/libs/PHPMailer/src/PHPMailer.php';
  require '../../sunrise.games/libs/PHPMailer/src/SMTP.php';

  $validRequest = false;

  if(isset($_POST['username'])) {
      $validRequest = true;
  }

  $username = $_POST['username'];

  $recoverHash = bin2hex(random_bytes(16));
  $recoverLink = 'https://toontastic.sunrise.games/recoverPassword/recovery/?username='.$username.'&recoverHash='.$recoverHash.'';

  if ($validRequest) {
      // Retrieve the email tied to the account.
      $stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
      $stmt->bind_param('s', $username);
      $stmt->execute();

      $result = $stmt->get_result();

      $emailContents = file_get_contents('assets/emailTemplate.html');
      $emailContents = str_replace('(recoverLinkHere)', $recoverLink, $emailContents);

      if ($result->num_rows < 1) {
          // Somehow, this account does not exist!
          $response = 'The specified account does not exist!';
          header('Location: https://toontastic.sunrise.games');
      } else {
          while ($arr = $result->fetch_assoc()) {
              $ID = $arr['ID'];
              $email = $arr['Email'];

              if (empty($email)) {
                  // Must be a early Sunrise Games account.
                  $response = 'No email tied to account!';
              } else {
                  $expFormat = mktime(date("H"), date("i"), date("s"), date("m") ,date("d")+1, date("Y"));
                  $expDate = date("Y-m-d H:i:s", $expFormat);

                  $statement = $db->prepare('INSERT INTO PasswordReset (Username, Hash, Expire) VALUES (?, ?, ?)');
                  $statement->bind_param('sss', $username, $recoverHash, $expDate);
                  $statement->execute();

                  // Dispatch the email.
                  $email = new PHPMailer(false);

                  $email->isSMTP();

                  $email->Host = 'smtp.mailgun.org';
                  $email->SMTPAuth = true;
                  $email->Username = 'postmaster@mg.sunrise.games';
                  $email->Password = $emailKey;
                  $email->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                  $email->Port = 587;

                  $email->setFrom('tmemberservices@disney.twdc.com', 'Disney Online Guest Services');
                  $email->addAddress($arr['Email'], 'Account Holder');

                  $email->isHTML(true);

                  $email->Subject = 'Password Reset';
                  $email->Body = $emailContents;
                  $email->AltBody = '';

                  try {
                      $email->send();
                  } catch (Exception $e) {
                      echo 'Caught exception: '. $e->getMessage() ."\n";
                  }
                  $response = 'Successfully dispatched email!';
                  header('Location: https://toontastic.sunrise.games');
              }
            }
        }
    }
  echo $response;