<?php
  if (isset($_GET['serverType'])) {
      $serverType = $_GET['serverType'];
  } else {
      echo 'Invalid parameters.';
      exit();
  }

  $regularData = file_get_contents('releaseNotes.html');
  $maintenanceData = file_get_contents('releaseNotesMaintenance.html');

  if ($serverType == 2) {
      // News for the Test server is different.
      $regularData = file_get_contents('test/releaseNotes.html');
  }

  $statusUrl = 'https://api.sunrise.games/api/getStatusForServer?serverType=' . $serverType;
  $maintenance = file_get_contents($statusUrl);

  if ($maintenance == '1') {
      $contentsOfNews = $maintenanceData;
  } else {
      $contentsOfNews = $regularData;
  }
  echo $contentsOfNews;