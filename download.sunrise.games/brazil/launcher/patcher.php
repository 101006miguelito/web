PATCHER_VERSION_STRING_SERVER=V1.0.1.41
PATCHER_VERSION_STRING_SERVER_OSX=V1.0.1.41
GAME_VERSION_TEXT=sv1.4.40.32
DOWNLOAD_PATCHER_CURRENT_VERSION=ToontownLauncher.exe
DOWNLOAD_PATCHER_CURRENT_VERSION_OSX=ToontownLauncher.zip
GAME_WHITELIST_URL=http://download.sunrise.games/launcher/
GAME_IN_GAME_NEWS_URL=http://download.sunrise.games/toontown/en/gamenews/
GAME_SERVER=127.0.0.1:6667

ADDITIONAL_VERSION_TEXT=QA
ACCOUNT_SERVER=http://unite.sunrise.games:4500

PANDA_DOWNLOAD_URL=http://download.sunrise.games/brazil/launcher/
PATCHER_BASE_URL_HEAVY_LIFTING=http://download.sunrise.games/brazil/launcher/;

WEB_PAGE_LOGIN_RPC=https://sunrise.games/api/login

# use this format to push a errorcode to a special page..
# this example will spawn this page on exit with a value of 13 put in the errcode file
#WEB_PAGE_APP_ERROR_13=http://www.disney.com/

REQUIRED_INSTALL_FILES=phase_3.mf:2 phase_3.5.mf:2 phase_4.mf:2 phase_5.mf:2 phase_5.5.mf:2 phase_6.mf:2 phase_7.mf:2 phase_8.mf:2 phase_9.mf:2 phase_10.mf:2 phase_11.mf:2 phase_12.mf:2 phase_13.mf:2
REQUIRED_INSTALL_FILES_OSX=phase_3.mf:2 phase_3.5.mf:2 phase_4.mf:2 phase_5.mf:2 phase_5.5.mf:2 phase_6.mf:2 phase_7.mf:2 phase_8.mf:2 phase_9.mf:2 phase_10.mf:2 phase_11.mf:2 phase_12.mf:2 phase_13.mf:2

#REMOVE_OLD_FILE_LIST=QuestScripts.txt

FILE_phase_3.mf.current=v1.53
FILE_phase_3.5.mf.current=v1.60
FILE_phase_4.mf.current=v1.62
FILE_phase_5.mf.current=v1.43
FILE_phase_5.5.mf.current=v1.40
FILE_phase_6.mf.current=v1.48
FILE_phase_7.mf.current=v1.29
FILE_phase_8.mf.current=v1.47
FILE_phase_9.mf.current=v1.36
FILE_phase_10.mf.current=v1.32
FILE_phase_11.mf.current=v1.31
FILE_phase_12.mf.current=v1.31
FILE_phase_13.mf.current=v1.28

#phase_3.mf
FILE_phase_3.mf.v1.53=137094699 4a0ecc2af97b8efc9ea22a2208629fbc
#phase_3.5.mf
FILE_phase_3.5.mf.v1.60=82174739 b4398cae3c8db6f2910b0913a73e17d1
#phase_4.mf
FILE_phase_4.mf.v1.62=39101904 1038c565ddb5cf1d4d5b474e28653d02
#phase_5.mf
FILE_phase_5.mf.v1.43=52524722 33f5222718fff9562c60ec74cafbfccf
#phase_5.5.mf
FILE_phase_5.5.mf.v1.40=10915061 65b148ab25e598b875f97f3a259ea826
#phase_6.mf
FILE_phase_6.mf.v1.48=30761885 b46d343c3586c31d286d1093c3a8d466
#phase_7.mf
FILE_phase_7.mf.v1.29=344197 47ea5133b3f5bc573951b5c7c17c408d
#phase_8.mf
FILE_phase_8.mf.v1.47=17601581 12316f6ad607a6a82acfc4f08acf737f
#phase_9.mf
FILE_phase_9.mf.v1.36=7792812 009c2823df58ef91776663d708e186dc
#phase_10.mf
FILE_phase_10.mf.v1.32=3801034 64fd58f8e75760895471fe010871abf6
#phase_11.mf
FILE_phase_11.mf.v1.31=2833008 8eeae935f5768e84597eae87ccd233ca
#phase_12.mf
FILE_phase_12.mf.v1.31=5284948 7db1e60933757dfbac62122fbff4ade7
#phase_13.mf
FILE_phase_13.mf.v1.28=5247113 319a7461cc34e88f597ca46d7d7323c8

#EOF