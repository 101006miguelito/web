PATCHER_VERSION_STRING_SERVER=V1.0.1.41

PATCHER_VERSION_STRING_SERVER_OSX=V1.0.1.41

GAME_VERSION_TEXT=sv1.0.40.10.test

DOWNLOAD_PATCHER_CURRENT_VERSION=ToontownLauncher.exe

DOWNLOAD_PATCHER_CURRENT_VERSION_OSX=ToontownLauncher.zip

GAME_WHITELIST_URL=http://download.sunrise.games/test/2010/launcher/

GAME_IN_GAME_NEWS_URL=http://download.sunrise.games/launcher/news/game-news/

GAME_SERVER=127.0.0.1:6667

ADDITIONAL_VERSION_TEXT=TEST

ACCOUNT_SERVER=http://unite.sunrise.games:4500

PANDA_DOWNLOAD_URL=http://download.sunrise.games/test/2010/launcher/

PATCHER_BASE_URL_HEAVY_LIFTING=http://download.sunrise.games/test/2010/launcher/;

WEB_PAGE_LOGIN_RPC=https://sunrise.games/api/login

# use this format to push a errorcode to a special page..

# this example will spawn this page on exit with a value of 13 put in the errcode file

#WEB_PAGE_APP_ERROR_13=http://www.disney.com/

REQUIRED_INSTALL_FILES=phase_1.mf:3 phase_2.mf:3 phase_3.mf:2 phase_3.5.mf:2 phase_4.mf:2 phase_5.mf:2 phase_5.5.mf:2 phase_6.mf:2 phase_7.mf:2 phase_8.mf:2 phase_9.mf:2 phase_10.mf:2 phase_11.mf:2 phase_12.mf:2 phase_13.mf:2

REQUIRED_INSTALL_FILES_OSX=phase_1OSX.mf:3 phase_2OSX.mf:3 phase_3.mf:2 phase_3.5.mf:2 phase_4.mf:2 phase_5.mf:2 phase_5.5.mf:2 phase_6.mf:2 phase_7.mf:2 phase_8.mf:2 phase_9.mf:2 phase_10.mf:2 phase_11.mf:2 phase_12.mf:2 phase_13.mf:2

#REMOVE_OLD_FILE_LIST=QuestScripts.txt

FILE_phase_1.mf.current=v1.157

FILE_phase_1OSX.mf.current=v1.101

FILE_phase_2.mf.current=v1.157

FILE_phase_2OSX.mf.current=v1.33

FILE_phase_3.mf.current=v1.53

FILE_phase_3.5.mf.current=v1.60

FILE_phase_4.mf.current=v1.62

FILE_phase_5.mf.current=v1.43

FILE_phase_5.5.mf.current=v1.40

FILE_phase_6.mf.current=v1.48

FILE_phase_7.mf.current=v1.29

FILE_phase_8.mf.current=v1.47

FILE_phase_9.mf.current=v1.36

FILE_phase_10.mf.current=v1.32

FILE_phase_11.mf.current=v1.31

FILE_phase_12.mf.current=v1.31

FILE_phase_13.mf.current=v1.28

#phase_1.mf

FILE_phase_1.mf.v1.157=33822627 014cdd6bb9333f33c1ec4c5fa50b8de6

#phase_2.mf

FILE_phase_2.mf.v1.157=21172481 8ac475583f6e15a6ba24f0a2e92cf78a

#phase_1OSX.mf

FILE_phase_1OSX.mf.v1.101=58070619 3d1f3896206057afdda38f68d87c9658

#phase_3.mf

FILE_phase_3.mf.v1.53=8266095 08f52a618745b89eb9a092642027c074

#phase_3.5.mf

FILE_phase_3.5.mf.v1.60=56461949 5bfdc56501dbff9763416e81e44f8401

#phase_4.mf

FILE_phase_4.mf.v1.62=37195637 16971508f0771f3e105a88978142c858

#phase_5.mf

FILE_phase_5.mf.v1.43=26287487 f5509b5757c7a8fe1292fea49a9a1f79

#phase_5.5.mf

FILE_phase_5.5.mf.v1.40=10738389 937563a661ec7e877ba06d6bd61b098c

#phase_6.mf

FILE_phase_6.mf.v1.48=24863193 044dc03936972ddf4df091b0a8fed8d9

#phase_7.mf

FILE_phase_7.mf.v1.29=344197 b0023a851eac38958ec56429bc9c02b4

#phase_8.mf

FILE_phase_8.mf.v1.47=7079215 8c0dd6f854bdc07a49e000ccd381f447

#phase_9.mf

FILE_phase_9.mf.v1.36=7553007 2aa4548231512031b851805ef85a1915

#phase_10.mf

FILE_phase_10.mf.v1.32=3798566 b57867e4f7a25a0e9c083a11271e2413

#phase_11.mf

FILE_phase_11.mf.v1.31=2832507 019ec65e5353f067051ea9ac9f61ea07

#phase_12.mf

FILE_phase_12.mf.v1.31=5289398 98a466cf058104b2c19907b69cec9abc

#phase_13.mf

FILE_phase_13.mf.v1.28=1560380 e9a8b6f079c1bf7f00a049ef9500fb75

#EOF