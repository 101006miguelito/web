PATCHER_VERSION_STRING_SERVER=V1.0.1.87
PATCHER_VERSION_STRING_SERVER_OSX=V1.0.1.87
GAME_VERSION_TEXT=pcsv1.0.34.31
DOWNLOAD_PATCHER_CURRENT_VERSION=ToontownLauncher.exe
DOWNLOAD_PATCHER_CURRENT_VERSION_OSX=ToontownLauncher.zip
GAME_WHITELIST_URL=http://download.sunrise.games/pirates/2013/
GAME_IN_GAME_NEWS_URL=http://unset.com/launcher/news/game-news/
GAME_SERVER=127.0.0.1:6667

ACCOUNT_SERVER=http://unite.sunrise.games:4500

PANDA_DOWNLOAD_URL=http://download.sunrise.games/pirates/2013/launcher/
PATCHER_BASE_URL_HEAVY_LIFTING=http://download.sunrise.games/pirates/2013/launcher/;

WEB_PAGE_LOGIN_RPC=https://sunrise.games/api/login

# use this format to push a errorcode to a special page..
# this example will spawn this page on exit with a value of 13 put in the errcode file
#WEB_PAGE_APP_ERROR_13=http://www.disney.com/

REQUIRED_INSTALL_FILES=phase_1.mf:3 phase_2.mf:3 phase_3.mf:2 phase_4.mf:2 phase_5.mf:2
REQUIRED_INSTALL_FILES_OSX=phase_1OSX.mf:3 phase_2OSX.mf:3 phase_3.mf:2 phase_4.mf:2 phase_5.mf:2

#REMOVE_OLD_FILE_LIST=QuestScripts.txt

FILE_phase_1.mf.current=v1.157
FILE_phase_1OSX.mf.current=v1.101
FILE_phase_2.mf.current=v1.157
FILE_phase_2OSX.mf.current=v1.33
FILE_phase_3.mf.current=v1.53
FILE_phase_4.mf.current=v1.62
FILE_phase_5.mf.current=v1.43

#phase_1.mf
FILE_phase_1.mf.v1.157=105322852 70dd6b539d68bf833de2d9677a1c7c94
#phase_2.mf
FILE_phase_2.mf.v1.157=55393765 eac3dfdf2caa1b0eff161421db5155dd
#phase_1OSX.mf
FILE_phase_1OSX.mf.v1.101=58070619 3d1f3896206057afdda38f68d87c9658
#phase_3.mf
FILE_phase_3.mf.v1.53=65915618 cc8ba786cfe1fa1cf9d48f4229b19c03
#phase_4.mf
FILE_phase_4.mf.v1.62=191480880 cb12d947d9ea71f5f440ed549bb0cc61
#phase_5.mf
FILE_phase_5.mf.v1.43=121522892 d5d3bd8c159a007a3e7fa3b85ec5c133
#EOF
