# Sunrise Games Website

When you push to this repository, the production website will automatically be updated every 30 minutes.

## Dependencies
* [JSON-RPC 2.0 client](https://github.com/datto/php-json-rpc-http)

### Reporting a Vulnerability
If you have found a security vulnerability in our web code, please send a message to Rocket on Discord (Rocket#1776).

Please include details on how to trigger the vulnerability in your message.