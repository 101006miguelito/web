<?php
require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

// Mail
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

function sendMail($emailAddr, $emailContents, $fromAddr, $fromName, $addrName, $emailSubject, $emailKey) {
    // Dispatch the email.
    $email = new PHPMailer(false);

    $email->isSMTP();

    $email->Host = 'smtp.mailgun.org';
    $email->SMTPAuth = true;
    $email->Username = 'postmaster@mg.sunrise.games';
    $email->Password = $emailKey;
    $email->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $email->Port = 587;

    $email->setFrom($fromAddr, $fromName);
    $email->addAddress($emailAddr, $addrName);

    $email->isHTML(true);

    $email->Subject = $emailSubject;
    $email->Body = $emailContents;
    $email->AltBody = '';

    try {
      $email->send();
      return true;
    } catch (Exception $e) {
      echo 'Caught exception: '. $e->getMessage() ."\n";
      return false;
    }
}