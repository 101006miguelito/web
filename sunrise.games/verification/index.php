<?php
  include '../config/config.php';

  $response = 'Please use the link that has been sent to your email.';

  if (isset($_GET['emailAddress']) && !empty($_GET['emailAddress']) AND isset($_GET['emailHash']) && !empty($_GET['emailHash'])) {
      $emailAddress = $_GET['emailAddress'];
      $emailHash = $_GET['emailHash'];

      $stmt = $db->prepare('SELECT * FROM Users WHERE Email = ? AND EmailHash = ?');
      $stmt->bind_param('ss', $emailAddress, $emailHash);
      $stmt->execute();

      $result = $stmt->get_result();

      if ($result->num_rows > 0) {
          // Activate the account.
          $flag = 1;

          $setVerified = $db->prepare('UPDATE Users SET Verified = ? WHERE Email = ? AND EmailHash = ?');
          $setVerified->bind_param('iss', $flag, $emailAddress, $emailHash);
          $setVerified->execute();

          $response = 'Account has been activated!';
          header('Location: https://sunrise.games');
      } else {
          // Nope, invalid activation URL or the account has already been activated.
          $response = 'Account has already been activated!';
          header('Location: https://sunrise.games');
      }
    }
    echo $response;