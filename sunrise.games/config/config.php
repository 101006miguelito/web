<?php
  /*
    This is where I keep all my functions.
    Author: Rocket
    Author URL: https://rocketprogrammer.me
    Version: 0.1
  */

  $configPath = '';

  if (PHP_OS_FAMILY == 'Windows') {
      $configPath = 'site.ini';
  } else {
      $configPath = '/etc/site.ini';
  }

  // Parse the configuration data.
  $conf = parse_ini_file($configPath, true)['Config'];

  global $dbPass, $emailKey, $salt, $secretKey, $panelKey, $isDevelopment;

  // MySQL user password.
  $dbPass = $conf['dbPass'];

  // Email sending key (MailGun).
  $emailKey = $conf['emailKey'];

  // Used for game RPC actions.
  $secretKey = $conf['secretKey'];

  // Used to generate encrypted play tokens.
  $tokenKey = $conf['tokenKey'];

  // Session data for the Staff Panel uses this.
  $panelKey = $conf['panelKey'];

  // In dev, we don't need stuff like Email verification for accounts.
  $isDevelopment = $conf['isDevelopment'];

  // Password salt.
  $salt = $conf['salt'];

  // Host where the MySQL server is hosted.
  $dbHost = '127.0.0.1';

  // MySQL user.
  $dbUser = $conf['dbUser'];

  // Name of database table.
  $db = 'web';

  global $db;
  $db = new mysqli($dbHost, $dbUser, $dbPass, $db);

  function getServerAddress($serverType) {
    // Supplies the server address from the given server type.
    if ($serverType == 'Final Toontown') {
      return 'http://unite.sunrise.games:7969';
    } else if ($serverType == 'Test Toontown 2012') {
        return 'http://gameserver.test.sunrise.games:7969';
    } else if ($serverType == 'Toontown Japan 2010') {
      return 'http://anime.sunrise.games:7969';
    } else if ($serverType == 'Final Toontown Brazil') {
      return 'http://dublagem.sunrise.games:7969';
    }
  }