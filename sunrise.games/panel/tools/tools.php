<?php
  include '../../config/config.php';

  if (empty($_POST['userName']) || empty($_POST['action'])) {
    $response = 'Invalid parameters.';
    echo $response;
    exit();
  }

  $userName = $_POST['userName'];
  $action = $_POST['action'];

  // This is where the DB is queried.
  $sql = "SELECT * FROM Users WHERE Username='$userName'";
  $stmt = $db->query($sql);

  if ($stmt->num_rows < 1) {
    $response = 'The specified account was not found in the database.';
    echo $response;
    exit();
  }

  while ($arr = $stmt->fetch_assoc()) {
    if ($action == 'Chat') {
      $chatFlag = $arr['OpenChat'];

      if ($chatFlag == 1) {
        $flag = 0;
      } else {
        $flag = 1;
      }

      $db->query("UPDATE Users SET OpenChat='".$flag."' WHERE Username='".$userName."'");
      $response = 'Toggled chat for specified user.';
    } elseif ($action == 'Membership') {
      $memberFlag = $arr['Member'];

      if ($memberFlag == 1) {
        $flag = 0;
      } else {
        $flag = 1;
      }
    
      $db->query("UPDATE Users SET Member='".$flag."' WHERE Username='".$userName."'");
      $response = 'Toggled membership for specified user.';
    }
  }

  echo $response;
?>