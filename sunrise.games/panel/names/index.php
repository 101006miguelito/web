<?php
include '../../config/config.php';
include '../../libs/Cryptography.php';

// Start our PHP session.
session_start();


if (!isset($_SESSION['panelToken'])) {
    exit();
  } else {
    $panelToken = $_SESSION['panelToken'];
    $decrypted = checkToken($panelKey, $panelToken);

    // Try to decrypt the specified token.
    if (!$decrypted) {
      // Fake token.
      exit();
    }
  }
?>

<title>Sunrise Games | Name Approval</title>
<center><a href="../index.php">Back</a><br><br><Br>
<form id="Form1" action="approval.php" method="post">
<input type='hidden' id='operation' name='operation' value='jes' />
<input type='hidden' id='selected' name='selected' value='||' />
<center><div style="overflow:scroll; height: 400px; width:250px; list-style-type: none;" align="left">
    <?php
        $sql = "CALL GetPendingNames";
        $result = $db->query($sql);
        $db->next_result();

        if(!$result){
            die ('Something has gone wrong, try again later.');
        }

        if($result->num_rows > 0){
            echo "<ul>";
            while($row = $result->fetch_assoc()) {
                echo "<li><input type='checkbox' id='".$row["id"]."' name='".$row["id"]."' />".$row["Name"]."</li>";
            }
            echo "</ul>";
        }else{
    ?>
        No names awaiting approval!
    <?php
        }?>
</div>
<div>
    <button onClick="Accept();">Accept</button>
    &nbsp;&nbsp;
    <button onClick="Decline();">Decline</button>
</div>
</form>
<img src="https://rocketprogrammer.me/uploads/YS0oWpv.png" />
<?php
        if($_GET["m"] == 1){
    ?>
            <h2><font color="red">Names Approved</font></h2>
    <?php
        }else if($_GET["m"] == 2){
            echo '<h2><font color="red">Names Declined</font></h2>';
        }
    ?>
<script>
    function Accept(){
        var selected="";

        $("input:checkbox").each(function(){
            var $this = $(this);

            if($this.is(":checked")){
                selected += $this.attr("id") + "|";
            }
        });

        $("#operation").val("jes");
        $("#selected").val(selected.slice(0, -1));

        $("Form1").submit();
    }

    function Decline(){
        var selected="";

        $("input:checkbox").each(function(){
            var $this = $(this);

            if($this.is(":checked")){
                selected += $this.attr("id") + "|";
            }
        });

        $("#operation").val("ne");
        $("#selected").val(selected.slice(0, -1));

        $("Form1").submit();
    }

    var time = new Date().getTime();
    $(document.body).bind("mousemove keypress", function(e) {
        time = new Date().getTime();
    });

    function refresh() {
        if(new Date().getTime() - time >= 30000)
            document.location = "approval.php";
        else
            setTimeout(refresh, 5000);
    }

     setTimeout(refresh, 5000);
</script>