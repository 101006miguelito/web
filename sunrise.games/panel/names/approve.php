<?php include '../../config/config.php';
header('Content-Type: application/json');

if (!isset($_POST['name']) || !isset($_POST['secretKey']) || !isset($_POST['avatarId']) || !isset($_POST['serverName'])) {
	echo json_encode(array('status' => 'error', 'code' => '0', 'message' => 'Nice try.'));
	exit(0);
}

if ($secretKey !== $_POST['secretKey'] || $_SERVER['HTTP_USER_AGENT'] !== 'SunriseGames-ExtAgent') {
	echo json_encode(array('status' => 'error', 'code' => '1', 'message' => 'Who are you?'));
	exit(0);
}

$name = $db->real_escape_string($_POST['name']);
$avatarId = $db->real_escape_string($_POST['avatarId']);
$serverName = $db->real_escape_string($_POST['serverName']);

$stmt = $db->prepare('SELECT Status FROM NameApproval WHERE Name = ?');
$stmt->bind_param('s', $name);
$stmt->execute();

$result = $stmt->get_result();

if($result->num_rows > 0) {
	$row = $result->fetch_assoc();
	echo json_encode(array('status' => 'success', 'code' => '2', 'nameStatus' => $row["Status"], 'message' => 'Pulled name status.'));
	exit(0);
}

$statement = $db->prepare('INSERT INTO NameApproval (Name, Status, avatarId, ServerName) VALUES (?, ?, ?, ?)');
$statement->bind_param('siis', $name, 0, $avatarId, $serverName);
$statement->execute();

echo json_encode(array('status' => 'success', 'code' => '2', 'nameStatus' => '0', 'message' => 'Sent name for review.'));
?>