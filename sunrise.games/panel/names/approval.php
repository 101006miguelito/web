<?php
include '../../config/config.php';
include '../rpc.php';
include '../../libs/Cryptography.php';

// Start our PHP session.
session_start();

if (!isset($_SESSION['panelToken'])) {
    exit();
  } else {
    $panelToken = $_SESSION['panelToken'];
    $decrypted = checkToken($panelKey, $panelToken);

    // Try to decrypt the specified token.
    if (!$decrypted) {
      // Fake token.
      exit();
    }
  }

$operation = $_POST['operation'];
$selected = $_POST['selected'];

if($operation == "jes"){
	$array = explode('|', $selected);
	foreach($array as $value){
		$query = $db->query("SELECT * FROM NameApproval WHERE id='".$value."'");

		if ($query->num_rows < 1) {
			exit();
		} else {
			while ($arr = $query->fetch_assoc()) {
				$avatarId = $arr['avatarId'];

				$serverName = $arr['ServerName'];
				$endpointAddr = getServerAddress($serverName);
				$rpcObject = new RPCHandler();

				$rpcObject->approveName($avatarId, $endpointAddr);
			};
		}

	    $sql = "UPDATE NameApproval SET Status='2' where id='".$value."'";
		$result = $db->query($sql);

		if(!$result) {
			die ('Something has gone wrong, try again later.');
		}
	}
	header("location:https://sunrise.games/panel/names?m=1");
	exit();
} else {
	$array = explode('|', $selected);
	foreach($array as $value) {
		$query = $db->query("SELECT * FROM NameApproval WHERE id='".$value."'");

		if ($query->num_rows > 0) {
			$response = 'Failed.';
			echo $response;
			exit();
		} else {
			while ($arr = $query->fetch_assoc()) {
				$avatarId = $arr['avatarId'];

				$serverName = $arr['ServerName'];
				$endpointAddr = getServerAddress($serverName);
				$rpcObject = new RPCHandler();

				$rpcObject->rejectName($avatarId, $endpointAddr);
			}
		}

	    $sql = "UPDATE NameApproval SET Status='1' where id='".$value."'";
		$result = $db->query($sql);

		if(!$result) {
			die ('Something has gone wrong, try again later.');
		}
	}
	header("location:https://sunrise.games/panel/names?m=2");
	exit();
}
echo $response;
?>