<?php
  include '../config/config.php';
  include '../libs/Cryptography.php';

  $username = $_POST['username'];
  $password = $_POST['password'];

  // Combining the salt and password together.
  $saltedPassword = $salt . $password;

  // Query the database for the account.
  $stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
  $stmt->bind_param('s', $username);
  $stmt->execute();

  $result = $stmt->get_result();

  if ($result->num_rows < 1) {
    // Account not present in database.
    header('Location: https://sunrise.games');
  } else {
    while ($arr = $result->fetch_assoc()) {
      $hashedPassword = $arr['Password'];

      if (!password_verify($saltedPassword, $hashedPassword)) {
        // Invalid credentials.
        header('Location: https://sunrise.games');
      } else {
        if ($arr['Ranking'] == 'Player') {
          // Not a staff member.
          header('Location: https://sunrise.games');
        } else {
          session_start();

          $data = array(
            'User' => $username,
            'Access' => $arr['Ranking']
          );

          // Set our panel token for this session.
          $token = encrypt(json_encode($data), $panelKey);
          $_SESSION['panelToken'] = $token;

          // Redirect them to the actual panel.
          header('Location: https://sunrise.games/panel/main');
        }
      }
    }
  }