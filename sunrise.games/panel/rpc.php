<?php
require __DIR__ . '/vendor/autoload.php';

use Datto\JsonRpc\Http\Client;
use Datto\JsonRpc\Http\Exceptions\HttpException;
use Datto\JsonRpc\Responses\ErrorResponse;

class RPCHandler {
    /**
     * Execute the RPC call to the server
     *
     * @param string $action
     * @param array $args
     * @return string $result
     */
    function execute($action, $args, $endpointAddr){
        // Create the client
        $client = new Client($endpointAddr);
        $result = '';
        global $secretKey;

        // Create the query
        $client->query(
            'action',
            [
                'action' => $action,
                'arguments' => $args,
                'secretKey' => $secretKey
            ],
            $result
        );

        // Send off the request
        $client->send();

        // Return the result.
        return $result;
    }

    /**
     * Execute the System Message RPC endpoint
     *
     * @param string $message
     * @param string $endpointAddr;
     */
    function systemMessage($message, $endpointAddr) {
        // Execute the RPC endpoint
        $response = $this->execute('systemMessage', [$message], $endpointAddr);

        // Check the response
        if ($response !== 'Nice try.') {
            return json_encode([
                'errorCode' => 0,
                'message' => $response
            ]);
        } else {
            return json_encode([
                'errorCode' => 400,
                'message' => $response
            ]);
        }
    }

    /**
     * Execute the Kick RPC endpoint
     *
     * @param int $avId;
     * @param string $reason;
     * @param string $endpointAddr;
     */
    function kickPlayer($avId, $reason, $endpointAddr) {
        // Execute the RPC endpoint
        $response = $this->execute('kickPlayer', [$avId, $reason], $endpointAddr);

        // Check the response
        if ($response !== 'Nice try.') {
            return json_encode([
                'errorCode' => 0,
                'message' => $response
            ]);
        } else {
            return json_encode([
                'errorCode' => 400,
                'message' => $response
            ]);
        }
    }

    /**
     * Execute the name approve RPC endpoint
     *
     * @param int $avId;
     * @param string $endpointAddr;
     */
    function approveName($avId, $endpointAddr) {
        // Execute the RPC endpoint
        $response = $this->execute('approveName', [$avId], $endpointAddr);

        // Check the response
        if ($response !== 'Nice try.') {
            return json_encode([
                'errorCode' => 0,
                'message' => $response
            ]);
        } else {
            return json_encode([
                'errorCode' => 400,
                'message' => $response
            ]);
        }
    }

    /**
     * Execute the name reject RPC endpoint
     *
     * @param int $avId;
     * @param string $endpointAddr;
     */
    function rejectName($avId, $endpointAddr) {
        // Execute the RPC endpoint
        $response = $this->execute('rejectName', [$avId], $endpointAddr);

        // Check the response
        if ($response !== 'Nice try.') {
            return json_encode([
                'errorCode' => 0,
                'message' => $response
            ]);
        } else {
            return json_encode([
                'errorCode' => 400,
                'message' => $response
            ]);
        }
    }

    /**
     * Execute the ban player RPC endpoint
     *
     * @param int $avId;
     * @param string $endpointAddr;
     */
    function banPlayer($avId, $endpointAddr) {
        // Execute the RPC endpoint
        $response = $this->execute('banPlayer', [$avId], $endpointAddr);

        // Check the response
        if ($response !== 'Nice try.') {
            return json_encode([
                'errorCode' => 0,
                'message' => $response
            ]);
        } else {
            return json_encode([
                'errorCode' => 400,
                'message' => $response
            ]);
        }
    }

    /**
     * Execute the warn player RPC endpoint
     *
     * @param int $avId;
     * @param string reason;
     * @param string $endpointAddr;
     */
    function warnPlayer($avId, $reason, $endpointAddr) {
        // Execute the RPC endpoint
        $response = $this->execute('warnPlayer', [$avId, $reason], $endpointAddr);

        // Check the response
        if ($response !== 'Nice try.') {
            return json_encode([
                'errorCode' => 0,
                'message' => $response
            ]);
        } else {
            return json_encode([
                'errorCode' => 400,
                'message' => $response
            ]);
        }
    }
}