<?php
  include '../rpc.php';
  include '../../config/config.php';

  if (!isset($_SESSION['panelToken'])) {
    exit();
  } else {
    $panelToken = $_SESSION['panelToken'];
    $decrypted = checkToken($panelKey, $panelToken);
  
    // Try to decrypt the specified token.
    if (!$decrypted) {
      // Fake token.
      exit();
    }
  }
  
  if (empty($_POST['server']) || empty($_POST['reason']) || empty($_POST['action']) || empty($_POST['avId'])) {
    $response = 'Invalid parameters.';
    echo $response;
    exit();
  }

  $serverName = $_POST['server'];
  $reason = $_POST['reason'];
  $modAction = $_POST['action'];
  $avId = $_POST['avId'];

  $endpointAddr = getServerAddress($serverName);
  $rpcObject = new RPCHandler();

  if ($modAction == 'Kick') {
    $rpcObject->kickPlayer($avId, $reason, $endpointAddr);
    $response = 'Kicked player from the server.';
  } elseif ($modAction == 'ApproveName') {
    $rpcObject->approveName($avId, $endpointAddr);
    $response = 'Approved name for avatar.';
  } elseif ($modAction == 'RejectName') {
    $rpcObject->rejectName($avId, $endpointAddr);
    $response = 'Approved name for avatar.';
  } elseif ($modAction == 'Ban') {
    $rpcObject->banPlayer($avId, $endpointAddr);
    $response = 'Banned and kicked player from the server.';
  } elseif ($modAction == 'Warn') {
    $rpcObject->warnPlayer($avId, $reason, $endpointAddr);
    $response = 'Warned player.';
  } else {
    $response = 'Unknown action.';
  }
  echo $response;