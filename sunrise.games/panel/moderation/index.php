<?php
include '../../libs/Cryptography.php';
include '../../config/config.php';

// Start our PHP session.
session_start();

if (!isset($_SESSION['panelToken'])) {
  exit();
} else {
  $panelToken = $_SESSION['panelToken'];
  $decrypted = checkToken($panelKey, $panelToken);

  // Try to decrypt the specified token.
  if (!$decrypted) {
    // Fake token.
    exit();
  }
}
?>

<!DOCTYPE html>
<html>
<head>
<title>Sunrise Games | Moderation</title>
<style>
      @font-face {
        font-family: 'vtRemington';
        src: url(../fonts/vtRemingtonPortable.ttf) format('truetype')
      }
      body {
        background: url(https://sunrise.games/img/lightyearBG.jpg) no-repeat center center fixed;
        font-family: 'vtRemington';
        background-size: cover;
      }
      div.container {
        margin: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        text-align: center;
        background-color: rgba(225, 225, 225, 0.6);
        border: 10px black;
        padding: 50px;
        border-radius: 16px;
      }
      p.query, span.error {
        font-size: 1.2em;
      }
      span.error {
        color: #ff0000;
      }
      input.button {
        box-shadow: inset 0px 1px 3px 0px #91b8b3;
        background: linear-gradient(to bottom, #55635f 5%, #3e4747 100%);
        background-color: #55635f;
        border-radius: 5px;
        border: 1px solid #566963;
        display: inline-block;
        cursor: pointer;
        color: #ffffff;
        font-size: 1.1em;
        font-weight: bold;
        padding: 11px 23px;
        text-decoration: none;
        }
      input.button:hover {
        background: linear-gradient(to bottom, #3e4747 5%, #55635f 100%);
        background-color: #3e4747;
      }
      input.button:active {
        position: relative;
        top: 1px;
      }
      input[type=avId] {
        font-size: 1.5em;
      }
    </style>
</head>
<body>
<div class="container">
<h1>Please enter a avatarId, a reason, action and a server to send it to.</h1>
<br>
<form method="POST" action="moderation.php">
<input type="avId" name="avId" />
<br><br>
<input type="reason" name="reason" />
<br>
<br>
<div class="formrow">
    <label class="drops">
        <select name="action">
            <option value="Kick">Kick</option>
            <option value="Ban">Ban</option>
            <option value="Warn">Warn</option>
          </select>
    </label>
<div class="formrow">
    <label class="drops">
        <select name="server">
            <option value="Final Toontown">Final Toontown</option>
            <option value="Test Toontown 2012">Test Toontown 2012</option>
            <option value="Toontown Japan 2010">Toontown Japan 2010</option>
            <option value="Final Toontown Brazil">Final Toontown Brazil</option>
        </select>
    </label>
<br><br>
<input type="submit" class="button" value="Send" />
</form>
</div>
</body>
</html>