<?php
  include '../../libs/Cryptography.php';
  include '../../config/config.php';

  // Start our PHP session.
  session_start();

  if (!isset($_SESSION['panelToken'])) {
    exit();
  } else {
    $panelToken = $_SESSION['panelToken'];
    $decrypted = checkToken($panelKey, $panelToken);

    // Try to decrypt the specified token.
    if (!$decrypted) {
      // Fake token.
      exit();
    }
  }
?>

<link rel="stylesheet" type="text/css" href="../css/normalize.css" />
<link rel="stylesheet" type="text/css" href="../css/demo.css" />
<link rel="stylesheet" type="text/css" href="../css/component.css" />
<script src="../js/modernizr-custom.js"></script>
<ul>
  <body>
    <div id="perspective" class="perspective effect-rotateleft">
      <div class="container">
        <div class="wrapper"><!-- wrapper needed for scroll -->
          <!-- Top Navigation -->
          <header class="codrops-header">
          <img src="https://sunrise.games/img/logo.png" height="200px" width="475px" />
          </header>
          <div class="main clearfix">
            <center>
              <p><button id="showMenu" style="min-height: 100px; width: 50%; padding: 0 2em; position: relative;">Show Menu</button></p>
          </div><!-- /main -->
        </div><!-- wrapper -->
      </div><!-- /container -->
      <nav class="outer-nav right vertical">
        <a href="../names" class="icon-lock">Name Approval</a>
        <a href="../system" class="icon-lock">System Message</a>
        <a href="../moderation" class="icon-lock">Moderation</a>
        <a href="../tools" class="icon-lock">Account Tools</a>
<a href="../database" class="icon-upload">Database Management</a>
</nav>
</div><!-- /perspective -->
<script src="../js/classie.js"></script>
<script src="../js/menu.js"></script>
</script>
</body>