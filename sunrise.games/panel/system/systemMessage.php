<?php
  include '../rpc.php';
  include '../../config/config.php';

  if (!isset($_SESSION['panelToken'])) {
    exit();
  } else {
    $panelToken = $_SESSION['panelToken'];
    $decrypted = checkToken($panelKey, $panelToken);

    // Try to decrypt the specified token.
    if (!$decrypted) {
      // Fake token.
      exit();
    }
  }

  if (!isset($_POST['server']) || !isset($_POST['message'])) {
    echo json_encode(array('status' => 'error', 'code' => '0', 'message' => 'Invalid parameters.'));
    exit(0);
  }

  $serverName = $_POST['server'];
  $endpointAddr = getServerAddress($serverName);
  $rpcObject = new RPCHandler();
  $message = $_POST['message'];
  $rpcObject->systemMessage($message, $endpointAddr);
  echo 'Dispatched system message to RPC.';