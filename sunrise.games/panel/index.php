<!DOCTYPE html>
<html>
<head>
<title>Sunrise Games | Staff Login</title>
<style>
      @font-face {
        font-family: 'vtRemington';
        src: url(fonts/vtRemingtonPortable.ttf) format('truetype')
      }
      body {
        background: url(https://sunrise.games/img/lightyearBG.jpg) no-repeat center center fixed;
        font-family: 'vtRemington';
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
      }
      div.container {
        margin: 0;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        text-align: center;
        background-color: rgba(225, 225, 225, 0.6);
        border: 10px black;
        padding: 50px;
        border-radius: 16px;
      }
      p.query, span.error {
        font-size: 1.2em;
      }
      span.error {
        color: #ff0000;
      }
      input.button {
        box-shadow: inset 0px 1px 3px 0px #91b8b3;
        background: linear-gradient(to bottom, #55635f 5%, #3e4747 100%);
        background-color: #55635f;
        border-radius: 5px;
        border: 1px solid #566963;
        display: inline-block;
        cursor: pointer;
        color: #ffffff;
        font-size: 1.1em;
        font-weight: bold;
        padding: 11px 23px;
        text-decoration: none;
        }
      input.button:hover {
        background: linear-gradient(to bottom, #3e4747 5%, #55635f 100%);
        background-color: #3e4747;
      }
      input.button:active {
        position: relative;
        top: 1px;
      }
      input[type=username] {
        font-size: 1.5em;
      }
      input[type=password] {
        font-size: 1.5em;
      }
    </style>
</head>
<body>
<div class="container">
<h1>Please sign in to continue.</h1>
<br>
<form method="POST" action="login.php">
<input type="username" name="username" />
<br>
<input type="password" name="password" />
<br><br>
<input type="submit" class="button" value="Login" />
</form>
</div>
</body>
</html>