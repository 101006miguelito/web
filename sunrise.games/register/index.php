<?php include 'register.php'; ?>
<!DOCTYPE html>
<html>
<script src='https://www.google.com/recaptcha/api.js'></script>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, maximum-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="master.css" charset="utf-8">
    <title>Register | Sunrise Games</title>
  </head>
  <body>
    <div class="container">
    <center><img src="../img/logo.png" class="img-responsive" height="300px" width="590px"></center>
        <div class="col-sm-6 col-sm-offset-3">
          <div class="panel panel-default">
          <form method="post">
              <div class="form-group">
                <label>Username</label>
                <input type="text" name="Usr" class="form-control"/>
              </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="Pwd" class="form-control"/>
            </div>
            <div class="form-group">
              <label>Password Confirm</label>
              <input type="password" name="pwdConf" class="form-control"/>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" name="Email" class="form-control"/>
              </div>
              <div class="form-group">
                <label>Email Confirm</label>
                <input type="text" name="EmailConf" class="form-control"/>
              </div>
              <div class="form-group">
                <div class="g-recaptcha" data-sitekey="6LfZudIZAAAAAG6VUM7X7jopYeAdEpa_I71SLj5O"></div>
            </div>
            <center><button type="submit" name="Submit" class="btn btn-primary btn-lg">Register</center></button>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>