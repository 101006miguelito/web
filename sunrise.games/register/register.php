<?php
  include '../config/config.php';
  include '../libs/Mail.php';

  $output = '';

  if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
      // We are using Cloudflare.
      $ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
    }
  else {
      // We are not using Cloudflare.
      $ip = $_SERVER['REMOTE_ADDR'];
    }

  if (isset($_POST['Submit'])) {
    $usr = strtolower($_POST['Usr']);
    $pwd = $_POST['Pwd'];
    $spwd = $salt . $pwd;
    $hpwd = password_hash($spwd, PASSWORD_DEFAULT);
    $pwdConf = $_POST['pwdConf'];
    $re = $_POST['g-recaptcha-response'];
    $email = $_POST['Email'];
    $emailConf = $_POST['EmailConf'];
    $emailHash = bin2hex(random_bytes(16));

    $statement = $db->prepare('INSERT INTO Users (Email, Username, Password, EmailHash) VALUES (?, ?, ?, ?)');
    $statement->bind_param('ssss', $email, $usr, $hpwd, $emailHash);

    // Check if the username is already taken.
    $userNameTakenQuery = $db->prepare('SELECT * FROM Users WHERE Username = ?');
    $userNameTakenQuery->bind_param('s', $usr);
    $userNameTakenQuery->execute();

    $userNameTakenResult = $userNameTakenQuery->get_result();

    $emailContents = '';
    $emailContents = file_get_contents('assets/emailTemplate.html');
    $toonsName = 'Toon';
    $partsToMod = array('(PUTYOURNAMEHERE)', '(YOURUSERNAMEHERE)', '(YOUREMAILHERE)', '(verifyLinkHere)');
    $verifyLink = 'https://sunrise.games/verification/?emailAddress='.$email.'&emailHash='.$emailHash.'';
    $replaceWith = array($toonsName, $usr, $email, $verifyLink);

    for ($i = 0; $i < count($partsToMod); $i++) {
      $emailContents = str_replace($partsToMod[$i], $replaceWith[$i], $emailContents);
    }

    if (!$re) {
      $output = "<center><div class='alert alert-danger'><strong>Uh oh!</strong> Please do the captcha!</div></center>";
    } else {
      $resp = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LfZudIZAAAAAGPBfXFY5Jg2LUbN4oLAFzKv35VA&response=".$re."&remoteip=".$ip."");
      $captchaResp = json_decode($resp);
      if (!$captchaResp->success) {
        $output = '<center><div class="alert alert-danger"><strong>Uh oh!</strong> Are you a bot? Try again.</div></center>';
      } else {
        if(empty($pwd) || empty($pwdConf) || empty($email) || empty($emailConf) || $pwd != $pwdConf || $email != $emailConf) {
          $output = "<center><div class='alert alert-danger'><strong>Uh oh!</strong> You did not input all required information.</div></center>";
        } else if ($userNameTakenResult->num_rows > 0) {
            $output = "<center><div class='alert alert-danger'><strong>Uh oh!</strong> Username is already taken!</div></center>";
          } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $output = "<center><div class='alert alert-danger'><strong>Uh oh!</strong> Invalid email!</center></div>";
        } else if ($statement->execute()) {
            $output = "<center><div class='alert alert-success'> You're now signed up for Sunrise Games!\nPlease check your email address to verify.</center></div>";

            if ($isDevelopment) {
              // In development, we don't want emails to be sent for verification.
              $output = "<center><div class='alert alert-success'> You're now signed up for Sunrise Games!</center></div>";
            } else {
              // Dispatch the email.
              if (!sendMail($email, $emailContents, 'mg@sunrise.games', 'Sunrise Games', 'Account Holder', 'Welcome to Sunrise Games!', $emailKey)) {
                $output = "<center><div class='alert alert-danger'><strong>Uh oh!</strong> Something has gone wrong.</div></center>";
              } else {
                $output = "<center><div class='alert alert-success'> You're now signed up for Sunrise Games!\nPlease check your email address to verify.</center></div>";
              }
            }
          } else {
            $output = "<center><div class='alert alert-danger'><strong>Uh oh!</strong> Something went wrong creating account.</div></center>";
          }
        }
    }
  }
echo $output;