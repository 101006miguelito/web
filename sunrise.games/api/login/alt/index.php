<?php
  /*
    Alternate API endpoint for authentication.
    Author: Sunrise Games Team
    Author URL: https://sunrise.games
    Version: 1.1
  */

  include '../../../config/config.php';
  include '../../../libs/Cryptography.php';

  // Where the server gets the data.
  if (isset($_POST['username']) && isset($_POST['password'])) {
    $usr = $_POST['username'];
    $pwd = $_POST['password'];

    // Ease of use.
    if (isset($_POST['serverType'])) {
      $serverType = $_POST['serverType'];
    } else {
      $serverType = 'Final Toontown';
    }

  } else {
    // They did not provide POST parameters.
    echo json_encode(array('success' => False, 'errorCode' => -1, 'reason' => 'Missing arguments.'));
    exit();
  }

  // Server Accessibility.
  $statusUrl = 'https://api.sunrise.games/api/getStatusForServer?serverType=' . $serverType;
  $maintenance = file_get_contents($statusUrl);

  $isTest = 0;

  if ($serverType == 2) {
    // This is Test Toontown.
    $isTest = 1;
  }

  if ($maintenance == '1') {
    $isClosed = 1;
  } else {
    $isClosed = 0;
  }

  // Convert the username to all lowercase.
  $usr = strtolower($usr);

  // Combine the salt and password together.
  $spwd = $salt . $pwd;

  // Retrieve our client's IP address.
  if (isset($_POST['clientAddr'])) {
    // Request is from the server, not a API request from a user.
    $ip = $_POST['clientAddr'];
  } else {
    // Client request.
    $ip = $_SERVER['REMOTE_ADDR'];
  }

  // This is where the DB is queried.
  $stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
  $stmt->bind_param('s', $usr);
  $stmt->execute();

  $result = $stmt->get_result();

  // The default response array.
  $responseArray = array(
    'success' => False,
    'errorCode' => 0,
    'token' => '',
    'message' => sprintf('Welcome back, %s!', $usr)
  );

  // Finally, the IF statement. Only edit if you know what's happening.
  if ($result->num_rows < 1) {
    $responseArray['errorCode'] = 700;
    $responseArray['message'] = 'The specified account was not found in the database.';
  } else {
    while ($arr = $result->fetch_assoc()) {
      // Grab the User's password from the database.
      $ID = $arr['ID'];
      $hpwd = $arr['Password'];

      $timestamp = time() + 10 * 60;

      $array = array(
        'playToken' => $usr,
        'OpenChat' => $arr['OpenChat'],
        'Member' => $arr['Member'],
        'Timestamp' => $timestamp,
        'dislId' => $ID,
        'accountType' => $arr['Ranking'],
        'LinkedToParent' => $arr['LinkedToParent'],
        'token' => ''
      );

      // Encrypt the token.
      $encryptedToken = encrypt(json_encode($array), $tokenKey);

      if (!password_verify($spwd, $hpwd)) {
        // Invalid password for this account.
        $responseArray['errorCode'] = 705;
        $responseArray['message'] = 'Incorrect password.';

        // Inserting Login Attempt in the DB
        $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', 'Password was incorrect.')");
      } elseif ($isTest == 1) {
        if ($arr['TestAccess'] == 1 && $arr['Member'] == 1) {
          // This account has Test server access.
          $responseArray['success'] = True;
          $insertMsg = 'Tester accessed Test server for ' . $serverType + '.';
          $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', $insertMsg)");
        } else {
          // This account has no Test server access.
          $responseArray['errorCode'] = 710;
          $responseArray['message'] = 'Invalid access level.';
          $insertMsg = 'Trying to access Test server for ' . $serverType + '.';
          $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', $insertMsg)");
        }
      } elseif ($isClosed == 1) {
        if ($arr['Ranking'] == 'Administrator' || $arr['Ranking'] == 'Developer' || $arr['Ranking'] == 'Moderator') {
          // We are a staff member, therefore we can access the server while its closed.
          $responseArray['success'] = True;
          $responseArray['token'] = $encryptedToken;
          $insertMsg = 'Staff Member access Closed Server for ' . $serverType;
          $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', $insertMsg)");
        } else {
          // We are not able to access the server while it is closed, as we are a normal player.
          $responseArray['errorCode'] = 715;
          $responseArray['message'] = 'Server is closed for maintenance.';
          $insertMsg = 'Trying to access closed server for ' . $serverType + '.';
          $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', $insertMsg)");
        }
      } elseif ($arr['Verified'] == 0) {
        // This account has not verified their email.
        $responseArray['errorCode'] = 720;
        $responseArray['message'] = 'Unverified account.';
        $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', 'Unverified account.')");
      } elseif ($arr['Banned'] == 1) {
        $responseArray['errorCode'] = 725;
        $responseArray['message'] = 'You are currently banned.';
        $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', 'Banned account.')");
      } else {
        // Everything is fine.
        $responseArray['success'] = True;
        $responseArray['token'] = $encryptedToken;
      }
    }
  }
  // Display our response.
  echo json_encode($responseArray);