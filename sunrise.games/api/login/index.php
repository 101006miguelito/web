<?php
  /*
    This is where the magic happens. Schweet!
    Author: Sunrise Games Team
    Author URL: https://sunrise.games
    Version: 0.2
  */

  include '../../config/config.php';
  include '../../libs/Cryptography.php';

  $response = 'Sunrise Games Launcher API.';

  // Server Accessibility.
  if (isset($_GET['serverType'])) {
    $serverType = $_GET['serverType'];
  } else {
    $serverType = 'Final Toontown';
  }

  $statusUrl = 'https://api.sunrise.games/api/getStatusForServer?serverType=' . $serverType;
  $maintenance = file_get_contents($statusUrl);

  $isTest = 0;

  if ($serverType == 2) {
    // This is Test Toontown.
    $isTest = 1;
  }

  if ($maintenance == '1') {
    $isClosed = 1;
  } else {
    $isClosed = 0;
  }

  // Where the server gets the data. e.g https://sunrise.games/api/login/?n=test&p=test
  if (isset($_GET['n']) && isset($_GET['p'])) {
    $usr = $_GET['n']; // Unfortunately, I'm unable to change these. They're in the latest build of the launcher.
    $pwd = $_GET['p'];
  } else {
    if (isset($_GET['username']) && isset($_GET['password'])) {
      // The macOS launcher uses different GET parameter names.
      $usr = $_GET['username'];
      $pwd = $_GET['password'];
    } else {
      if (isset($_POST['username']) && isset($_POST['password'])) {
        // This is coming from our new custom client, Aether.
        $usr = $_POST['username'];
        $pwd = $_POST['password'];
      }
    }
  }

  // Convert the username to all lowercase.
  $usr = strtolower($usr);

  $spwd = $salt . $pwd; // Combinding the salt and pwd together.

  if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) {
      // We are using Cloudflare.
      $ip = $_SERVER['HTTP_CF_CONNECTING_IP'];
  }
  else {
      // We are not using Cloudflare.
      $ip = $_SERVER['REMOTE_ADDR'];
  }

  // This is where the DB is queried.
  $stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
  $stmt->bind_param('s', $usr);
  $stmt->execute();

  $result = $stmt->get_result();

  // Finally, the IF statement. Only edit if you know what's happening.
  if ($result->num_rows < 1) {
    $response = "LOGIN_ACTION=LOGIN\nLOGIN_ERROR=LOGIN_FAILED\nGLOBAL_DISPLAYTEXT=Unable to retrieve account ".$usr.".\n"; // Checking to see if the account exists.
  } else {
    while ($arr = $result->fetch_assoc()) {
      $ID = $arr['ID'];
      $hpwd = $arr['Password']; // Grabbing the User's password.

      $timestamp = time() + 10 * 60;

      $array = array(
        'playToken' => $usr,
        'OpenChat' => $arr['OpenChat'],
        'Member' => $arr['Member'],
        'Timestamp' => $timestamp,
        'dislId' => $ID,
        'accountType' => $arr['Ranking'],
        'LinkedToParent' => $arr['LinkedToParent']
      );

      $accessType = 'VELVET';

      if ($arr['Member']) {
        $accessType = 'FULL';
      }

      $chatEligible = 0;

      if ($arr['OpenChat']) {
        $chatEligible = 1;
      }

      // Encrypt the token.
      $encryptedToken = encrypt(json_encode($array), $tokenKey);

      if (!password_verify($spwd, $hpwd)) {
        $response = "LOGIN_ACTION=LOGIN\nLOGIN_ERROR=LOGIN_FAILED\nGLOBAL_DISPLAYTEXT=Password was incorrect. Please try again.\n"; // Checking to see if the password is incorrect.
        $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', 'Password was incorrect.')"); // Inserting Login Attempt in the DB
      } elseif ($arr['Banned'] == 1 || $arr['Terminated'] == 1) {
        if ($arr['Banned'] == 1) {
          $response = "LOGIN_ACTION=LOGIN\nLOGIN_ERROR=LOGIN_FAILED\nGLOBAL_DISPLAYTEXT=This account is on hold. Please try again later.\nGLOBAL_URL_SPAWN=https://toontastic.sunrise.games/bans?username=$usr\n";
          $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', 'Banned account.')");
        } elseif($arr['Terminated'] == 1) {
          $response = "LOGIN_ACTION=LOGIN\nLOGIN_ERROR=LOGIN_FAILED\nGLOBAL_DISPLAYTEXT=This account is on hold. Please try again later.\nGLOBAL_URL_SPAWN=https://toontastic.sunrise.games/bans/accountHold.php?username=$usr\n";
          $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', 'Terminated account.')");
        }
      } elseif ($isTest == 1) {
        if ($arr['TestAccess'] == 1 && $arr['Member'] == 1) {
          $response = "LOGIN_ACTION=PLAY\nLOGIN_TOKEN=$encryptedToken\nGAME_USERNAME=".$usr."\nGAME_DISL_ID=$ID\nUSER_TOONTOWN_ACCESS=FULL\nGAME_CHAT_ELIGIBLE=1\nDISLTOKEN=dev\nTOONTOWN_BLUE=1\n"; // Distributing Game Token
          $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', 'Tester accessed Test Toontown.')");
        } else {
          $response = "LOGIN_ACTION=LOGIN\nLOGIN_ERROR=LOGIN_FAILED\nGLOBAL_DISPLAYTEXT=You're unable to participate in Test Toontown.\n";
          $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', 'Trying to access Test server.')");
        }
      } elseif ($isClosed == 1) {
        if ($arr['Ranking'] == 'Administrator' || $arr['Ranking'] == 'Developer' || $arr['Ranking'] == 'Moderator') {
          $response = "LOGIN_ACTION=PLAY\nLOGIN_TOKEN=$encryptedToken\nGAME_USERNAME=$usr\nGAME_DISL_ID=$ID\nUSER_TOONTOWN_ACCESS=FULL\nGAME_CHAT_ELIGIBLE=1";
          $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', 'Staff Member accessed Closed Server')");
        } else {
          $response = "LOGIN_ACTION=LOGIN\nLOGIN_ERROR=LOGIN_FAILED\nGLOBAL_DISPLAYTEXT=Closed for maintenance. Please try again later.\n";
          $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', 'Trying to access closed server.')");
        }
      } elseif ($arr['Verified'] == 0) {
        $response = "LOGIN_ACTION=LOGIN\nLOGIN_ERROR=LOGIN_FAILED\nGLOBAL_DISPLAYTEXT=Account isn't verified.\n";
        $db->query("INSERT INTO LoginAttempts (`IP`, `Username`, `Reason`) VALUES('$ip', '$usr', 'Unverified account.')");
      } else {
        $response = "LOGIN_ACTION=PLAY\nLOGIN_TOKEN=".$encryptedToken."\nGAME_USERNAME=".$usr."\nGAME_DISL_ID=$ID\nUSER_TOONTOWN_ACCESS=".$accessType."\nGAME_CHAT_ELIGIBLE=".$chatEligible."";
      }
    }
  }

  echo $response;
