<?php
  include '../../../sunrise.games/config/config.php';

  $response = 'Invalid parameters.';

  if (isset($_POST['username']) && isset($_POST['secretKey'])) {
      if ($secretKey !== $_POST['secretKey']) {
          echo json_encode(array('status' => 'error', 'code' => '1', 'message' => 'Who are you?'));
          exit(0);
        }

      $usr = $_POST['username'];

      // This is where the DB is queried.
      $stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
      $stmt->bind_param('s', $usr);
      $stmt->execute();

      $result = $stmt->get_result();

      if ($result->num_rows) {
        while ($row = $result->fetch_array()) {
            $response = $row['Email'];
        }
    }
  }
  echo $response;