<?php
  include '../../../sunrise.games/config/config.php';

  if (!isset($_POST['username']) || !isset($_POST['password'])) {
      exit();
  }

  function postToDiscord($message) {
    $webhookUrl = 'https://canary.discord.com/api/webhooks/938858349393489925/_A05zrg0hiTrxJZmHXL8A-6QFJa5iNzpc63i7k9z_Q5eo2qriWO79RcH2Ny-bnLNAouO';
    $data = ['content' => $message];

    $options = [
      'http' => [
          'method' => 'POST',
          'header' => 'Content-Type: application/json',
          'content' => json_encode($data)
      ]
  ];

    $context = stream_context_create($options);
    $result = file_get_contents($webhookUrl, false, $context);
  }

  $username = strtolower($_POST['username']);
  $password = $_POST['password'];

  // Combine the salt and password.
  $saltedPassword = $salt . $password;

  // Query the database.
  $stmt = $db->prepare('SELECT * FROM Users WHERE Username = ?');
  $stmt->bind_param('s', $username);
  $stmt->execute();

  $result = $stmt->get_result();

  if ($result->num_rows < 1) {
    echo 'A error has occured or you did not pass POST parameters.';
  } else {
    while ($arr = $result->fetch_assoc()) {
      // Grab the database hashed password.
      $hashedPassword = $arr['Password'];

      if (password_verify($saltedPassword, $hashedPassword) && $arr['Member'] == 1) {
          echo json_encode(array('success' => True, 'errorCode' => -1));
          postToDiscord('User '.$username.' has requested access to the Test Toontown server.');
      } else {
        echo json_encode(array('success' => False, 'errorCode' => -1, 'reason' => 'Invalid password or account is non-paid.'));
      }

      header('Location: https://test.sunrise.games');
    }
  }