-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 07, 2016 at 01:20 AM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
DELIMITER ;;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `TTOPatcher`
--

-- --------------------------------------------------------

--
-- Table structure for table `LoginAttempts`
--

CREATE TABLE IF NOT EXISTS `LoginAttempts` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Username` varchar(100) NOT NULL,
  `IP` TEXT NOT NULL,
  `Reason` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Table structure for table `Bans`
--

CREATE TABLE IF NOT EXISTS `Bans` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Username` varchar(100) NOT NULL,
  `Reason` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------


-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Table structure for table `Terminations`
--

CREATE TABLE IF NOT EXISTS `Terminations` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Username` varchar(100) NOT NULL,
  `Reason` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Table structure for table `GameCards`
--

CREATE TABLE IF NOT EXISTS `GameCards` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Code` VARCHAR( 32 ) NOT NULL DEFAULT '',
  `Used` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Email` TEXT NOT NULL DEFAULT '',
  `Username` varchar(100) NOT NULL,
  `Password` varchar(150) NOT NULL,
  `Ranking` varchar(50) NOT NULL DEFAULT 'Player',
  `Banned` int(10) NOT NULL DEFAULT 0,
  `Terminated` int(10) NOT NULL DEFAULT 0,
  `TestAccess` int(11) NOT NULL DEFAULT 0,
  `Verified` int(11) NOT NULL DEFAULT 0,
  `EmailHash` VARCHAR( 32 ) NOT NULL DEFAULT '',
  `Member` int(11) NOT NULL DEFAULT 0,
  `OpenChat` int(11) NOT NULL DEFAULT 0,
  `FirstName` varchar(100) NOT NULL DEFAULT 'Toon',
  `LastName` varchar(100) NOT NULL DEFAULT 'Toon',
  `Age` int(11) NOT NULL DEFAULT 0,
  `LinkedToParent` int(10) NOT NULL DEFAULT 0,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `PasswordReset`
--

CREATE TABLE `PasswordReset` (
  `Username` varchar(250) NOT NULL,
  `Hash` varchar(250) NOT NULL,
  `Expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

CREATE TABLE IF NOT EXISTS `NameApproval` (
  `Name` varchar(50) NOT NULL,
  `Status` int(10) unsigned DEFAULT NULL,
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `avatarId` int(11) NOT NULL DEFAULT 0,
  `ServerName` varchar(50) NOT NULL DEFAULT 'Final Toontown',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP PROCEDURE IF EXISTS `GetPendingNames`;;
CREATE PROCEDURE `GetPendingNames`()
SELECT * FROM NameApproval
WHERE Status=0;;

CREATE TABLE IF NOT EXISTS `ParentAccounts` (
  `Parent` varchar(150) NOT NULL,
  `Child` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `CodeRedemption` (
  `Code` varchar(150) NOT NULL,
  `UsedBy` JSON NOT NULL,
  `ServerName` varchar(50) NOT NULL DEFAULT 'Final Toontown'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
