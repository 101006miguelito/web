<!DOCTYPE html>
<html class="mythran">
	<head>
		<title>Sunrise Games - Lego Universe</title>
		<link rel="stylesheet" href="css/lu.css?launcher=1" type="text/css"/>
	</head>
	<body style="overflow:hidden;">
		<div style="position: relative">
			<div class="left">
				<div class="box">
					<h3>Welcome!</h3>
				</div>
				<br>
				<span style="color: #FFF">The LEGO Group has not endorsed or authorized the operation of the server and is not liable for any safety issues in relation to the operation of the server.</span>
				<br>
				<br>
				<span style="color: #FFF">Credit for the server base goes to Darkflame Universe.</span>
			</div>
			<p style="position: fixed; bottom: 0; width: 100%; text-align: center; color: #444;">
			</p>
			</div>
		</div>
	</body>
</html>