<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/getPopulation', 'App\Http\Controllers\PopulationController@getPopulation');
Route::post('/setPopulation', 'App\Http\Controllers\PopulationController@setPopulation');