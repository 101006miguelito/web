<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use function response;

class PopulationController extends Controller
{
    /*
     * Constructor for PopulationController
     */
    function __construct() {
        $this->middleware('guest');
    }

    function setPopulation(Request $request) {
        $method = $request->method();

        if ($request->isMethod('post')) {
            $data = $request->all();
            return response()->json($data['u']);
        }
    }

    function getPopulation(Request $request)
    {
        $method = $request->method();

        if ($request->isMethod('get')) {
            // TODO
        }
    }
}
