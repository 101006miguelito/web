#!/bin/sh

# Open our web directory and update it from the GitLab.
cd ~/web && git pull

# Update the production website.
cd sunrise.games && cp -r * /var/www/sunrise.games

# Update the download subdomain.
cd ../download.sunrise.games && cp -r * /var/www/download.sunrise.games

# Update the Toontown Online website replica subdomain.
cd ../toontastic.sunrise.games && cp -r * /var/www/toontastic.sunrise.games

# Update the Lego Universe subdomain.
cd ../imagination.sunrise.games && cp -r * /var/www/imagination.sunrise.games

# Update the Test Toontown subdomain.
cd ../test.sunrise.games && cp -r * /var/www/test.sunrise.games